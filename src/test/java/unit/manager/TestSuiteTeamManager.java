package unit.manager;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.logic.tree.dao.criteria.TeamCriteria;
import org.logic.tree.dao.vo.Team;
import org.logic.tree.manager.TeamManager;
import org.logic.tree.manager.custom.CustomTeam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-configuration.xml")
@TransactionConfiguration
@Transactional
public class TestSuiteTeamManager extends AbstractTransactionalJUnit4SpringContextTests{
	
	@Autowired
	TeamManager teamManager;


	
	@Test
	public void testGetByOrganizationIdCorrect(){
		
		List<Team> teams = teamManager.getByOrganizationId((long)1);
		
		Assert.notEmpty(teams);
		
	}
	
	@Test
	public void testGetByOrganizationIdThatDoesntExist() throws Exception{
		List<Team> teams =  teamManager.getByOrganizationId((long)666);
		
		Assert.isTrue(teams.isEmpty());
	}
	
	@Test
	public void  testFindByCriteriaCorrect(){
		TeamCriteria criteria = new TeamCriteria();
		
		criteria.setIdOrganization((long)1);
		
		List<Team> teams = teamManager.findByCriteria(criteria);
		
		Assert.notEmpty(teams);
		
		Assert.isTrue(teams.get(0).getIdOrganization() == 1);
	}
	
	@Test
	public void testGetAssociationsOfATeamCorrect(){
		
		List<CustomTeam> associations = teamManager.getTeamMemberAssociationsByOrgId((long)1);
		
		Assert.notEmpty(associations);
		Assert.isTrue(associations.get(0).getMailMembers().size()>1);
	}
}
