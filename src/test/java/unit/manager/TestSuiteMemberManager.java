package unit.manager;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.logic.tree.dao.OrganizationDAO;
import org.logic.tree.dao.criteria.MemberCriteria;
import org.logic.tree.dao.vo.Organization;
import org.logic.tree.manager.MemberManager;
import org.logic.tree.manager.custom.CustomMember;
import org.logic.tree.manager.exception.ModelException;
import org.logic.tree.manager.exception.SignUpException;
import org.logic.tree.manager.exception.UserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-configuration.xml")
@TransactionConfiguration
@Transactional
public class TestSuiteMemberManager extends AbstractTransactionalJUnit4SpringContextTests{

	@Autowired
	MemberManager memberManager;
	
	@Autowired
	OrganizationDAO organizationDao;


	@Test
	public void testSignInMemberCorrect() throws Exception{
		CustomMember user = new CustomMember();
		
		user.setMail("johndoe@buildingsoftware.com");
		user.setPasswordMember("1234");
		
		Long id = memberManager.signIn(user).getIdMember();
		Assert.isTrue(id != null);
		
		
	}
	
	@Test(expected=ModelException.class)
	public void testSignInNotMember() throws ModelException{
		CustomMember user = new CustomMember();
		user.setUserName("notMember");
		user.setPasswordMember("notMember");
		memberManager.signIn(user);
		
	}
	
	@Test
	public void testSignUpCorrect() throws Exception{
		
		CustomMember newUser = new CustomMember();
		
		newUser.setMail("nluaces@midori.com");
		newUser.setUserName("nluacesmidori");
		newUser.setPasswordMember("1234");
		
		Long idMember = memberManager.saveMemberSignUp(newUser, newUser.getUserName()).getIdMember();
		
		Assert.notNull(idMember);
		Assert.isTrue(idMember>0);
	}
	
	@Test(expected = SignUpException.class)
	public void testSignUpErrorMailAlreadyUsed() throws Exception{
		CustomMember newUser = new CustomMember();
		
		newUser.setMail("johndoe@buildingsoftware.com");
		newUser.setUserName("johndoe");
		newUser.setPasswordMember("1234");
		
		memberManager.saveMemberSignUp(newUser, newUser.getUserName());

	}
	
	@Test(expected = SignUpException.class)
	public void testSignUpErrorNameAlreadyUsed() throws Exception{
		CustomMember newUser = new CustomMember();
		
		newUser.setMail("janedoe@gmail.com");
		newUser.setUserName("janedoe");
		newUser.setPasswordMember("1234");
		
		memberManager.saveMemberSignUp(newUser, newUser.getUserName());
	}
	
	
	@Test
	public void testFindCriteriaCorrect(){
		MemberCriteria criteria = new MemberCriteria();
		
		String email = "nluaces@buildingsoftware.com";
		String userName = "nluaces";
		
		criteria.setMail(email);
		criteria.setUserName(userName);
		
		List<CustomMember> members =memberManager.findByCriteria(criteria);
		
		Assert.notNull(members);
		Assert.isTrue(email.equals(members.get(0).getMail()));
		Assert.isTrue(userName.equals(members.get(0).getUserName()));
	}
	
	@Test
	public void testFindIdTeamsByMemberIdCorrect() throws Exception{
		
		List<Long> idTeams = memberManager.findIdTeamsByMemberId((long)1);
		
		Assert.notEmpty(idTeams);
		
	}
	
	@Test(expected = UserException.class)
	public void testFindIdTeamsByMemberIdThatNotExists() throws Exception{
		memberManager.findIdTeamsByMemberId((long)666);
	}
	
	
	@Test
	public void testSignUpWithOrganizationCorrect() throws Exception{
		
		CustomMember newUser = new CustomMember();
		
		newUser.setMail("organization@gmail.com");
		newUser.setUserName("organization");
		newUser.setCompleteName("Organization Full Name");
		newUser.setPasswordMember("1234");
		newUser.setIsOrganization(true);
		
		Long idMember = memberManager.saveMemberSignUp(newUser, newUser.getUserName()).getIdMember();
		
		Assert.notNull(idMember);
		Assert.isTrue(idMember>0);
		
		MemberCriteria criteria = new MemberCriteria();
		criteria.setIdUser(idMember);
		CustomMember member = memberManager.findByCriteria(criteria).get(0);
		
		Assert.notNull(member);
		Assert.notNull(member.getOrganization());
		Assert.notNull(member.getOrganization().getIdOrganization());
		
		Organization org  = organizationDao.getOne(member.getOrganization().getIdOrganization());
		
		Assert.notNull(org);
		Assert.notNull(org.getNameOrganization());
	}
}
