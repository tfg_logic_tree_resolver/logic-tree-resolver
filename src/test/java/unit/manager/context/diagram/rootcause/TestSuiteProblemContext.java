package unit.manager.context.diagram.rootcause;

import java.io.File;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.logic.tree.manager.context.Step2;
import org.logic.tree.manager.context.Step3;
import org.logic.tree.manager.context.Step4;
import org.logic.tree.manager.context.Step5;
import org.logic.tree.manager.context.Step6;
import org.logic.tree.manager.context.Step7;
import org.logic.tree.manager.context.diagram.rootcause.DiagramStep1;
import org.logic.tree.manager.context.diagram.rootcause.DiagramStep2;
import org.logic.tree.manager.context.diagram.rootcause.DiagramStep3;
import org.logic.tree.manager.context.diagram.rootcause.DiagramStep4;
import org.logic.tree.manager.context.diagram.rootcause.DiagramStep5;
import org.logic.tree.manager.context.diagram.rootcause.DiagramStep6;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;


import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-configuration.xml")
@TransactionConfiguration
@Transactional
public class TestSuiteProblemContext extends AbstractTransactionalJUnit4SpringContextTests{

	
	@Test
	public void testRootCauseDiagram1ToDiagram2() throws Exception{
		
		
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("diagram1.json").getFile());
		
		ObjectMapper mapper = new ObjectMapper();
		
		mapper.setSerializationInclusion(Include.NON_NULL);
		
		
		DiagramStep1 diagram1 = mapper.readValue(file, DiagramStep1.class);
		
		Assert.assertNotNull(diagram1);
		
		String jsonValues = mapper.writeValueAsString(diagram1);
		
		Assert.assertNotNull(jsonValues);
		
		Step2 step2 = new Step2();
		
		jsonValues = step2.generateRootCauseDiagram(jsonValues);

		Assert.assertNotNull(jsonValues);
		
			
	}
	
	@Test
	public void testRootCauseDiagram2ToDiagram3() throws Exception{
		
		
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("diagram2.json").getFile());
		
		ObjectMapper mapper = new ObjectMapper();
		
		mapper.setSerializationInclusion(Include.NON_NULL);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		
		DiagramStep2 diagram2 = mapper.readValue(file, DiagramStep2.class);
		
		Assert.assertNotNull(diagram2);
		
		String jsonValues = mapper.writeValueAsString(diagram2);
		
		Assert.assertNotNull(jsonValues);
		
				
		Step3 step3 = new Step3();
		
		jsonValues = step3.generateRootCauseDiagram(jsonValues);

		Assert.assertNotNull(jsonValues);
		
			
	}

	
	@Test
	public void testRootCauseDiagram3ToDiagram4() throws Exception{
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("diagram3.json").getFile());
		
		ObjectMapper mapper = new ObjectMapper();
		
		mapper.setSerializationInclusion(Include.NON_NULL);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		
		DiagramStep3 diagram3 = mapper.readValue(file, DiagramStep3.class);
		
		Assert.assertNotNull(diagram3);
		
		String jsonValues = mapper.writeValueAsString(diagram3);
		
		Assert.assertNotNull(jsonValues);
		
				
		Step4 step4 = new Step4();
		
		jsonValues = step4.generateRootCauseDiagram(jsonValues);

		Assert.assertNotNull(jsonValues);
	}
	
	@Test
	public void testRootCauseDiagram4ToDiagram5() throws Exception{
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("diagram4.json").getFile());
		
		ObjectMapper mapper = new ObjectMapper();
		
		mapper.setSerializationInclusion(Include.NON_NULL);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		
		DiagramStep4 diagram4 = mapper.readValue(file, DiagramStep4.class);
		
		Assert.assertNotNull(diagram4);
		
		String jsonValues = mapper.writeValueAsString(diagram4);
		
		Assert.assertNotNull(jsonValues);
		
				
		Step5 step5 = new Step5();
		
		jsonValues = step5.generateRootCauseDiagram(jsonValues);

		Assert.assertNotNull(jsonValues);
	}
	
	
	@Test
	public void testRootCauseDiagram5ToDiagram6() throws Exception{
		
		
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("diagram5.json").getFile());
		
		ObjectMapper mapper = new ObjectMapper();
		
		mapper.setSerializationInclusion(Include.NON_NULL);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		
		DiagramStep5 diagram5 = mapper.readValue(file, DiagramStep5.class);
		
		Assert.assertNotNull(diagram5);
		
		String jsonValues = mapper.writeValueAsString(diagram5);
		
		Assert.assertNotNull(jsonValues);
		
				
		Step6 step6 = new Step6();
		
		jsonValues = step6.generateRootCauseDiagram(jsonValues);

		Assert.assertNotNull(jsonValues);
		
			
	}
	
	@Test
	public void testRootCauseDiagram6ToDiagram7() throws Exception{
		
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("diagram6.json").getFile());
		
		ObjectMapper mapper = new ObjectMapper();
		
		mapper.setSerializationInclusion(Include.NON_NULL);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		
		DiagramStep6 diagram6 = mapper.readValue(file, DiagramStep6.class);
		
		Assert.assertNotNull(diagram6);
		
		String jsonValues = mapper.writeValueAsString(diagram6);
		
		Assert.assertNotNull(jsonValues);
		
				
		Step7 step7 = new Step7();
		
		jsonValues = step7.generateRootCauseDiagram(jsonValues);

		Assert.assertNotNull(jsonValues);
		
	}
		
	
}
