package unit.manager.context.diagram.goals;


import java.io.File;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.logic.tree.manager.context.Step1;
import org.logic.tree.manager.context.Step2;
import org.logic.tree.manager.context.Step3;
import org.logic.tree.manager.context.diagram.goals.DiagramStep1;
import org.logic.tree.manager.context.diagram.goals.DiagramStep2;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-configuration.xml")
@TransactionConfiguration
@Transactional
public class TestSuiteProblemContext extends AbstractTransactionalJUnit4SpringContextTests{

	
	@Test
	public void testRootCauseDiagram1() throws Exception{
		

		
		Step1 step1 = new Step1();
		
		String jsonValues = step1.generateGoalsDiagram(null);

		Assert.assertNotNull(jsonValues);
		
			
	}
	
	
	@Test
	public void testRootCauseDiagram1ToDiagram2() throws Exception{
		
		
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("diagramgoals1.json").getFile());
		
		ObjectMapper mapper = new ObjectMapper();
		
		mapper.setSerializationInclusion(Include.NON_NULL);
		
		
		DiagramStep1 diagram1 = mapper.readValue(file, DiagramStep1.class);
		
		Assert.assertNotNull(diagram1);
		
		String jsonValues = mapper.writeValueAsString(diagram1);
		
		Assert.assertNotNull(jsonValues);
		
		Step2 step2 = new Step2();
		
		jsonValues = step2.generateGoalsDiagram(jsonValues);

		Assert.assertNotNull(jsonValues);
		
			
	}
	
	
	@Test
	public void testRootCauseDiagram2ToDiagram3() throws Exception{
		
		
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("diagramgoals2.json").getFile());
		
		ObjectMapper mapper = new ObjectMapper();
		
		mapper.setSerializationInclusion(Include.NON_NULL);
		
		
		DiagramStep2 diagram2 = mapper.readValue(file, DiagramStep2.class);
		
		Assert.assertNotNull(diagram2);
		
		String jsonValues = mapper.writeValueAsString(diagram2);
		
		Assert.assertNotNull(jsonValues);
		
		Step3 step3 = new Step3();
		
		jsonValues = step3.generateGoalsDiagram(jsonValues);

		Assert.assertNotNull(jsonValues);
		
			
	}

	
	@Test
	public void testRootCauseDiagram3ToDiagram4() throws Exception{

	}
	
	@Test
	public void testRootCauseDiagram4ToDiagram5() throws Exception{

	}
	
	
	@Test
	public void testRootCauseDiagram5ToDiagram6() throws Exception{
		
		

		
			
	}

		
	
}

