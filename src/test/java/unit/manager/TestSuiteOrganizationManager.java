package unit.manager;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.logic.tree.manager.OrganizationManager;
import org.logic.tree.manager.custom.CustomOrganization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-configuration.xml")
@TransactionConfiguration
@Transactional
public class TestSuiteOrganizationManager extends AbstractTransactionalJUnit4SpringContextTests{

	@Autowired
	OrganizationManager organizationManager;


	@Test
	public void testGetDetailCorrect() throws Exception{
		
		CustomOrganization custom = organizationManager.getDetail((long)1);
		
		Assert.notNull(custom);
		Assert.notNull(custom.getIdApplicationLanguage());
		Assert.notNull(custom.getNameOrganization());
		Assert.notEmpty(custom.getTeams());
		Assert.notEmpty(custom.getEmployees());
	}
	
	@Test
	public void testUpdateGeneralDetailCorrect() throws Exception{
		CustomOrganization custom = organizationManager.getDetail((long)1);
		
		custom.setNameOrganization("Other name");
		
		organizationManager.updateGeneralDetail(custom, "testUser");
		
		custom = organizationManager.getDetail((long)1); 
		
		Assert.notNull("Other name".equals(custom.getNameOrganization()));
		Assert.notNull(custom.getModificationDate());
		Assert.notNull(custom.getModifier());
		
	}

}
