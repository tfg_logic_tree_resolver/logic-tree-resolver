package unit.manager;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.logic.tree.manager.ProblemManager;
import org.logic.tree.manager.custom.CustomProblem;
import org.logic.tree.manager.exception.ModelException;
import org.logic.tree.manager.exception.UserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-configuration.xml")
@TransactionConfiguration
@Transactional
public class TestSuiteProblemManager extends AbstractTransactionalJUnit4SpringContextTests{
	
	@Autowired
	ProblemManager problemManager;



	@Test
    public void testCreateIssueCorrect() throws Exception{
		
		String title = "Title";
		String description = "Description";
		long type = 1;
		long idTeam = 1;
		
		CustomProblem custom = new CustomProblem(title,description,type,idTeam);
		
		
		Long idIssue = problemManager.saveIssue(custom, "johndoe");
    	    	
    	Assert.notNull(idIssue);
    	Assert.isTrue(idIssue>0);
    	
    	CustomProblem problem = problemManager.getIssueById(idIssue);
    	
    	Assert.notNull(problem);
    	Assert.isTrue(title.equals(problem.getTitle()));
    	Assert.isTrue(description.equals(problem.getDescription()));
    	Assert.isTrue(type == problem.getIdProblemType());
    	Assert.isTrue(idTeam == problem.getTeam().getId());
    	
    	
    }
	
	@Test(expected = UserException.class)
	public void testCreateIssueErrorUserDoesntExist() throws Exception{
		String title = "Title";
		String description = "Description";
		long type = 1;
		long idTeam = 1;
		
		CustomProblem custom = new CustomProblem(title,description,type,idTeam);
		
				
		problemManager.saveIssue(custom, "nonexistent");   	
    	
	}
	
	@Test(expected = UserException.class)
	public void testCreateIssueErrorUserDoesntHaveTeam() throws Exception{
		String title = "Title";
		String description = "Description";
		long type = 1;
		long idTeam = 1;
		
		CustomProblem custom = new CustomProblem(title,description,type,idTeam);
		
		
		problemManager.saveIssue(custom, "otherUser");
	}
	
	@Test(expected = UserException.class)
	public void testCreateIssueErrorUserDoesntBelongToAnyOrganization() throws Exception{
		String title = "Title";
		String description = "Description";
		long type = 1;
		long idTeam = 1;
		
		CustomProblem custom = new CustomProblem(title,description,type,idTeam);
		
		
		problemManager.saveIssue(custom, "anotherUser");
	}
	
	@Test(expected = UserException.class)
	public void testCreateIssueErrorUserDoesntHavePermissionInTeam() throws Exception{
		String title = "Title";
		String description = "Description";
		long type = 1;
		long idTeam = 1;
		
		CustomProblem custom = new CustomProblem(title,description,type,idTeam);
		
		problemManager.saveIssue(custom, "rickyGervais");
	}
    
	@Test(expected = ModelException.class)
    public void testFindAllIssuesByUserIdIncorrect() throws Exception{
		problemManager.findAllIssuesByUserId((long)666);
		
		
    }
	
	@Test
	public void testFindAllIssuesByUserIdCorrect () throws Exception{
		
		String title = "Title";
		String description = "Description";
		long type = 1;
		long idTeam = 1;
		
		CustomProblem custom = new CustomProblem(title,description,type,idTeam);
		
		
		Long idIssue = problemManager.saveIssue(custom, "johndoe");
    	    	
    	Assert.notNull(idIssue);
    	Assert.isTrue(idIssue>0);
    	
    	CustomProblem problem = problemManager.getIssueById(idIssue);
    	
    	Assert.notNull(problem);
    	Assert.isTrue(title.equals(problem.getTitle()));
    	Assert.isTrue(description.equals(problem.getDescription()));
    	Assert.isTrue(type == problem.getIdProblemType());
    	Assert.isTrue(idTeam == problem.getTeam().getId());
    	
    	List<CustomProblem> results = problemManager.findAllIssuesByUserId((long)1);
    	Assert.notNull(results);
    	
		
	}
}
