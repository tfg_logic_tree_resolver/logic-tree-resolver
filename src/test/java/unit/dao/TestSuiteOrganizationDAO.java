package unit.dao;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.logic.tree.dao.OrganizationDAO;
import org.logic.tree.dao.vo.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-configuration.xml")
@TransactionConfiguration
@Transactional
public class TestSuiteOrganizationDAO extends AbstractTransactionalJUnit4SpringContextTests{
	
	@Autowired
	OrganizationDAO organizationDAO;

	
	@Test
	public void testSaveAndGet() throws Exception{
		
		Organization transientOrganization = new Organization();
		
		transientOrganization.setIdApplicationLanguage(1);
		transientOrganization.setCreationDate(new Date());
		transientOrganization.setCreator("test");
		transientOrganization.setNameOrganization("Boogle");
		

		Organization persistedOrganization = organizationDAO.save(
				transientOrganization);

		Assert.notNull(persistedOrganization.getIdOrganization());
 
		
		Organization retrievedOrganization = organizationDAO.findOne(persistedOrganization.getIdOrganization());
		
		Assert.notNull(retrievedOrganization);
		Assert.isTrue(retrievedOrganization.getIdApplicationLanguage().equals(transientOrganization.getIdApplicationLanguage()));
		
		
	}
	
	@Test
	public void testFindAll(){
		List<Organization> list = (List<Organization>) organizationDAO.findAll();
		
		Assert.notEmpty(list);
	}

}