package unit.dao;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.logic.tree.dao.MemberDAO;
import org.logic.tree.dao.vo.Member;
import org.logic.tree.util.EncryptUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-configuration.xml")
@TransactionConfiguration
@Transactional
public class TestSuiteMemberDAO extends AbstractTransactionalJUnit4SpringContextTests{
	
	@Autowired
	MemberDAO memberDAO;

	
	@Test
	public void testSaveAndGet() throws Exception{
		
		String nameUser = "Noelia Luaces";
		String mail = "nluaces@gmail.com";
		String password = "1234";
		String userName = "nluaces";
		String color = "ffffff";
		Integer idApplicationLanguage = 1;
		Integer idMemberType = 1;
		
		
		
		Member transientMember = new Member();
		
		transientMember.setCompleteName(nameUser);
		transientMember.setMail(mail);
		transientMember.setColor(color);
		transientMember.setUserName(userName);
		transientMember.setPasswordMember(EncryptUtil.hash(password));
		transientMember.setCreator(userName);
		transientMember.setCreationDate(new Date());
		transientMember.setIdApplicationLanguage(idApplicationLanguage);
		transientMember.setIdMemberType(idMemberType);
		
		

		Member persistedMember = memberDAO.save(
				transientMember);

		Assert.notNull(persistedMember);
 
		System.out.println("Id: "+persistedMember.getIdMember());
		
		Member retrievedMember = memberDAO.findOne(persistedMember.getIdMember());
		
		Assert.notNull(retrievedMember);
		Assert.isTrue(transientMember.getCompleteName().equals(nameUser));
		Assert.isTrue(transientMember.getMail().equals(mail));
		
	}

	@Test
	public void testFindAll(){
		List<Member> list = (List<Member>) memberDAO.findAll();
		
		Assert.notEmpty(list);
	}
	

}
