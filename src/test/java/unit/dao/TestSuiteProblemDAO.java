package unit.dao;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.logic.tree.dao.ProblemDAO;
import org.logic.tree.dao.vo.Problem;
import org.logic.tree.dao.vo.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-configuration.xml")
@TransactionConfiguration
@Transactional
public class TestSuiteProblemDAO extends AbstractTransactionalJUnit4SpringContextTests{

	@Autowired
	ProblemDAO problemDAO;

	
	@Test
	public void testSaveAndGet() throws Exception{
		
		Problem trasientProblem = new Problem();
		
		trasientProblem.setCreationDate(new Date());
		trasientProblem.setCreator("nluaces");
		trasientProblem.setIdProblemType((long)1);
		trasientProblem.setDescription("Problem description");
		Team team = new Team();
		team.setId((long)1);
		trasientProblem.setTeam(team);
		trasientProblem.setTitle("title");
	
		Problem persistedProblem = problemDAO.save(trasientProblem);

		Assert.notNull(persistedProblem);
 
		
		Problem retrievedProblem = problemDAO.findOne(persistedProblem.getId());
		
		Assert.notNull(retrievedProblem);
		Assert.isTrue(retrievedProblem.getIdProblemType().equals(trasientProblem.getIdProblemType()));
		Assert.isTrue(retrievedProblem.getDescription().equals(trasientProblem.getDescription()));
		
	}
	
	@Test
	public void testFindAll() throws Exception{
		testSaveAndGet();
		List<Problem> list = (List<Problem>) problemDAO.findAll();
		
		Assert.notEmpty(list);
	}
		
	
}
