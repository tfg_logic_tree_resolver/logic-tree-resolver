package unit.dao;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.logic.tree.dao.TeamDAO;
import org.logic.tree.dao.vo.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-configuration.xml")
@TransactionConfiguration
@Transactional
public class TestSuiteTeamDAO extends AbstractTransactionalJUnit4SpringContextTests{

	@Autowired
	TeamDAO teamDAO;

	
	@Test
	public void testSaveAndGet() throws Exception{
		
		Team trasientTeam = new Team();
		
		trasientTeam.setCreationDate(new Date());
		trasientTeam.setCreator("nluaces");
		trasientTeam.setNameTeam("New Team");
		trasientTeam.setIdOrganization((long) 1);
		
		Team persistedTeam = teamDAO.save(trasientTeam);

		Assert.notNull(persistedTeam);
 
		
		Team retrievedTeam = teamDAO.findOne(persistedTeam.getId());
		
		Assert.notNull(retrievedTeam);
		Assert.isTrue(retrievedTeam.getNameTeam().equals(trasientTeam.getNameTeam()));
		
		
	}

	@Test
	public void testFindAll() throws Exception{
		testSaveAndGet();
		List<Team> list = (List<Team>) teamDAO.findAll();
		
		Assert.notEmpty(list);
	}
	
}

