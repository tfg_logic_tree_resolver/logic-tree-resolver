package unit.dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.logic.tree.dao.LanguageDAO;
import org.logic.tree.dao.vo.ApplicationLanguage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-configuration.xml")
@TransactionConfiguration
@Transactional
public class TestSuiteLanguageDAO extends AbstractTransactionalJUnit4SpringContextTests{


	@Autowired
	LanguageDAO languageDAO;

	

	@Test
	public void testSaveAndGet() throws Exception {
		ApplicationLanguage transientLanguage = new ApplicationLanguage();

		transientLanguage.setId(1);
		transientLanguage.setDescriptionApplicationLanguage("Galego");
		transientLanguage.setLocaleApplicationLanguage("gl_ES");

		ApplicationLanguage persistedLanguage = languageDAO.save(transientLanguage);

		Assert.notNull(persistedLanguage.getId());
 
		
		ApplicationLanguage retrievedLanguage = languageDAO.findOne(persistedLanguage.getId());
		
		Assert.notNull(retrievedLanguage);
		Assert.isTrue(transientLanguage.getId().equals(retrievedLanguage.getId()));
		Assert.isTrue(transientLanguage.getDescriptionApplicationLanguage().equals(retrievedLanguage.getDescriptionApplicationLanguage()));
		
		
	}
	
	@Test
	public void testFindAll() throws Exception{
		List<ApplicationLanguage> list = (List<ApplicationLanguage>) languageDAO.findAll();
		
		Assert.notEmpty(list);
	}

}
