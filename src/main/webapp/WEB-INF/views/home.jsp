<!DOCTYPE HTML>
<!--
	Ion by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
	<title>Logic Tree Resolver</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="shortcut icon" href="resources/images/favicons/bulb.png">
	<script src="resources/js/jquery.min.js"></script>
	<script src="resources/js/skel.min.js"></script>
	<script src="resources/js/skel-layers.min.js"></script>
	<script src="resources/js/init.js"></script>
</head>
<body id="top">

	<!-- Header -->
	<header id="header" class="skel-layers-fixed">
		<h1><a href="<s:url value="/home"/>">LOGIC TREE RESOLVER</a></h1>

	</header>

	<!-- Banner -->
	<section id="banner">
		<div class="inner">
			<h2>Logic Tree Resolver</h2>
			<p>A collaborative tool for solving problems.</p>
			<ul class="actions">
				<li><a href="<s:url value="/sign_up"/>" class="button big special">Register</a></li>
				<li><a href="<s:url value="/sign_in"/>" class="button big alt">Sign In</a></li>
			</ul>
		</div>
	</section>

	<!-- One -->
	<section id="one" class="wrapper style1">
		<header class="major">
			<h2>Problem solving techniques</h2>
			<p>based on <b>Ken Watanabe</b>'s book <b>Problem Solving 101</b></p>
		</header>
		<div class="container">
			<div class="row">
				<div class="4u">
					<section class="special box">
						<img src="resources/images/rootCauseIcon.png">
						<h3>Root Cause</h3>
						<p>Detect the root causes of a problem and propose solutions.</p>
					</section>
				</div>
				<div class="4u">
					<section class="special box">
						<img src="resources/images/goalFishingIcon.png">
						<h3>Goal Fishing</h3>
						<p>Set a goal and achieve it with possible solutions.</p>
					</section>
				</div>
				<div class="4u">
					<section class="special box">
						<img src="resources/images/prosConsIcon.png">
						<h3>Pros and Cons</h3>
						<p>Evaluations of pros and cons to choose the best solution.</p>
					</section>
				</div>
			</div>
		</div>
	</section>
	
	<!-- Two -->
	<section id="two" class="wrapper style2">
		<header class="major">
			<h2>Teamwork</h2>
			<p>Solving problems with everyone</p>
		</header>
		<div class="container">
			<div class="row">
				<div class="6u">
					<section class="special">
						<a href="#" class="image fit"><img src="resources/images/communicate.jpg" alt="" /></a>
						<h3>Communicate</h3>
						<p>In meetings, communication is often ineffective: depending on the problem's complexity, not everyone is able to understand it without visualizing it. Moreover, most influential people can impose their decisions and not let express other ideas.</p>
					</section>
				</div>
				<div class="6u">
					<section class="special">
						<a href="#" class="image fit"><img src="resources/images/achieving.jpg" alt="" /></a>
						<h3>Achieving</h3>
						<p>All team members can participate until reaching solution. Another team can see the problem and provide or suggest a solution of their projects.</p>

					</section>
				</div>
			</div>
		</div>
	</section>

	<!-- Three -->
	<section id="three" class="wrapper style1">

	</section>			
	
	<!-- Footer -->
	<footer id="footer">
		<div class="container">
			<div class="row double">
				
			</div>
			<ul class="copyright">
				<li>Creative Commons Attribution 3.0 license.</li>
				<li>Design: <a href="http://templated.co">TEMPLATED</a></li>
			</ul>
		</div>
	</footer>
</body>
</html>