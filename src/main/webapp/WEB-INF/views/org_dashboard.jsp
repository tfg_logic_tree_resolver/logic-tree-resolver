<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html class="no-focus">

<head>
	<meta charset="utf-8">
	<title><s:message code="dashboard.organization"/></title>
	
	<link rel="shortcut icon" href="resources/images/favicons/bulb.png">
	
	<link rel="stylesheet"	href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
	<link rel="stylesheet" href="resources/css/bootstrap.min.css">
	<link rel="stylesheet" id="css-main" href="resources/css/oneui.min.css">
	<link rel="stylesheet" id="css-theme" href="resources/css/themes/amethyst.min.css">
</head>
<body>
	<div id="page-container"
	class="sidebar-l sidebar-mini sidebar-o side-scroll header-navbar-fixed header-navbar-transparent">
		
		<main id="main-container"> 
			<div class="content content-boxed">
				<!-- Organization Header -->
				<div class="block">
					<!-- Basic Info -->
					<div class="bg-image" style="background-image: url('resources/images/photos/pexels-photo-245618.jpeg');">
						<div class="block-content bg-primary-op text-center overflow-hidden">
							<div class="push-30-t push animated fadeInDown">
								<img class="img-avatar img-avatar96 img-avatar-thumb" src="resources/images/icons/building-icon.png" alt="">
							</div>
							<div class="push-30 animated fadeInUp">
								<h2 class="h4 font-w600 text-white push-5">${org.nameOrganization}</h2>
							</div>
						</div>
					</div>
					<!-- END Basic Info -->
					<!-- Stats -->
					<div class="block-content text-center">
						<div class="row items-push text-uppercase">
							<div class="col-xs-6 col-sm-3">
								<div class="font-w700 text-gray-darker animated fadeIn"><s:message code="dashboard.organization.members"/></div>
								<a class="h2 font-w300 text-primary animated flipInX"
								href="javascript:void(0)">${org.numberEmployees}</a>
							</div>
							<div class="col-xs-6 col-sm-3">
								<div class="font-w700 text-gray-darker animated fadeIn"><s:message code="dashboard.organization.teams"/></div>
								<a class="h2 font-w300 text-primary animated flipInX"
								href="javascript:void(0)">${org.numberTeams}</a>
							</div>
							<div class="col-xs-6 col-sm-3">
								<div class="font-w700 text-gray-darker animated fadeIn"><s:message code="dashboard.organization.problems"/></div>
								<a class="h2 font-w300 text-primary animated flipInX"
								href="javascript:void(0)">${org.numberProblems}</a>
							</div>
							<div class="col-xs-6 col-sm-3">
								<div class="font-w700 text-gray-darker animated fadeIn"><s:message code="dashboard.organization.resolved"/></div>
								<a class="h2 font-w300 text-primary animated flipInX"
								href="javascript:void(0)">${org.numberResolvedProblems}</a>
							</div>
						</div>
					</div>
					<!-- END Stats -->
				</div>
				<!-- END Organization Header -->

				<!-- Main Content -->
				<div class="block">
					<ul class="nav nav-tabs nav-justified push-20" data-toggle="tabs">
						<li ${"tab-profile-general" == selectedTab ? 'class="active"' : ''}>
							<a href="#tab-profile-general"><i class="fa fa-fw fa-pencil"></i><s:message code="dashboard.organization.generalinformation"/></a>
						</li>
						<li ${"tab-profile-employees" == selectedTab ? 'class="active"' : ''}>
							<a href="#tab-profile-employees"><i class="fa fa-fw fa-briefcase"></i><s:message code="dashboard.organization.members"/></a>
						</li>
						<li ${"tab-profile-teams" == selectedTab ? 'class="active"' : ''}>
							<a href="#tab-profile-teams"><i class="fa fa-fw fa-group"></i><s:message code="dashboard.organization.teams"/></a>
						</li>
					</ul>
					<div class="block-content tab-content">
						<!-- General Information Tab -->			
						<jsp:include page="org_dashboard/org_dashboard_tab_general.jsp" /> 
					
						<!-- Employees Tab -->
						<jsp:include page="org_dashboard/org_dashboard_tab_employees.jsp" /> 
				
						<!-- Teams Tab -->
						<jsp:include page="org_dashboard/org_dashboard_tab_team.jsp" /> 
					</div>		
				</div>			
			</div>		
			<!-- END Main Content -->
		</main> 
		
		<jsp:include page="user_menu.jsp" /> 

		<!-- Footer -->
		<footer id="page-footer" class="bg-white">
			<div class="content content-boxed">
				<!-- Footer Navigation -->
				<div class="row push-30-t items-push-2x"></div>
				<!-- END Footer Navigation -->
			</div>
		</footer> 
		<!-- END Footer -->
	</div>
	<script src="resources/js/oneui.min.js"></script>
	<script src="resources/js/org_dashboard.js"></script>
	<script src="resources/js/core/jquery.min.js"></script>
    <script src="resources/js/core/bootstrap.min.js"></script>
    <script src="resources/js/core/jquery.slimscroll.min.js"></script>
    <script src="resources/js/core/jquery.scrollLock.min.js"></script>
    <script src="resources/js/core/jquery.appear.min.js"></script>
    <script src="resources/js/core/jquery.countTo.min.js"></script>
    <script src="resources/js/core/jquery.placeholder.min.js"></script>
    <script src="resources/js/core/js.cookie.min.js"></script>
    <script src="resources/js/app.js"></script>
</body>
</html>