<!DOCTYPE html>

<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<html >
<head>
	<meta charset="UTF-8">
	<title>Sign in</title>
	<link rel="stylesheet" href="resources/css/sign_in_style.css">
	<link rel="shortcut icon" href="resources/images/favicons/favicon.png">
</head>
<body>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<div id="login">
		<sf:form method="POST" modelAttribute="customMember" id="signin-form" >
			<sf:errors path="*"  element="p" />
			<span class="fontawesome-user"></span>
			<sf:input path="mail" id="user_email" type="text" value="Enter your email" onBlur="if(this.value=='')this.value='Enter your email'" onFocus="if(this.value=='Enter your email')this.value='' "/> 
			<span class="fontawesome-lock"></span>
			<sf:input path="passwordMember" id="user_password" type="password" value="Password" onBlur="if(this.value=='')this.value='Password'" onFocus="if(this.value=='Password')this.value='' "/>
			<input type="submit" value="Sign in" class="button alt">
			<a href="<s:url value="/sign_up"/>" >Not a member?</a>
		</sf:form>
	</div> 
</body>	
</html>
