<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">

        <title>Whoops!</title>


        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">



        <!-- Stylesheets -->
        <!-- Web fonts -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

        <!-- Bootstrap and OneUI CSS framework -->
        <link rel="stylesheet" href="resources/css/bootstrap.min.css">
        <link rel="stylesheet" id="css-main" href="resources/css/oneui.css">

        <!-- END Stylesheets -->
        <style>
body {
    background-image: url("resources/images/repairing.jpg");
    
    }
    .transparent {
		  background-color: rgba(255, 255, 255, 0.75);
	}
	


</style>

    </head>
    <body>
        <!-- Error Content -->

        <div class="content transparent text-center pulldown overflow-hidden">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <!-- Error Titles -->
                    <h1 class="font-s128 font-w300 text-modern animated zoomInDown">Whoops!</h1>
                    <h2 class="h3 font-w300 push-50 animated fadeInUp">Sorry! Something went wrong...</h2>
                    <!-- END Error Titles -->

                </div>
            </div>
        </div>
        <!-- END Error Content -->

        <!-- Error Footer -->
        <div class="content transparent pulldown text-muted text-center">
            Would you like to let us know about it?<br>
            <a class="link-effect" href="mailto:nluaces@gmail.com">Report it</a> or <a class="link-effect" href="<s:url value="/home"/>">Go Back to Home</a>
        </div>
        <!-- END Error Footer -->
		
        <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
        <script src="resources/js/core/jquery.min.js"></script>
        <script src="resources/js/core/bootstrap.min.js"></script>
        <script src="resources/js/core/jquery.slimscroll.min.js"></script>
        <script src="resources/js/core/jquery.scrollLock.min.js"></script>
        <script src="resources/js/core/jquery.appear.min.js"></script>
        <script src="resources/js/core/jquery.countTo.min.js"></script>
        <script src="resources/js/core/jquery.placeholder.min.js"></script>
        <script src="resources/js/core/js.cookie.min.js"></script>
        <script src="resources/js/app.js"></script>
    </body>
</html>