<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html class="no-focus">

<head>
	<meta charset="utf-8">
	<title><s:message code="problem.pros_cons"/></title>
	
	<link rel="shortcut icon" href="resources/images/favicons/bulb.png">
	
	<link rel="stylesheet"	href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
	<link rel="stylesheet" href="resources/css/bootstrap.min.css">
	<link rel="stylesheet" id="css-main" href="resources/css/oneui.min.css">
	<link rel="stylesheet" id="css-theme" href="resources/css/themes/amethyst.min.css">
	
	<script src="//cdn.jsdelivr.net/sockjs/1.1.4/sockjs.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.js"></script>
	<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
</head>
<body onload="initProsCons()">

	<input type=hidden id="idProblem" name="idProblem" value="${idProblem}"/>
	<input type=hidden id="idStatus" name="idStatus" value="${activeStatus}"/>
	<input type=hidden id="userName" name="userName" value="${member.userName}"/>
	<input type=hidden id="title" name="title" value="${title}"/>
	<input type=hidden id="memberCanEdit" name="title" value="${memberCanEdit}"/>
	
	
	<div id="page-container"
	class="sidebar-l sidebar-mini sidebar-o side-scroll header-navbar-fixed header-navbar-transparent">
		
		<main id="main-container"> 
			<div class="content content-boxed">
				<!-- Root Cause Header -->
				<div class="block">
					<!-- Basic Info -->
					<div class="bg-image" style="background-image: url('resources/images/photos/pexels-photo-383838.jpeg');">
						<div class="block-content bg-primary-op text-center overflow-hidden">
							<div class="push-30 animated fadeInUp">
								<h2 class="h4 font-w600 text-white push-5">${title}</h2>
							</div>
						</div>
					</div>
					<!-- END Basic Info -->
					<!-- Stats -->
					<div class="block-content text-center">
						${description}
					</div>
					<!-- END Stats -->
				</div>
				<!-- END Root Cause Header -->

				<!-- Main Content -->
                <div class="js-wizard-simple block">
 	               <!-- Step Tabs -->
                   <ul class="nav nav-tabs nav-justified">
    	               <li ${activeStatus == "1" ? 'class="active"' : ''}>
        	     	      <a class="inactive" href="#pros_cons-step1" data-toggle="tab"><s:message code="problem.pros_cons.step_1"/></a>
                       </li>
                       <li ${activeStatus == "2" ? 'class="active"' : ''}>
            	           <a class="inactive" href="#pros_cons-step2" data-toggle="tab"><s:message code="problem.pros_cons.step_2"/></a>
                       </li>
                       <li ${activeStatus == "3" ? 'class="active"' : ''}>
                    	   <a class="inactive" href="#pros_cons-step3" data-toggle="tab"><s:message code="problem.pros_cons.step_3"/></a>
                       </li>
                   </ul>
                   <!-- END Step Tabs -->

                   <!-- Form -->
                   <sf:form method="POST" action="${pageContext.request.contextPath}/problem_solving/working_problem" modelAttribute="customProblem" id="problem-form1" onsubmit="disconnectDiagramProscons()">
                   
                   	   <!-- Steps Progress -->
                       <div class="block-content block-content-mini block-content-full border-b">
                       	   <div class="wizard-progress progress progress-mini remove-margin-b">
                           	   <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 0"></div>
                           </div>
                       </div>
                       <!-- END Steps Progress -->

                       <!-- Steps Content -->
                       <div class="block-content tab-content">
                       	   <!-- Step 1 -->
                           <div ${activeStatus == "1" ? 'class="tab-pane fade fade-up in push-30-t push-50 active"' : 'class="tab-pane fade fade-up in push-30-t push-50"'}  id="simple-classic-progress-step1">
								<jsp:include page="pros_cons/pros_cons_step_1.jsp" /> 
                           </div>
                           <!-- END Step 1 -->
                           <!-- Step 2 -->
                           <div ${activeStatus == "2" ? 'class="tab-pane fade fade-up in push-30-t push-50 active"' : 'class="tab-pane fade fade-up in push-30-t push-50"'} id="simple-classic-progress-step2">
                           		<jsp:include page="pros_cons/pros_cons_step_2.jsp" /> 
                           </div>
                           <!-- END Step 2 -->
                           <!-- Step 3 -->
                           <div ${activeStatus == "3" ? 'class="tab-pane fade fade-up in push-30-t push-50 active"' : 'class="tab-pane fade fade-up in push-30-t push-50"'} id="simple-classic-progress-step3">
                           		<jsp:include page="pros_cons/pros_cons_step_3.jsp" /> 
                           </div>
                           <!-- END Step 3 -->
                       </div>

                       <!-- END Steps Content -->
                       <!-- Steps Navigation -->

                       <div class="block-content block-content-mini block-content-full border-t">
                       	   <div class="row">
                           		<div class="col-xs-6">
                            	    <button class="wizard-prev btn btn-default" type="submit" name="action" value="previous"><i class="fa fa-arrow-left"></i> <s:message code="problem.previous"/></button>
                                </div>
                                <div class="col-xs-6 text-right">
                                	<button class="wizard-next btn btn-default" type="submit" name="action" value="next"> <s:message code="problem.next"/><i class="fa fa-arrow-right"> </i></button>
                                    <button class="wizard-finish btn btn-primary" type="submit" name="action" value="resolve"><i class="fa fa-check"></i> <s:message code="problem.resolve"/></button>
                                </div>
                           </div>
                       </div>
                       <!-- END Steps Navigation -->
                       <div class="block-content block-content-mini block-content-full border-t" ${memberCanEdit == "false" ? 'style="display: none;"' : ''}>
	                   		<div class="col-xs-8">
					        	<div class="font-s13 font-w600"><s:message code="problem.reset"/></div>
					            <div class="font-s13 font-w400 text-muted"><s:message code="problem.reset.caution"/></div>
					        </div>
					        <div class="col-xs-1">
					        	<label class="css-input switch switch-sm switch-primary push-10-t">
					            	<input type="checkbox" id="redo_diagram" name="redo_diagram"><span></span>
					            </label>
					        </div>
                       </div>
                 	
                   </sf:form>
                   <jsp:include page="suggestion_timeline.jsp" />
				</div>			
			</div>		
			<!-- END Main Content -->
		</main> 
		
		<jsp:include page="user_menu.jsp" /> 

		<!-- Footer -->
		<footer id="page-footer" class="bg-white">
			<div class="content content-boxed">
				<!-- Footer Navigation -->
				<div class="row push-30-t items-push-2x"></div>
				<!-- END Footer Navigation -->
			</div>
		</footer> 
		<!-- END Footer -->
	</div>
	<script src="resources/js/oneui.min.js"></script>
	
	<!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
    <script src="resources/js/core/jquery.min.js"></script>
    <script src="resources/js/core/bootstrap.min.js"></script>
    <script src="resources/js/core/jquery.slimscroll.min.js"></script>
    <script src="resources/js/core/jquery.scrollLock.min.js"></script>
    <script src="resources/js/core/jquery.appear.min.js"></script>
    <script src="resources/js/core/jquery.countTo.min.js"></script>
    <script src="resources/js/core/jquery.placeholder.min.js"></script>
    <script src="resources/js/core/js.cookie.min.js"></script>
    <script src="resources/js/app.js"></script>
    <script src="resources/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
    <script src="resources/js/plugins/jquery-validation/jquery.validate.min.js"></script>

    <!-- Page JS Code -->
    <script src="resources/js/pages/base_forms_wizard.js"></script>
    
    <!--  GoJS -->
    <script src="resources/js/go/go-debug.js"></script>
    <script src="https://gojs.net/latest/extensions/TableLayout.js"></script>
    
    <script src="resources/js/pros_cons/step1.js"></script>
    <script src="resources/js/pros_cons/step2.js"></script>
    <script src="resources/js/pros_cons/step3.js"></script>
    <script src="resources/js/pros_cons/pros_cons.js"></script>

</body>
</html>