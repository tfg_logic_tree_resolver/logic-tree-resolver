<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html class="no-focus">

<head>
	<meta charset="utf-8">
	<title><s:message code="dashboard.member"/></title>
	<meta ">
	<link rel="shortcut icon" href="resources/images/favicons/bulb.png">
	
	<link rel="stylesheet"	href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
	<link rel="stylesheet" href="resources/css/bootstrap.min.css">
	<link rel="stylesheet" id="css-main" href="resources/css/oneui.min.css">
	<link rel="stylesheet" id="css-theme" href="resources/css/themes/amethyst.min.css">
</head>
<body>
	<div id="page-container"
	class="sidebar-l sidebar-mini sidebar-o side-scroll header-navbar-fixed header-navbar-transparent">	
		<main id="main-container"> 
			<div class="content content-boxed">
				<!-- Member Header -->
				<div class="block">
					<!-- Basic Info -->
					<div class="bg-image"
						style="background-image: url('resources/images/photos/pexels-photo-271560.jpeg');">
						<div
							class="block-content bg-primary-op text-center overflow-hidden">
							<div class="push-30-t push animated fadeInDown">
								<img class="img-avatar img-avatar96 img-avatar-thumb"
									src="resources/images/avatars/avatar1.png" alt="">
							</div>
							<div class="push-30 animated fadeInUp">
								<h2 class="h4 font-w600 text-white push-5"><s:message code="dashboard.member.hello"/> ${member.completeName} </h2>
							</div>
						</div>
					</div>
					<!-- END Basic Info -->
				</div>
				<!-- END Member Header -->

				<!-- Main Content -->
					
				<!-- Full Table Working problems-->
                <div class="block">
					<div class="block-header">
						<h3 class="block-title"><s:message code="dashboard.member.workingon"/></h3>
					</div>
					<div class="block-content">
						<div class="table-responsive">
							<table class="table table-striped table-vcenter">
								<thead>
									<tr>
										<th style="width: 20%;"><s:message code="dashboard.member.table.title"/></th>
										<th style="width: 20%;"><s:message code="dashboard.member.table.description"/></th>
										<th style="width: 10%;"><s:message code="dashboard.member.table.type"/></th>
										<th style="width: 15%;"><s:message code="dashboard.member.table.team"/></th>
										<th style="width: 10%;"><s:message code="dashboard.member.table.status"/></th>
										<th style="width: 20%;"><s:message code="dashboard.member.table.creationdate"/></th>
										<th style="width: 20%;"><s:message code="dashboard.member.table.lastmodification"/></th>
										<th class="text-center" style="width: 100px;"><s:message code="dashboard.member.table.actions"/></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${problems}" var="problem">
										<tr>
											<td class="font-w600"><c:out value = "${problem.title}"/></td>
											<td class="font-w600"><c:out value = "${problem.description}"/></td>
											<td>
												<c:if test = "${problem.idProblemType == typerootcause}">
											        <span class="label label-primary">
											        	<c:out value = "Root Cause"/>
											        </span>
											    </c:if>
											    <c:if test = "${problem.idProblemType == typegoals}">
											        <span class="label label-info">
											        	<c:out value = "Goals"/>
											        </span>
											    </c:if>
											    <c:if test = "${problem.idProblemType == typeproscons}">
											        <span class="label label-warning">
											        	<c:out value = "Pros & Cons"/>
											        </span>
											    </c:if>											    
											</td>
											<td><c:out value = "${problem.team.nameTeam}"/></td>
											<td>
												<c:if test = "${problem.problemResolved == true}">
											        <span class="label label-success">
											        	<s:message code="dashboard.member.table.status.resolved"/>
											        </span>
											    </c:if>
											    <c:if test = "${problem.problemResolved == false}">
											        <span class="label label-danger">
											        	<s:message code="dashboard.member.table.status.notresolved"/>
											        </span>
											    </c:if>
											</td>
											<td><fmt:formatDate value="${problem.creationDate}" pattern="dd-MM-yyyy HH:mm:ss" /></td>
											<td><fmt:formatDate value="${problem.modificationDate}" pattern="dd-MM-yyyy HH:mm:ss" /></td>
											<td class="text-center">
												<div class="btn-group">
													<form style="display: inline" action="problem_solving" method="get">
														<input type="hidden" id="id" name="id" value="${problem.id}"/>
														<input type="hidden" id="type" name="type" value="${problem.idProblemType}"/>
		  												<button class="btn btn-xs btn-default" type="submit"
		  												
															data-toggle="tooltip" title="<s:message code='dashboard.member.table.work_on_it'/>">
															<i class="fa fa-wrench"></i>
														</button>
													</form>
													<form style="display: inline" action="problem_solving" method="get">
														<input type="hidden" id="id" name="id" value="${problem.id}"/>
														<input type="hidden" id="type" name="type" value="${problem.idProblemType}"/>
														<input type="hidden" id="fromStart" name="fromStart" value="true"/>
		  												<button class="btn btn-xs btn-default" type="submit"
		  												
															data-toggle="tooltip" title="<s:message code='dashboard.member.table.work_on_it.from_start'/>">
															<i class="fa fa-fast-backward"></i>
														</button>
													</form>
												</div>
											</td>
										</tr>		
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
	            <!-- END Full Table -->

				<div class="block">
					<div class="block-header">
						<h3 class="block-title"><s:message code="dashboard.member.newproblem"/></h3>
					</div>
					<div class="row items-push">
						<div class="col-sm-6 col-sm-offset-3 form-horizontal">
							<sf:form class="form-horizontal push-10-t"
								method="POST"
								action="${pageContext.request.contextPath}/user_dashboard/save_problem"
								 id="member-form1"
								 modelAttribute="customProb">
								
								<div class="form-group">
									<div class="col-xs-6">
										<div class="form-material">
											<div class="form-material floating">
												<sf:input 
												class="form-control" type="text"
												id="problem-title"
												name="problem-title"
												path="title"
												onchange="checkValidDataNewProblem()"/>
												<label for="problem-title"><s:message code="dashboard.member.newproblem.title"/></label>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-xs-6">
										<div class="form-material">
											<div class="form-material floating">
												<sf:textarea class="form-control" 
												id="problem-description" 
												name="problem-description" 
												rows="3"
												path="description"
												onchange="checkValidDataNewProblem()"/>
												<label for="problem-description"><s:message code="dashboard.member.newproblem.description"/></label>
											</div>
	
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-3">
										<div class="form-material">
											<sf:select class="form-control" id="problem-type" name="problem-type" path="idProblemType" onchange="checkValidDataNewProblem()">
												<option value=""><s:message code="dashboard.member.newproblem.type"/></option>
												<c:forEach var="item" items="${problemTypes}">
													<option value="${item.key}">${item.value}</option>
												</c:forEach>
											</sf:select>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-material">
											<sf:select class="form-control" id="problem_team" name="problem_team" path="team.id" onchange="checkValidDataNewProblem()">
												<option value=""><s:message code="dashboard.member.newproblem.team"/></option>
												<c:forEach var="item" items="${teams}">
													<option value="${item.id}">${item.nameTeam}</option>
												</c:forEach>
											</sf:select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-xs-3">
										<button class="btn btn-sm btn-primary" type="submit" id="createProblemButton" disabled><s:message code="dashboard.member.newproblem.create"/></button>
									</div>
								</div>
							</sf:form>
						</div>
					</div>
				</div>

				<!-- Full Table -->
				<div class="block">
					<div class="block-header">
						<h3 class="block-title"><s:message code="dashboard.member.opentosuggestions"/></h3>
					</div>
					<div class="block-content">
						<div class="table-responsive">
							<table class="table table-striped table-vcenter">
								<thead>
									<tr>
										<th style="width: 20%;"><s:message code="dashboard.member.table.title"/></th>
										<th style="width: 20%;"><s:message code="dashboard.member.table.description"/></th>
										<th style="width: 10%;"><s:message code="dashboard.member.table.type"/></th>
										<th style="width: 15%;"><s:message code="dashboard.member.table.team"/></th>
										<th style="width: 10%;"><s:message code="dashboard.member.table.status"/></th>
										<th style="width: 20%;"><s:message code="dashboard.member.table.creationdate"/></th>
										<th style="width: 20%;"><s:message code="dashboard.member.table.lastmodification"/></th>
										<th class="text-center" style="width: 100px;"><s:message code="dashboard.member.table.actions"/></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${problemstosuggest}" var="problem">
										<tr>
											<td class="font-w600"><c:out value = "${problem.title}"/></td>
											<td class="font-w600"><c:out value = "${problem.description}"/></td>
											<td>
												<c:if test = "${problem.idProblemType == typerootcause}">
											        <span class="label label-primary">
											        	<c:out value = "Root Cause"/>
											        </span>
											    </c:if>
											    <c:if test = "${problem.idProblemType == typegoals}">
											        <span class="label label-info">
											        	<c:out value = "Goals"/>
											        </span>
											    </c:if>
											    <c:if test = "${problem.idProblemType == typeproscons}">
											        <span class="label label-danger">
											        	<c:out value = "Pros & Cons"/>
											        </span>
											    </c:if>											    
											</td>
											<td><c:out value = "${problem.team.nameTeam}"/></td>
											<td>
												<c:if test = "${problem.problemResolved == true}">
											        <span class="label label-success">
											        	<s:message code="dashboard.member.table.status.resolved"/>
											        </span>
											    </c:if>
											    <c:if test = "${problem.problemResolved == false}">
											        <span class="label label-danger">
											        	<s:message code="dashboard.member.table.status.notresolved"/>
											        </span>
											    </c:if>
											</td>
											<td><fmt:formatDate value="${problem.creationDate}" pattern="dd-MM-yyyy HH:mm:ss" /></td>
											<td><fmt:formatDate value="${problem.modificationDate}" pattern="dd-MM-yyyy HH:mm:ss" /></td>
											<td class="text-center">
												<div class="btn-group">
													<form style="display: inline" action="problem_solving" method="get">
														<input type="hidden" id="id" name="id" value="${problem.id}"/>
														<input type="hidden" id="type" name="type" value="${problem.idProblemType}"/>
		  												<button class="btn btn-xs btn-default" type="submit"
															data-toggle="tooltip" title="<s:message code='dashboard.member.table.work_on_it'/>">
															<i class="fa fa-comment-o"></i>
														</button>
													</form>
													<form style="display: inline" action="problem_solving" method="get">
														<input type="hidden" id="id" name="id" value="${problem.id}"/>
														<input type="hidden" id="type" name="type" value="${problem.idProblemType}"/>
														<input type="hidden" id="fromStart" name="fromStart" value="true"/>
		  												<button class="btn btn-xs btn-default" type="submit"
		  												
															data-toggle="tooltip" title="<s:message code='dashboard.member.table.work_on_it.from_start'/>">
															<i class="fa fa-fast-backward"></i>
														</button>
													</form>
												</div>
											</td>
										</tr>		
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- END Full Table -->
				
			<!-- END Main Content -->
			</div>
		</main> 
		
		<jsp:include page="user_menu.jsp" /> 

		<!-- Footer -->
		<footer id="page-footer" class="bg-white">
			<div class="content content-boxed">
				<!-- Footer Navigation -->
				<div class="row push-30-t items-push-2x"></div>
				<!-- END Footer Navigation -->
			</div>
		</footer> 
		<!-- END Footer -->
	</div>
	
	<script src="resources/js/user_dashboard.js"></script>
	<script src="resources/js/oneui.min.js"></script>
	<script src="resources/js/core/jquery.min.js"></script>
    <script src="resources/js/core/bootstrap.min.js"></script>
    <script src="resources/js/core/jquery.slimscroll.min.js"></script>
    <script src="resources/js/core/jquery.scrollLock.min.js"></script>
    <script src="resources/js/core/jquery.appear.min.js"></script>
    <script src="resources/js/core/jquery.countTo.min.js"></script>
    <script src="resources/js/core/jquery.placeholder.min.js"></script>
    <script src="resources/js/core/js.cookie.min.js"></script>
    <script src="resources/js/app.js"></script>
</body>
</html>