<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<textarea id="savedModelStep7" style="display:none">${savedModelStep}</textarea>
	<div class="tab-panel" id="tab-root-cause-step4">
		<div class="row items-push">  
			<span>
				 <h2 class="font-w300 push"><s:message code="problem.root_cause.step_7.title"/></h2>
        	</span>
	      	<div id="myDiagramDiv7" style="border: solid 1px black; height: 500px"></div>
    		
	    </div>
	</div>
</body>
</html>