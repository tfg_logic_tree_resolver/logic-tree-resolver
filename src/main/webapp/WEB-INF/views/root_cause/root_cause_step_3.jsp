<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>

	<textarea id="savedModelStep3" style="display: none">${savedModelStep}</textarea>


	<div class="tab-panel" id="tab-root-cause-step3">

		<div class="row items-push">
			<span>
				 <h2 class="font-w300 push"><s:message code="problem.root_cause.step_3.title"/></h2>
        	</span>
			<span style="display: inline-block; vertical-align: top; width: 100px">
				<div id="myPaletteDiv3" style="border: solid 1px black; height: 500px"></div>
			</span> 
			<span style="display: inline-block; vertical-align: top; width: 80%">
				<div id="myDiagramDiv3"	style="border: solid 1px black; height: 500px"></div>
			</span>
			<div class="block-content block-content-mini block-content-full border-t">
	        	<div class="col-xs-8">			
					<div class="font-s13 font-w600 text-muted"><s:message code="problem.root_cause.step_3.instructions"/></div>
				</div>
            </div>
		</div>

	</div>
</body>
</html>