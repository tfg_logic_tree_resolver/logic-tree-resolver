<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<div class="tab-pane fade ${'tab-profile-general' == selectedTab ? 'in active' : ''}" id="tab-profile-general">
		<div class="row items-push">
			<div class="col-sm-6 col-sm-offset-3 form-horizontal">
				<sf:form method="POST" action="${pageContext.request.contextPath}/org_dashboard/save_general"	modelAttribute="customOrg" id="org-form1">
					<sf:errors path="*" element="p" />
					<div class="form-group">
						<div class="col-xs-12">
							<label><s:message code="dashboard.organization.generalinformation.name"/></label>
							<sf:input path="nameOrganization"
							class="form-control input-lg" type="text"
							id="organization-name"
							placeholder="Enter the name of your organization"
							value="${org.nameOrganization}" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<label for="organization-language"><s:message code="dashboard.organization.generalinformation.language"/></label>
							<sf:select class="form-control" id="organization-language"
							path="idApplicationLanguage" size="3">
								<c:forEach var="item" items="${languages}">
									<option value="${item.key}"	${item.key == selectedLang ? 'selected="selected"' : ''}>${item.value}</option>
								</c:forEach>
							</sf:select>
						</div>
					</div>
					<div class="block-content block-content-full bg-gray-lighter text-center">
						<input type="submit" value="Save Changes" class="btn btn-sm btn-primary"/>
					</div>
				</sf:form>
			</div>
		</div>
	</div>
</body>
</html>