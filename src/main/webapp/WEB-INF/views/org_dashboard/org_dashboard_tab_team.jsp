<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<div class="tab-pane fade ${'tab-profile-teams' == selectedTab ? 'in active' : ''}" id="tab-profile-teams">
		<div class="row items-push">
			<div class="col-sm-6 col-sm-offset-3 form-horizontal">
				<sf:form method="POST" action="${pageContext.request.contextPath}/org_dashboard/save_team" modelAttribute="customOrg" id="org-form4">
					<div class="form-group">
						<div class="col-xs-6">
							<input class="form-control input-lg" type="text"
							id="team-name" name="team-name"
							placeholder="Enter the name of the new team"/>
						</div>
						<div class="col-xs-6">
							<label
							class="css-input switch switch-sm switch-primary push-10-t">
								<button class="btn btn-sm btn-primary" type="submit">
									<i class="fa fa-check push-5-r"></i><s:message code="dashboard.organization.teams.create"/>
								</button>
							</label>
						</div>
					</div>
				</sf:form>
				<sf:form method="POST" action="${pageContext.request.contextPath}/org_dashboard/update_teams"	modelAttribute="customOrg" id="org-form5">
					<div class="form-group">
						<div class="col-xs-6">
							<label for="organization-teams">P<s:message code="dashboard.organization.teams.pick"/></label>
							<select class="form-control" id="organization-teams" name="organization-teams" size="5" onchange="showEnrolledMembers()">
								<c:forEach items="${org.teams}" var="team">
									<option value="${team.id}">${team.id}) ${team.nameTeam}</option>
								</c:forEach>
							</select>
						</div>
						<div class="col-xs-6">
							<label for="organization-teams"><s:message code="dashboard.organization.teams.enrolled"/></label>
							<select class="form-control" id="organization-team-members"	name="organization-team-members" size="5" multiple="multiple">
								<c:forEach items="${org.teams}" var="team">
									<c:forEach items="${team.mailMembers}" var="mailMember">
										<option disabled="disabled" value="${team.id}">${mailMember}
										</option>
									</c:forEach>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<label for="organization-team-employees"><s:message code="dashboard.organization.teams.pickmembers"/></label>
							<select class="form-control" id="organization-team-employees" name="organization-team-employees" size="10"
							multiple="multiple">
								<c:forEach items="${org.employees}" var="employee">
									<option value="${employee.idMember}">${employee.mail}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="block-content block-content-full bg-gray-lighter text-center">
						<button class="btn btn-sm btn-primary" type="submit" name="action" value="join">
							<i class="fa fa-check push-5-r"></i><s:message code="dashboard.organization.teams.join"/>
						</button>
						<button class="btn btn-sm btn-primary" type="submit" name="action" value="leave">
							<i class="fa fa-check push-5-r"></i><s:message code="dashboard.organization.teams.leave"/>
						</button>
					</div>
				</sf:form>
			</div>
		</div>
	</div>
</body>
</html>