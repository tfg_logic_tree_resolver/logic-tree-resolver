<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body>
	<div class="tab-pane fade ${'tab-profile-employees' == selectedTab ? 'in active' : ''}" id="tab-profile-employees">
		<div class="row items-push">
			<div class="col-sm-6 col-sm-offset-3 form-horizontal">
				<sf:form method="POST" action="${pageContext.request.contextPath}/org_dashboard/search_employees"	modelAttribute="customOrg" id="org-form2">
					<sf:errors path="*" element="p" />
					<div class="form-group">
						<div class="col-xs-12">
							<label><s:message code="dashboard.organization.members.domain"/></label>
							<sf:input path="domain"
							class="form-control input-lg" type="text"
							id="organization-domain"
							placeholder="Enter the domain of your organization"
							value="${org.domain}" />
						</div>
						<div class="col-xs-12 text-right">
							<label class="css-input switch switch-sm switch-primary push-10-t">
								<button class="btn btn-sm btn-primary" type="submit">
									<i class="fa fa-check push-5-r"></i><s:message code="dashboard.organization.members.search"/>
								</button>
							</label>
						</div>
					</div>
				</sf:form>
				<sf:form method="POST" action="${pageContext.request.contextPath}/org_dashboard/save_employees" modelAttribute="customOrg" id="org-form3">
					<div class="form-group">
						<div class="col-xs-12">
							<label><s:message code="dashboard.organization.members.pickyourmembers"/></label>
							<div class="block">
								<div class="block-content">
									<table class="js-table-checkable table table-hover">
										<thead>
											<tr>
												<th class="text-center" style="width: 70px;"><label
													class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
												</label></th>
												<th><s:message code="dashboard.organization.members.email"/></th>
												<th><s:message code="dashboard.organization.members.organization"/></th>
												<th class="hidden-xs" style="width: 15%;"><s:message code="dashboard.organization.members.date"/></th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${membersList}" var="member">
												<tr>
													<td class="text-center">
														<label	class="css-input css-checkbox css-checkbox-primary">
															<input type="checkbox" name="selectedEmployees" value="${member.idMember}"
															${member.organization != null && member.organization.idOrganization == idOrg ? 'checked="checked"' : ''} />
															<span></span>
														</label>
													</td>
													<td>
														<p class="font-w600 push-10"><c:out value = "${member.mail}"/></p>
													</td>
													<td>
														<p class="font-w600 push-10"><c:out value = "${member.organization != null  ? member.organization.nameOrganization : '--'}"/></p>
													</td>
													<td class="hidden-xs">
														<em class="text-muted"><fmt:formatDate value="${member.creationDate}" pattern="yyyy-MM-dd" /></em>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="block-content block-content-full bg-gray-lighter text-center">
							<button class="btn btn-sm btn-primary" type="submit">
								<i class="fa fa-check push-5-r"></i><s:message code="dashboard.organization.members.savechanges"/>
							</button>
						</div>
					</div>
				</sf:form>
			</div>
		</div>
	</div>
</body>
</html>