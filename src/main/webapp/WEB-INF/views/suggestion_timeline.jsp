<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
 .suggestioncanvas {
    width: 400px;
    height: 100px;
    word-wrap:break-word;
 }
</style>
</head>
<body>
	<div class="content content-boxed">
    	<div class="row">
        	<div class="col-sm-7 col-lg-8">
            	<!-- Timeline -->
                <div class="block">
                	<div class="block-header bg-gray-lighter">
                    	<ul class="block-options">
                        	<li>
                            	<button type="button" data-toggle="block-option" data-action="fullscreen_toggle"></button>
                            </li>
                        </ul>
                        <h3 class="block-title"><i class="fa fa-comments-o"></i> <s:message code="dashboard.suggestions"/></h3>
                    </div>
                    <div class="block-content">
                    	<ul class="list list-timeline pull-t">
	                    	<c:forEach items="${suggestions}" var="suggestion">
								<!-- Notification -->
                                <li>
                                	<div class="list-timeline-time"><fmt:formatDate value="${suggestion.creationDate}" pattern="dd-MM-yyyy HH:mm:ss" /></div>
                                    <i class="fa fa-comment-o list-timeline-icon bg-primary-dark"></i>
                                    <div class="list-timeline-content">
                                       	<p class="font-w600">${suggestion.creator}</p>
                                        <p class="font-s13"><div class="suggestioncanvas">${suggestion.description}</div></p>
                                    </div>  
                               </li>
                               <!-- END Notification -->
                            </c:forEach> 
                        </ul>
                    </div>
                </div>
                <!-- END Timeline -->
            </div>
            
            <div class="col-sm-5 col-lg-4" ${memberCanEdit == "true" ? 'style="display: none;"' : ''}>             
	            <div class="block">
    	            <div class="block-header bg-gray-lighter">
	                    <h3 class="block-title"><i class="fa fa-fw fa-envelope-square "></i> <s:message code="dashboard.suggestions.send"/></h3>
                    </div>
                    <div class="block-content">
                    <form class="js-validation-material form-horizontal push-10-t" method="POST" action="${pageContext.request.contextPath}/problem_solving/save_suggestion" id="suggestion-form1">
						<div class="form-group">
							<div class="col-xs-6">
								<div class="form-material">
									<div class="form-material floating">							
										<textarea class="form-control" 
												id="suggestion-description" 
												name="suggestion-description" 
												rows="3"
												onchange="checkValidDataSuggestion()"></textarea>
										<label for="suggestion-description"><s:message code="dashboard.suggestions.anyidea"/></label>
									</div>
	
								</div>
							</div>
					    </div>
						<div class="form-group">
							<div class="col-xs-3">
								<button class="btn btn-sm btn-primary" id="send_suggestion_button" type="submit" onclick="disconnectDiagram()" disabled> <s:message code="dashboard.suggestions.there"/></button>
							</div>
						</div>
					</form>
                </div>
            </div>
		</div>
	</div>
	<script src="resources/js/suggestion_timeline.js"></script>
</body>
</html>