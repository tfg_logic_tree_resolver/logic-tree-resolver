<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" >
	    function disconnectDiagramUserAction() {
	
			if (stompClient != null) {
				stompClient.disconnect();
			}
			console.log("Disconnected");
	
	 	}
</script>
</head>
<body>
	<!-- Header -->
	<header id="header-navbar" class="content-mini content-mini-full">
	<!-- Header Navigation Right -->
	<ul class="nav-header pull-right">
		<li>
			<div class="btn-group">
				<button class="btn btn-default btn-image dropdown-toggle"
					data-toggle="dropdown" type="button">
					<img src="resources/images/avatars/avatar1.png" alt="Avatar">
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu dropdown-menu-right">				
					<li class="dropdown-header">Actions</li>
					<li><a href="<s:url value="/home"/>" tabindex="-1" onclick="disconnectDiagramUserAction()"> <i
							class="si si-home pull-right"></i>Back to Home
					</a></li>
					<li><a href="<s:url value="/logout"/>" tabindex="-1" onclick="disconnectDiagramUserAction()"> <i
							class="si si-logout pull-right"></i>Log out
					</a></li>
				</ul>
			</div>
		</li>
	</ul>
	<!-- END Header Navigation Right --> 
	</header>
	<script src="resources/js/oneui.min.js"></script>
</body>
</html>