<!DOCTYPE html>

<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
	<title>Sing Up</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<link rel="stylesheet" href="resources/css/signup.css" />
	<link rel="shortcut icon" href="resources/images/favicons/favicon.png">
</head>
<body>

	<!-- Header -->
	<header id="header">
		<h1>Welcome.</h1>
	</header>

	<!-- Signup Form -->
	<sf:form method="POST" modelAttribute="customMember" id="signup-form" >
		<sf:errors path="*"  element="p" />
		<sf:input path="userName" id="user_name" placeholder="User name" type="text"/>
		<sf:input path="completeName" id="user_full_name" placeholder="Full name" type="text"/>
		<sf:input path="mail"  id="user_email" placeholder="Email Address" type="email"/>
		<sf:input path="passwordMember" id="user_password" placeholder="Password" type="password"/>
		<sf:checkbox path="isOrganization" id="user_is_organization" label="Do you represent an organization?"/>
		<input type="submit" value="Sign Up" />
		<a href="<s:url value="/sign_in"/>" >Already a member?</a>
	</sf:form>
	<!-- Footer -->
	<footer id="footer">
		<ul class="icons">
			<li><a href="mailto:nluaces@gmail.com" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
		</ul>
		<ul class="copyright">
			<li>Credits: <a href="http://html5up.net">HTML5 UP</a></li>
		</ul>
	</footer>
	<!-- Scripts -->
	<script src="resources/js/signup.js"></script>
</body>
</html>