 function checkValidDataSuggestion() {
  	
	  
	  var description = document.getElementById("suggestion-description").value;
	  
	  var buttonMustBeEnabled = description !=null && description !="";
	  
	  	  
	  document.getElementById("send_suggestion_button").disabled = !buttonMustBeEnabled;
	  
      
  }
 
 function disconnectDiagram() {

		if (stompClient != null) {
			stompClient.disconnect();
		}
		console.log("Disconnected");

 }
 