/**
 * 
 */

function showEnrolledMembers() {
	var teamsSelect = document.getElementById("organization-teams");
	var idTeam = teamsSelect.options[teamsSelect.selectedIndex].value;

	var teamMembersSelect = document
			.getElementById("organization-team-members");

	for (i = 0; i < teamMembersSelect.length; i++) {
		o = teamMembersSelect.options[i];
		o.selected = (o.value == idTeam);

	}
}
