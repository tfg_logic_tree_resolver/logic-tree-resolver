
function loadSavedDiagramRootCauseStep1() {
	var savedDiagram = document.getElementById("savedModelStep1").value;

	if (savedDiagram == null || savedDiagram == "") {

		var firstDiagram = {
			"class" : "go.TreeModel",
			"nodeDataArray" : [ {
				"key" : 1,
				"name" : title
			} ]
		};
		myDiagram.model = go.Model.fromJson(firstDiagram);

	} else {
		myDiagram.model = go.Model.fromJson(savedDiagram);
	}
	


}

function initRootCauseStep1() {

	//Connect to server
	connectRootCauseStep();

	var $ = go.GraphObject.make; // for conciseness in defining templates

	myDiagram = $(go.Diagram, "myDiagramDiv", // must be the ID or reference to div
	{
		"animationManager.isEnabled" : false,
		initialContentAlignment : go.Spot.Center,
		maxSelectionCount : 1, // users can select only one part at a time
		validCycle : go.Diagram.CycleDestinationTree, // make sure users can only create trees
		"clickCreatingTool.archetypeNodeData" : {}, // allow double-click in background to create a new node
		"clickCreatingTool.insertPart" : function(loc) { // customize the data for the new node
			this.archetypeNodeData = {
				key : getNextKey(), // assign the key based on the number of nodes
				name : "?"
			};
			return go.ClickCreatingTool.prototype.insertPart.call(this, loc);
		},
		layout : $(go.TreeLayout, {
			treeStyle : go.TreeLayout.StyleLastParents,
			arrangement : go.TreeLayout.ArrangementVertical,
		}),
		"clickCreatingTool.isEnabled" : !isReadOnly,
		"undoManager.isEnabled" : true
	// enable undo & redo
	});
	
	//permissions
	var isReadOnly = (document.getElementById("memberCanEdit").value == 'false'); 
	myDiagram.isReadOnly = isReadOnly;
	
	

	// manage boss info manually when a node or link is deleted from the diagram
	myDiagram.addDiagramListener("SelectionDeleting", function(e) {
		var part = e.subject.first(); // e.subject is the myDiagram.selection collection,
		// so we'll get the first since we know we only have one selection
		myDiagram.startTransaction("clear boss");
		if (part instanceof go.Node) {
			var it = part.findTreeChildrenNodes(); // find all child nodes
			while (it.next()) { // now iterate through them and clear out the boss information
				var child = it.value;
				var bossText = child.findObject("boss"); // since the boss TextBlock is named, we can access it by name
				if (bossText === null)
					return;
				bossText.text = "";
			}
		} else if (part instanceof go.Link) {
			var child = part.toNode;
			var bossText = child.findObject("boss"); // since the boss TextBlock is named, we can access it by name
			if (bossText === null)
				return;
			bossText.text = "";
		}
		myDiagram.commitTransaction("clear boss");

		console.log('sending diagram');
		sendDiagramRootCauseStep();
	});

	var levelColors = [ "#4B83F2", "#CCD96A", "#F2BB77", "#F2865E", "#829889",
			"#EECC47", "#B29D51", "#DF524C" ];

	// override TreeLayout.commitNodes to also modify the background brush based on the tree depth level
	myDiagram.layout.commitNodes = function() {
		go.TreeLayout.prototype.commitNodes.call(myDiagram.layout); // do the standard behavior
		// then go through all of the vertexes and set their corresponding node's Shape.fill
		// to a brush dependent on the TreeVertex.level value
		myDiagram.layout.network.vertexes.each(function(v) {
			if (v.node) {
				var level = v.level % (levelColors.length);
				var color = levelColors[level];
				var shape = v.node.findObject("SHAPE");
				if (shape)
					shape.fill = $(go.Brush, "Linear", {
						0 : color,
						1 : go.Brush.lightenBy(color, 0.05),
						start : go.Spot.Left,
						end : go.Spot.Right
					});
			}
		});
	};

	// This function is used to find a suitable ID when modifying/creating nodes.
	// We used the counter combined with findNodeDataForKey to ensure uniqueness.
	function getNextKey() {
		var key = nodeIdCounter;
		while (myDiagram.model.findNodeDataForKey(key) !== null) {
			key = nodeIdCounter++;
		}
		return key;
	}

	var nodeIdCounter = 1; // use a sequence to guarantee key uniqueness as we add/remove/modify nodes

	// when a node is double-clicked, add a child to it
	function nodeDoubleClick(e, obj) {
		if(!isReadOnly){
			var clicked = obj.part;
			if (clicked !== null) {
				var thisemp = clicked.data;
				myDiagram.startTransaction("add cause");
				var newemp = {
					key : getNextKey(),
					name : "?",
					parent : thisemp.key
				};
				myDiagram.model.addNodeData(newemp);
				myDiagram.commitTransaction("add cause");
	
				console.log('sending diagram');
				sendDiagramRootCauseStep();
			}
		}
	}

	// this is used to determine feedback during drags
	function mayWorkFor(node1, node2) {
		if (!(node1 instanceof go.Node))
			return false; // must be a Node
		if (node1 === node2)
			return false; // cannot work for yourself
		if (node2.isInTreeOf(node1))
			return false; // cannot work for someone who works for you
		return true;
	}

	// This function provides a common style for most of the TextBlocks.
	// Some of these values may be overridden in a particular TextBlock.
	function textStyle() {
		return {
			font : "9pt  Segoe UI,sans-serif",
			stroke : "white"
		};
	}

	// define the Node template
	myDiagram.nodeTemplate = $(go.Node, "Auto", {
		doubleClick : nodeDoubleClick
	}, { // handle dragging a Node onto a Node to (maybe) change the reporting relationship
		mouseDragEnter : function(e, node, prev) {
			var diagram = node.diagram;
			var selnode = diagram.selection.first();
			if (!mayWorkFor(selnode, node))
				return;
			var shape = node.findObject("SHAPE");
			if (shape) {
				shape._prevFill = shape.fill; // remember the original brush
				shape.fill = "darkred";
			}

		},
		mouseDragLeave : function(e, node, next) {
			var shape = node.findObject("SHAPE");
			if (shape && shape._prevFill) {
				shape.fill = shape._prevFill; // restore the original brush
			}
		},
		mouseDrop : function(e, node) {
			var diagram = node.diagram;
			var selnode = diagram.selection.first(); // assume just one Node in selection
			if (mayWorkFor(selnode, node)) {
				// find any existing link into the selected node
				var link = selnode.findTreeParentLink();
				if (link !== null) { // reconnect any existing link
					link.fromNode = node;
				} else { // else create a new link
					diagram.toolManager.linkingTool.insertLink(node, node.port,
							selnode, selnode.port);
				}

				console.log('sending diagram');
				sendDiagramRootCauseStep();
			}
		}
	},
	// for sorting, have the Node.text be the data.name
	new go.Binding("text", "name"),
	// bind the Part.layerName to control the Node's layer depending on whether it isSelected
	new go.Binding("layerName", "isSelected", function(sel) {
		return sel ? "Foreground" : "";
	}).ofObject(),
	// define the node's outer shape
	$(go.Shape, "Rectangle", {
		name : "SHAPE",
		fill : "white",
		stroke : null,
		// set the port properties:
		portId : "",
		fromLinkable : true,
		toLinkable : true,
		cursor : "pointer"
	}), $(go.Panel, "Horizontal",

	// define the panel where the text will appear
	$(go.Panel, "Table", {
		maxSize : new go.Size(150, 999),
		margin : new go.Margin(6, 10, 0, 3),
		defaultAlignment : go.Spot.Left
	}, $(go.RowColumnDefinition, {
		column : 2,
		width : 4
	}), $(go.TextBlock, textStyle(), // the name
	{
		row : 0,
		column : 0,
		columnSpan : 5,
		font : "12pt Segoe UI,sans-serif",
		editable : true,
		isMultiline : false,
		minSize : new go.Size(10, 16)
	}, new go.Binding("text", "name").makeTwoWay())

	) // end Table Panel
	) // end Horizontal Panel
	); // end Node

	// define the Link template
	myDiagram.linkTemplate = $(go.Link, go.Link.Orthogonal, {
		corner : 5,
		relinkableFrom : true,
		relinkableTo : true
	}, $(go.Shape, {
		strokeWidth : 4,
		stroke : "#2C2F40"
	})); // the link shape

	// read in the JSON-format data from the "mySavedModel" element
	loadSavedDiagramRootCauseStep1();

	myDiagram.addDiagramListener("TextEdited", function(e) {
		console.log('sending diagram');
		sendDiagramRootCauseStep();
	});

	myDiagram.addDiagramListener("SelectionDeleted", function(e) {
		console.log('sending diagram');
		sendDiagramRootCauseStep();
	});

}
