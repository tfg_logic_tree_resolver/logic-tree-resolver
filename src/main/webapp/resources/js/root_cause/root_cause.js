var stompClient = null;

var idProblem = document.getElementById("idProblem").value;
var idStatus = document.getElementById("idStatus").value;
var userName = document.getElementById("userName").value;
var title = document.getElementById("title").value;

function initRootCause() {

	var idStatus = document.getElementById("idStatus").value;

	switch (idStatus) {
	case "1":
		initRootCauseStep1();
		break;
	case "2":
		initRootCauseStep2();
		break;
	case "3":
		initRootCauseStep3();
		break;
	case "4":
		initRootCauseStep4();
		break;
	case "5":
		initRootCauseStep5();
		break;
	case "6":
		initRootCauseStep6();
		break;
	case "7":
		initRootCauseStep7();
		break;
	}

}

function disconnectDiagramRootCause() {

	if (stompClient != null) {
		stompClient.disconnect();
	}
	console.log("Disconnected");

}

function sendDiagramRootCauseStep() {

	var message = {
		"content" : myDiagram.model.toJson(),
		"idProblem" : idProblem,
		"idStatus" : idStatus,
		"userName" : userName
	};

	stompClient.send("/app/update", {}, JSON.stringify(message));
}

function loadDiagramRootCauseStep(frame) {
	console.log('loading diagram');
	
	myDiagram.model = go.Model.fromJson(JSON.parse(frame.body).content);

}

function loadSavedDiagramRootCauseStep(nameSavedModel) {
	var savedDiagram = document.getElementById(nameSavedModel).value;

	if (savedDiagram != null && savedDiagram != "") {
	
		myDiagram.model = go.Model.fromJson(savedDiagram);
	}


}

function connectRootCauseStep() {

	var loc = window.location.host;
	
	var endpoint = '/ws';
	if(loc == 'localhost:8080'){
		endpoint = '/LogicTreeWebApp/ws';
	}
	
	var socket = new SockJS(endpoint);
	stompClient = Stomp.over(socket);

	// Callback function to be called when stomp client is connected to server
	var connectCallback = function() {

		stompClient.subscribe('/topic/diagram_' + idProblem + '_' + idStatus,
				loadDiagramRootCauseStep);
	};

	var errorCallback = function() {
		alert("Ending websocket connection to server");
	};

	// Connect to server via websocket
	stompClient.connect("guest", "guest", connectCallback, errorCallback);

}
