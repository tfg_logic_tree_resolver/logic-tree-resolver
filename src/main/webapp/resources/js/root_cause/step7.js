
function initRootCauseStep7() {
    
	connectRootCauseStep();
	
    var $ = go.GraphObject.make;
    var pieRadius = 100;

    myDiagram =
      $(go.Diagram, "myDiagramDiv7",
        {
          initialContentAlignment: go.Spot.Center,
          "textEditingTool.starting": go.TextEditingTool.SingleClick,
          "ModelChanged": onModelChanged,
          "undoManager.isEnabled": true,
          "animationManager.isEnabled": false
        }
      );

	//permissions
	var isReadOnly = (document.getElementById("memberCanEdit").value == 'false'); 
	myDiagram.isReadOnly = isReadOnly;
	
    // When a count changes in our model, ensure we trigger a redrawing of each
	// slice in the pie
    function onModelChanged(e) {
      if (e.change === go.ChangedEvent.Property && e.propertyName === "count") {
        var slicedata = e.object;
        var nodedata = findNodeDataForSlice(slicedata);
        if (nodedata) {
          // Update the count binding to force makeGeo/positionSlice
          myDiagram.model.updateTargetBindings(nodedata, "count");
          // If the count went to 0, hide the slice
          var sliceindex = nodedata.slices.indexOf(slicedata);
          var slice = myDiagram.findNodeForKey(nodedata.key).findObject("PIE").elt(sliceindex);
          var sliceshape = slice.findObject("SLICE");
          if (slicedata.count === 0)
            sliceshape.visible = false;
          else
            sliceshape.visible = true;
        }
      }
    }

    var sliceTemplate =
      $(go.Panel,
        $(go.Shape,
          {
            name: "SLICE",
            strokeWidth: 2, stroke: "transparent",
            isGeometryPositioned: true
          },
          new go.Binding("fill", "color"),
          new go.Binding("geometry", "", makeGeo)
        ),
        new go.Binding("position", "", positionSlice),
        { // Allow the user to "select" slices when clicking them
          click: function(e, slice) {
            var sliceShape = slice.findObject("SLICE");
            var oldskips = slice.diagram.skipsUndoManager;
            slice.diagram.skipsUndoManager = true;
            if (sliceShape.stroke === "transparent") {
              sliceShape.stroke = go.Brush.darkenBy(slice.data.color, 0.4);
              // Move the slice out from the pie when selected
              var nodedata = findNodeDataForSlice(slice.data);
              if (nodedata) {
                var sliceindex = nodedata.slices.indexOf(slice.data);
                var angles = getAngles(nodedata, sliceindex);
                if (angles.sweep !== 360) {
                  var angle = angles.start + angles.sweep / 2;
                  var offsetPoint = new go.Point(pieRadius / 10, 0);
                  slice.position = offsetPoint.rotate(angle).offset(pieRadius / 10, pieRadius / 10);
                }
              }
            } else {
              sliceShape.stroke = "transparent";
              slice.position = new go.Point(pieRadius / 10, pieRadius / 10);
            }
            slice.diagram.skipsUndoManager = oldskips;
          }
        },
        {
          toolTip:
            $(go.Adornment, "Auto",
              $(go.Shape, { fill: "lightgray" }),
              $(go.TextBlock,
                { font: "10pt Verdana, sans-serif", margin: 4 },
                new go.Binding("text", "", function(data) {
                      // Display text and percentage rounded to 2 decimals
                      var nodedata = findNodeDataForSlice(data);
                      if (nodedata) {
                        var percent = Math.round((data.count / getTotalCount(nodedata) * 100) * 100) / 100;
                        return data.text + ": " + percent + "%";
                      }
                      return "";
                    }))
            )
        }
      );

    var optionTemplate =
      $(go.Panel, "TableRow",
        $(go.TextBlock,
          { column: 0,
            font: "10pt Verdana, sans-serif", alignment: go.Spot.Left,
            margin: 5 },
          new go.Binding("text")
        ),
        $(go.Panel, "Auto",
          { column: 1 },
          $(go.Shape, { fill: "#F2F2F2" }),
          $(go.TextBlock,
            {
              font: "10pt Verdana, sans-serif",
              textAlign: "right", margin: 2,
              wrap: go.TextBlock.None, width: 40,
              editable: true, isMultiline: false,
              textValidation: isValidCount
            },
            new go.Binding("text", "count").makeTwoWay(function(count) { return parseInt(count, 10); })
          )
        ),
        $(go.Panel, "Horizontal",
          { column: 2 },
          $("Button",
            {
              click: incrementCount
            },
            $(go.Shape, "PlusLine", { margin: 3, desiredSize: new go.Size(7, 7) })
          ),
          $("Button",
            {
              click: decrementCount
            },
            $(go.Shape, "MinusLine", { margin: 3, desiredSize: new go.Size(7, 7) })
          )
        )
      );
    
    
    var guessedSliceTemplate =
   	
        $(go.Panel,
          $(go.Shape,
            {
              name: "GUESSEDSLICE",
              strokeWidth: 2, stroke: "transparent",
              isGeometryPositioned: true
            },
            new go.Binding("fill", "color"),
            new go.Binding("geometry", "", makeGuessedGeo)
          ),
          new go.Binding("position", "", positionGuessedSlice),
          {
            toolTip:
              $(go.Adornment, "Auto",
                $(go.Shape, { fill: "lightgray" }),
                $(go.TextBlock,
                  { font: "10pt Verdana, sans-serif", margin: 4 },
                  new go.Binding("text", "", function(data) {
                        // Display text and percentage rounded to 2 decimals
                        var nodedata = findGuessedNodeDataForSlice(data);
                        if (nodedata) {
                          var percent = Math.round((data.count / getTotalCount(nodedata) * 100) * 100) / 100;
                          return data.text + ": " + percent + "%";
                        }
                        return "";
                      }))
              )
          }
        );
    

    myDiagram.nodeTemplate =
        
        $(go.Node, "Horizontal",
        		{ deletable: false },
        	              	    	        
        	   $(go.Panel, "Vertical",
	    	        
	    	        $(go.TextBlock,
	    	          { font: "11pt Verdana, sans-serif", margin: 5 },
	    	          new go.Binding("text", "nameGuessedPie")
	    	          
	    	          
	    	        ),
	    	        $(go.Panel, "Horizontal",
	    	        		$(go.Panel, "Position",
          {
            name: "GUESSEDPIE",
            // account for slices offsetting when selected so the node won't
				// change size
            desiredSize: new go.Size(pieRadius * 2.2  + 5, pieRadius * 2.2 + 5),
            itemTemplate: guessedSliceTemplate
          },
          new go.Binding("itemArray", "guessedslices")
        ),
        $(go.Panel, "Table",
          {
            margin: 5,
            itemTemplate: optionTemplate
          },
          new go.Binding("itemArray", "guessedslices")
        )
        )
      ),
	      $(go.Panel, "Vertical",
	    	        
	    	        $(go.TextBlock,
	    	          { font: "11pt Verdana, sans-serif", margin: 5 },
	    	          new go.Binding("text", "namePie")
	    	          
	    	          
	    	        ),
	    	        $(go.Panel, "Horizontal",
	    	        		$(go.Panel, "Position",
            {
              name: "PIE",
              // account for slices offsetting when selected so the node won't
				// change size
              desiredSize: new go.Size(pieRadius * 2.2  + 5, pieRadius * 2.2 + 5),
              itemTemplate: sliceTemplate
            },
            new go.Binding("itemArray", "slices")
          ),
          $(go.Panel, "Table",
            {
              margin: 5,
              itemTemplate: optionTemplate
            },
            new go.Binding("itemArray", "slices")
          )
          )
        )
        
      );



    // Validation function for editing text
    function isValidCount(textblock, oldstr, newstr) {
      if (newstr === "") return false;
      var num = +newstr; // quick way to convert a string to a number
      return !isNaN(num) && Number.isInteger(num) && num >= 0;
    }

    // Given some slice data, find the corresponding node data
    function findNodeDataForSlice(slice) {
      var arr = myDiagram.model.nodeDataArray;
      for (var i = 0; i < arr.length; i++) {
        var data = arr[i];
        if (data.slices.indexOf(slice) >= 0) {
          return data;
        }
      }
    }

    function findGuessedNodeDataForSlice(slice) {
        var arr = myDiagram.model.nodeDataArray;
        for (var i = 0; i < arr.length; i++) {
          var data = arr[i];
          if (data.guessedslices.indexOf(slice) >= 0) {
            return data;
          }
        }
      }
    
    function makeGeo(data) {
      var nodedata = findNodeDataForSlice(data);
      var sliceindex = nodedata.slices.indexOf(data);
      var angles = getAngles(nodedata, sliceindex);

      // Constructing the Geomtery this way is much more efficient than
		// calling go.GraphObject.make:
      return new go.Geometry()
            .add(new go.PathFigure(pieRadius, pieRadius)  // start point
                 .add(new go.PathSegment(go.PathSegment.Arc,
                                         angles.start, angles.sweep,  // angles
                                         pieRadius, pieRadius,  // center
                                         pieRadius, pieRadius)  // radius
                      .close()));
    }
    
    function makeGuessedGeo(data) {
        var nodedata = findGuessedNodeDataForSlice(data);
        var sliceindex = nodedata.guessedslices.indexOf(data);
        var angles = getGuessedAngles(nodedata, sliceindex);

        // Constructing the Geomtery this way is much more efficient than
		// calling go.GraphObject.make:
        return new go.Geometry()
              .add(new go.PathFigure(pieRadius, pieRadius)  // start point
                   .add(new go.PathSegment(go.PathSegment.Arc,
                                           angles.start, angles.sweep,  // angles
                                           pieRadius, pieRadius,  // center
                                           pieRadius, pieRadius)  // radius
                        .close()));
      }

    // Ensure slices get the proper positioning after we update any counts
    function positionSlice(data, obj) {
      var nodedata = findNodeDataForSlice(data);
      var sliceindex = nodedata.slices.indexOf(data);
      var angles = getAngles(nodedata, sliceindex);

      var selected = obj.findObject("SLICE").stroke !== "transparent";
      if (selected && angles.sweep !== 360) {
        var offsetPoint = new go.Point(pieRadius / 10, 0); // offset by 1/10
															// the radius
        offsetPoint = offsetPoint.rotate(angles.start + angles.sweep / 2); // rotate
																			// to
																			// the
																			// correct
																			// angle
        offsetPoint = offsetPoint.offset(pieRadius / 10, pieRadius / 10); // translate
																			// center
																			// toward
																			// middle
																			// of
																			// pie
																			// panel
        return offsetPoint;
      }
      return new go.Point(pieRadius / 10, pieRadius / 10);
    }
    
    function positionGuessedSlice(data, obj) {
        var nodedata = findGuessedNodeDataForSlice(data);
        var sliceindex = nodedata.guessedslices.indexOf(data);
        var angles = getGuessedAngles(nodedata, sliceindex);

        var selected = obj.findObject("GUESSEDSLICE").stroke !== "transparent";
        if (selected && angles.sweep !== 360) {
          var offsetPoint = new go.Point(pieRadius / 10, 0); // offset by
																// 1/10 the
																// radius
          offsetPoint = offsetPoint.rotate(angles.start + angles.sweep / 2); // rotate
																				// to
																				// the
																				// correct
																				// angle
          offsetPoint = offsetPoint.offset(pieRadius / 10, pieRadius / 10); // translate
																			// center
																			// toward
																			// middle
																			// of
																			// pie
																			// panel
          return offsetPoint;
        }
        return new go.Point(pieRadius / 10, pieRadius / 10);
      }

    // This is a bit inefficient, but should be OK for normal-sized graphs with
	// reasonable numbers of slices per node
    function findAllSelectedItems() {
      var slices = [];
      for (var nit = myDiagram.nodes; nit.next(); ) {
        var node = nit.value;
        var pie = node.findObject("PIE");
        if (pie) {
          for (var sit = pie.elements; sit.next(); ) {
            var slicepanel = sit.value;
            if (slicepanel.findObject("SLICE").stroke !== "transparent") slices.push(slicepanel);
          }
        }
      }
      return slices;
    }

   

    // Return total count of a given node
    function getTotalCount(nodedata) {
      var totCount = 0;
      for (var i = 0; i < nodedata.slices.length; i++) {
        totCount += nodedata.slices[i].count;
      }
      return totCount;
    }
    
    function getGuessedTotalCount(nodedata) {
        var totCount = 0;
        for (var i = 0; i < nodedata.guessedslices.length; i++) {
          totCount += nodedata.guessedslices[i].count;
        }
        return totCount;
      }

    // Determine start and sweep angles given some node data and the index of
	// the slice
    function getAngles(nodedata, index) {
      var totCount = getTotalCount(nodedata);
      var startAngle = -90;
      for (var i = 0; i < index; i++) {
        startAngle += 360 * nodedata.slices[i].count / totCount;
      }
      return { "start": startAngle, "sweep": 360 * nodedata.slices[index].count / totCount };
    }
    
    function getGuessedAngles(nodedata, index) {
        var totCount = getGuessedTotalCount(nodedata);
        var startAngle = -90;
        for (var i = 0; i < index; i++) {
          startAngle += 360 * nodedata.guessedslices[i].count / totCount;
        }
        return { "start": startAngle, "sweep": 360 * nodedata.guessedslices[index].count / totCount };
      }

    // When user hits + button, increment count on that option
    function incrementCount(e, obj) {
      myDiagram.model.startTransaction("increment count");
      var slicedata = obj.panel.panel.data;
      myDiagram.model.setDataProperty(slicedata, "count", slicedata.count + 1);
      myDiagram.model.commitTransaction("increment count");
      console.log('sending diagram');
	  sendDiagramRootCauseStep();
    }

    // When user hits - button, decrement count on that option
    function decrementCount(e, obj) {
      myDiagram.model.startTransaction("decrement count");
      var slicedata = obj.panel.panel.data;
      if (slicedata.count > 0)
        myDiagram.model.setDataProperty(slicedata, "count", slicedata.count - 1);
      myDiagram.model.commitTransaction("decrement count");
      console.log('sending diagram');
	  sendDiagramRootCauseStep();
    }
    
    loadSavedDiagramRootCauseStep("savedModelStep7");
}