var myDiagramMethods;

var myDiagramPriorities;

  function sendDiagramRootCauseStep6() {
	  
	   var content = myDiagramMethods.model;
	   
	   content.modelData.complementaryModel = myDiagramPriorities.model.toJson();

		var message = {
			"content" : content.toJson(),
			"idProblem" : idProblem,
			"idStatus" : idStatus,
			"userName" : userName
		};

		stompClient.send("/app/update", {}, JSON.stringify(message));
	}

	function loadDiagramRootCauseStep6(frame) {
		console.log('loading diagram');
		
		var content = JSON.parse(frame.body).content;
		
		var contentModel = JSON.parse(content);
		
		myDiagramMethods.model = go.Model.fromJson(content);
		myDiagramPriorities.model = go.Model.fromJson(contentModel.modelData.complementaryModel);

	}


	function connectRootCauseStep6() {

		var loc = window.location.host;
		
		var endpoint = '/ws';
		if(loc == 'localhost:8080'){
			endpoint = '/LogicTreeWebApp/ws';
		}
		
		var socket = new SockJS(endpoint);
		
		stompClient = Stomp.over(socket);

		// Callback function to be called when stomp client is connected to server
		var connectCallback = function() {

			stompClient.subscribe('/topic/diagram_' + idProblem + '_' + idStatus,
					loadDiagramRootCauseStep6);
		};

		var errorCallback = function() {
			alert("Ending websocket connection to server");
		};

		// Connect to server via websocket
		stompClient.connect("guest", "guest", connectCallback, errorCallback);

	}
  
  
  function initRootCauseStep6() {
   
	  var compositeDiagram = JSON.parse(document.getElementById("savedModelStep6A").value);
	   
	  document.getElementById("savedModelStep6B").value = compositeDiagram.modelData.complementaryModel;
	  
	  connectRootCauseStep6();
	  
	  myDiagramMethods = initDiagram("myDiagramDiv6A","savedModelStep6A","myPaletteDiv6A",false);
	  myDiagramPriorities = initDiagram("myDiagramDiv6B","savedModelStep6B","myPaletteDiv6B",true);

	
  }
  

  
  function initDiagram(nameDiagram,nameSavedModel,namePalette,priority){
	  
	    var $ = go.GraphObject.make;

	    if(priority){
		    myDiagram =
			      $(go.Diagram, nameDiagram,
			        {
			          initialContentAlignment: go.Spot.Center,
			          layout: $(TableLayout,
			                    $(go.RowColumnDefinition, { row: 1, height: 22 }),  // fixed size column headers
			                    $(go.RowColumnDefinition, { column: 1, width: 22 }) // fixed size row headers
			                  ),
			          
			          allowDrop: true,
			          // feedback that dropping in the background is not allowed
			          mouseDragOver: function(e) { e.diagram.currentCursor = "not-allowed"; },
			          // when dropped in the background, not on a Node or a Group, cancel the drop
			          mouseDrop: function(e) { e.diagram.currentTool.doCancel(); },
			          "animationManager.isInitial": false,
			          "undoManager.isEnabled": true
			        });
	    	
	    }else{
		    myDiagram =
			      $(go.Diagram, nameDiagram,
			        {
			          initialContentAlignment: go.Spot.Center,
			          layout: $(TableLayout,
			                    $(go.RowColumnDefinition, { row: 1, height: 22 }),  // fixed size column headers
			                    $(go.RowColumnDefinition, { column: 1, width: 22 }) // fixed size row headers
			                  ),
			          "SelectionMoved": function(e) { e.diagram.layoutDiagram(true); },
			          "resizingTool": new LaneResizingTool(),
			          allowDrop: true,
			          // feedback that dropping in the background is not allowed
			          mouseDragOver: function(e) { e.diagram.currentCursor = "not-allowed"; },
			          // when dropped in the background, not on a Node or a Group, cancel the drop
			          mouseDrop: function(e) { e.diagram.currentTool.doCancel(); },
			          "animationManager.isInitial": false,
			          "undoManager.isEnabled": true
			        });
	    }
	    
		//permissions
		var isReadOnly = (document.getElementById("memberCanEdit").value == 'false'); 
		myDiagram.isReadOnly = isReadOnly;

	    myDiagram.nodeTemplateMap.add("Header",  // an overall table header, at the top
	      $(go.Part, "Auto",
	        {
	          row: 0, column: 1, columnSpan: 9999,
	          stretch: go.GraphObject.Horizontal,
	          selectable: false, pickable: false
	        },
	        $(go.Shape, { fill: "transparent", strokeWidth: 0 }),
	        $(go.TextBlock, { alignment: go.Spot.Center, font: "bold 12pt sans-serif" },
	          new go.Binding("text"))
	      ));

	    myDiagram.nodeTemplateMap.add("Sider",  // an overall table header, on the left side
	      $(go.Part, "Auto",
	        {
	          row: 1, rowSpan: 9999, column: 0,
	          stretch: go.GraphObject.Vertical,
	          selectable: false, pickable: false
	        },
	        $(go.Shape, { fill: "transparent", strokeWidth: 0 }),
	        $(go.TextBlock, { alignment: go.Spot.Center, font: "bold 12pt sans-serif", angle: 270 },
	          new go.Binding("text"))
	      ));

	    
	    var cellSizeColumn = null;
	    var cellSizeRow = null;
	    
	    if(priority){
	    
	    	cellSizeColumn =  new go.Size(250,250);
		    cellSizeRow =  new go.Size(250,250);
	    	
	    }else{
	    	cellSizeColumn =  new go.Size(100,NaN);
		    cellSizeRow =  new go.Size(NaN,100);
	    }
	    
	    myDiagram.nodeTemplateMap.add("Column Header",  // for each column header
	      $(go.Part, "Spot",
	        {
	          row: 1, rowSpan: 9999, column: 2,
	          minSize: cellSizeColumn,
	          stretch: go.GraphObject.Fill,
	          movable: false,
	          resizable: !priority,
	          resizeAdornmentTemplate:
	            $(go.Adornment, "Spot",
	              $(go.Placeholder),
	              $(go.Shape,  // for changing the length of a lane
	                {
	                  alignment: go.Spot.Right,
	                  desiredSize: new go.Size(7, 50),
	                  fill: "lightblue", stroke: "dodgerblue",
	                  cursor: "col-resize"
	                })
	            )
	        },
	        new go.Binding("column", "col"),
	        $(go.Shape, { fill: null },
	          new go.Binding("fill", "color")),
	        $(go.Panel, "Auto",
	          { // this is positioned above the Shape, in row 1
	            alignment: go.Spot.Top, alignmentFocus: go.Spot.Bottom,
	            stretch: go.GraphObject.Horizontal,
	            height: myDiagram.layout.getRowDefinition(1).height
	          },
	          $(go.Shape, { fill: "transparent", strokeWidth: 0 }),
	          $(go.TextBlock,
	            {
	              font: "bold 10pt sans-serif", isMultiline: false,
	              wrap: go.TextBlock.None, overflow: go.TextBlock.OverflowEllipsis
	            },
	            new go.Binding("text"))
	        )
	      ));
	    
	   

	    myDiagram.nodeTemplateMap.add("Row Sider",  // for each row header
	      $(go.Part, "Spot",
	        {
	          row: 2, column: 1, columnSpan: 9999,
	          minSize: cellSizeRow,
	          stretch: go.GraphObject.Fill,
	          movable: false,
	          resizable: !priority,
	          resizeAdornmentTemplate:
	            $(go.Adornment, "Spot",
	              $(go.Placeholder),
	              $(go.Shape,  // for changing the breadth of a lane
	                {
	                  alignment: go.Spot.Bottom,
	                  desiredSize: new go.Size(50, 7),
	                  fill: "lightblue", stroke: "dodgerblue",
	                  cursor: "row-resize"
	                })
	            )
	        },
	        new go.Binding("row"),
	        $(go.Shape, { fill: null },
	          new go.Binding("fill", "color")),
	        $(go.Panel, "Auto",
	          { // this is positioned to the left of the Shape, in column 1
	            alignment: go.Spot.Left, alignmentFocus: go.Spot.Right,
	            stretch: go.GraphObject.Vertical, angle: 270,
	            height: myDiagram.layout.getColumnDefinition(1).width
	          },
	          $(go.Shape, { fill: "transparent", strokeWidth: 0 }),
	          $(go.TextBlock,
	            {
	              font: "bold 10pt sans-serif", isMultiline: false,
	              wrap: go.TextBlock.None, overflow: go.TextBlock.OverflowEllipsis
	            },
	            new go.Binding("text"))
	        )
	      ));

	    myDiagram.nodeTemplate =  // for regular nodes within cells (groups); you'll want to extend this
	      $(go.Node, "Auto",
	        { margin: 4 },  // assume uniform Margin, all around
	        new go.Binding("row"),
	        new go.Binding("column", "col"),
	        $(go.Shape, { fill: "white",
		    	  minSize : new go.Size(100, 50)  },
	          new go.Binding("fill", "color")),
	        $(go.TextBlock,
	          {editable: true},
	          new go.Binding("text").makeTwoWay())
	      );
	    
	    myDiagram.nodeTemplateMap.add("Priority",
	    	      $(go.Node, "Spot",
	    	        $(go.Panel, "Auto",
	    	          $(go.Shape, "Circle",
	    	            { minSize: new go.Size(40, 40), fill: "#BE3D8F", stroke: null }),
	    	          $(go.TextBlock, 
	    	            { font: "bold 11pt Helvetica, Arial, sans-serif",
	    	        	  editable: false
	    	        	},
	    	            new go.Binding("text"))
	    	        )
	    	      ));

	    myDiagram.groupTemplate =  // for cells
	      $(go.Group, "Auto",
	        {
	          layerName: "Background",
	          stretch: go.GraphObject.Fill,
	          selectable: false,
	          computesBoundsAfterDrag: true,
	          computesBoundsIncludingLocation: true,
	          handlesDragDropForMembers: true,  // don't need to define handlers on member Nodes and Links
	          mouseDragEnter: function(e, group, prev) { group.isHighlighted = true; },
	          mouseDragLeave: function(e, group, next) { group.isHighlighted = false; },
	          mouseDrop: function(e, group) {
	            // if any dropped part wasn't already a member of this group, we'll want to let the group's row
	            // column allow themselves to be resized automatically, in case the row height or column width
	            // had been set manually by the LaneResizingTool
	            var anynew = e.diagram.selection.any(function(p) { return p.containingGroup !== group; });
	            // Don't allow headers/siders to be dropped
	            var anyHeadersSiders = e.diagram.selection.any(function(p) {
	              return p.category === "Column Header" || p.category === "Row Sider";
	            });
	            if (!anyHeadersSiders && group.addMembers(e.diagram.selection, true)) {
	              if (anynew) {
	                e.diagram.layout.getRowDefinition(group.row).height = NaN;
	                e.diagram.layout.getColumnDefinition(group.column).width = NaN;
	              }
	            } else {  // failure upon trying to add parts to this group
	              e.diagram.currentTool.doCancel();
	            }
	          }
	        },
	        new go.Binding("row"),
	        new go.Binding("column", "col"),
	        // the group is normally unseen -- it is completely transparent except when given a color or when highlighted
	        $(go.Shape,
	          {
	            fill: "transparent", stroke: "transparent",
	            strokeWidth: myDiagram.nodeTemplate.margin.left,
	            stretch: go.GraphObject.Fill
	          },
	          new go.Binding("fill", "color"),
	          new go.Binding("stroke", "isHighlighted", function(h) { return h ? "red" : "transparent"; }).ofObject()),
	        $(go.Placeholder,
	          { // leave a margin around the member nodes of the group which is the same as the member node margin
	            alignment: (function(m) { return new go.Spot(0, 0, m.top, m.left); })(myDiagram.nodeTemplate.margin),
	            padding: (function(m) { return new go.Margin(0, m.right, m.bottom, 0); })(myDiagram.nodeTemplate.margin)
	          })
	      );

	    myDiagram.model = go.Model.fromJson(document.getElementById(nameSavedModel).value);
	    
	    if(!priority){
		    myPalette =
		        $(go.Palette, namePalette,
		          {
		            nodeTemplateMap: myDiagram.nodeTemplateMap,
		            "model.nodeDataArray": [
		            	
		                { text: "???", color: "orange"},
		                { text: "???", color: "tomato"},
		                { text: "???", color: "goldenrod"}
		              
		            ]
		            
		          }
		          );
	    }
	    
	    
	    myDiagram.addDiagramListener("TextEdited", function(e) {
	    	console.log('sending diagram');
	    	sendDiagramRootCauseStep6();
	  	});
	    
	    myDiagram.addDiagramListener("SelectionDeleted",function(e) {
	    	console.log('sending diagram');
	    	sendDiagramRootCauseStep6();
	    });
	    
	    myDiagram.addDiagramListener("PartCreated",function(e) {
	    	console.log('sending diagram');
	    	sendDiagramRootCauseStep6();
	    });
	    
	    myDiagram.addDiagramListener("SelectionMoved",function(e) {
	    	console.log('sending diagram');
	    	sendDiagramRootCauseStep6();
	    });
	    
	    myDiagram.addDiagramListener("LinkDrawn",function(e) {
	    	console.log('sending diagram');
	    	sendDiagramRootCauseStep6();
	    });

	    
	    return myDiagram;
  }
  
//define a custom ResizingTool to limit how far one can shrink a row or column
  function LaneResizingTool() {
    go.ResizingTool.call(this);
  }
  go.Diagram.inherit(LaneResizingTool, go.ResizingTool);

  /** @override */
  LaneResizingTool.prototype.computeMinSize = function() {
    var diagram = this.diagram;
    var lane = this.adornedObject.part;  // might be row or column
    var horiz = (lane.category === "Column Header");  // or "Row Header"
    var margin = diagram.nodeTemplate.margin;
    var bounds = new go.Rect();
    diagram.findTopLevelGroups().each(function(g) {
      if (horiz ? (g.column === lane.column) : (g.row === lane.row)) {
        var b = diagram.computePartsBounds(g.memberParts);
        if (b.isEmpty()) return;  // nothing in there?  ignore it
        b.unionPoint(g.location);  // keep any empty space on the left and top
        b.addMargin(margin);  // assume the same node margin applies to all nodes
        if (bounds.isEmpty()) {
          bounds = b;
        } else {
          bounds.unionRect(b);
        }
      }
    });

    // limit the result by the standard value of computeMinSize
    var msz = go.ResizingTool.prototype.computeMinSize.call(this);
    if (bounds.isEmpty()) return msz;
    return new go.Size(Math.max(msz.width, bounds.width), Math.max(msz.height, bounds.height));
  };

  /** @override */
  LaneResizingTool.prototype.resize = function(newr) {
    var lane = this.adornedObject.part;
    var horiz = (lane.category === "Column Header");
    var lay = this.diagram.layout;  // the TableLayout
    if (horiz) {
      var col = lane.column;
      var coldef = lay.getColumnDefinition(col);
      coldef.width = newr.width;
    } else {
      var row = lane.row;
      var rowdef = lay.getRowDefinition(row);
      rowdef.height = newr.height;
    }
    lay.invalidateLayout();
  };
  // end LaneResizingTool class
