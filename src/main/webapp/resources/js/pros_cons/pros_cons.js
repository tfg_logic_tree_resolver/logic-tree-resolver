var stompClient = null;

var idProblem = document.getElementById("idProblem").value;
var idStatus = document.getElementById("idStatus").value;
var userName = document.getElementById("userName").value;
var title = document.getElementById("title").value;

function initProsCons() {

	var idStatus = document.getElementById("idStatus").value;

	switch (idStatus) {
	case "1":
		initProsConsStep1();
		break;
	case "2":
		initProsConsStep2();
		break;
	case "3":
		initProsConsStep3();
		break;
	}

}

function disconnectDiagramProscons() {

	if (stompClient != null) {
		stompClient.disconnect();
	}
	console.log("Disconnected");

}

function sendDiagramProsConsStep() {

	var message = {
		"content" : myDiagram.model.toJson(),
		"idProblem" : idProblem,
		"idStatus" : idStatus,
		"userName" : userName
	};

	stompClient.send("/app/update", {}, JSON.stringify(message));
}

function loadDiagramProsConsStep(frame) {
	console.log('loading diagram');
	
	myDiagram.model = go.Model.fromJson(JSON.parse(frame.body).content);

}

function loadSavedDiagramProsConsStep(nameSavedModel) {
	var savedDiagram = document.getElementById(nameSavedModel).value;

	if (savedDiagram != null && savedDiagram != "") {
	
		myDiagram.model = go.Model.fromJson(savedDiagram);
	}


}

function connectProsConsStep() {

	var loc = window.location.host;
	
	var endpoint = '/ws';
	if(loc == 'localhost:8080'){
		endpoint = '/LogicTreeWebApp/ws';
	}
	
	var socket = new SockJS(endpoint);
	stompClient = Stomp.over(socket);

	// Callback function to be called when stomp client is connected to server
	var connectCallback = function() {

		stompClient.subscribe('/topic/diagram_' + idProblem + '_' + idStatus,
				loadDiagramProsConsStep);
	};

	var errorCallback = function() {
		alert("Ending websocket connection to server");
	};

	// Connect to server via websocket
	stompClient.connect("guest", "guest", connectCallback, errorCallback);

}