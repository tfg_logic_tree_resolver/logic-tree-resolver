  function checkValidDataNewProblem() {
  	
	  var title = document.getElementById("problem-title").value;
	  var description = document.getElementById("problem-description").value;
	  var type = document.getElementById("problem-type").value;
	  var team = document.getElementById("problem_team").value;

	  var buttonMustBeEnabled = title != null && title !="" &&
	  					description !=null && description !="" &&
	  					type!= null && type!="" &&
	  					team != null && team!="";
	  
	  	  
	  document.getElementById("createProblemButton").disabled = !buttonMustBeEnabled;
	  
      
  }
  