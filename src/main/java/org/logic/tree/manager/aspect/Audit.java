package org.logic.tree.manager.aspect;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.logic.tree.manager.custom.CustomEntity;
import org.springframework.stereotype.Component;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

@Component
@Aspect
public class Audit implements Serializable{

	/**
	 * This aspect cover the audit data of the custom entities.
	 */
	private static final long serialVersionUID = 6895605830623737179L;

	public Audit() {
		super();
	}
	
	@Before("execution(* org.logic.tree.manager.*.save*(..)) && args(custom, userName)")
	public <T extends CustomEntity> void auditCreateCustomEntity(T custom, String userName){
		custom.saveOperation(userName, new Date());
	}
	
	@Before("execution(* org.logic.tree.manager.impl.*.listSave*(..)) && args(customList, userName)")
	public <T extends CustomEntity> void auditCreateListCustomEntity(List<T> customList, String userName){
		Date now = new Date();
		
		for(T custom: customList){
			custom.saveOperation(userName, now);
		}
	}

	@Before("execution(* org.logic.tree.manager.impl.*.update*(..)) && args(custom, userName)")
	public <T extends CustomEntity> void auditUpdateCustomEntity(T custom, String userName){
		custom.updateOperation(userName, new Date());
		
	}
	
	@Before("execution(* org.logic.tree.manager.impl.*.listUpdate*(..)) && args(customList, userName)")
	public <T extends CustomEntity> void auditUpdateListCustomEntity(List<T> customList, String userName){
		
		Date now = new Date();
		
		for(T custom: customList){
			custom.updateOperation(userName, now);
		}
		
	}
	
	@Before("execution(* org.logic.tree.manager.impl.*.delete*(..)) && args(custom, userName)")
	public <T extends CustomEntity> void auditDeleteCustomEntity(T custom, String userName){
		
		custom.deleteOperation(userName, new Date());
		
	}
	
	@Before("execution(* org.logic.tree.manager.impl.*.listDelete*(..)) && args(customList, userName)")
	public <T extends CustomEntity> void auditDeleteListCustomEntity(List<T> customList, String userName){
		
		Date now = new Date();
		for(T custom: customList){
			custom.deleteOperation(userName, now);
		}
		
	}

}
