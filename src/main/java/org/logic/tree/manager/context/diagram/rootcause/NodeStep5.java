package org.logic.tree.manager.context.diagram.rootcause;

import java.io.Serializable;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */
public class NodeStep5 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7917534681214938056L;

	
	private int key;
	
	private String text;
	
	private String brush;
	
	private String loc;
	
	private Integer parent;
	

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getBrush() {
		return brush;
	}

	public void setBrush(String brush) {
		this.brush = brush;
	}

	public String getLoc() {
		return loc;
	}

	public void setLoc(String loc) {
		this.loc = loc;
	}

	public Integer getParent() {
		return parent;
	}

	public void setParent(Integer parent) {
		this.parent = parent;
	}
	
	
	
}
