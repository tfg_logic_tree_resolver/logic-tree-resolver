package org.logic.tree.manager.context.diagram.rootcause;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */
public class DiagramStep2 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3675276307114282248L;

	private String _class;

	private String linkFromPortIdProperty;

	private String linkToPortIdProperty;

	private List<NodeStep2> nodeDataArray;

	private List<LinkStep2> linkDataArray;

	@JsonProperty("class")
	public String get_class() {
		return _class;
	}

	@JsonProperty("class")
	public void set_class(String _class) {
		this._class = _class;
	}

	public String getLinkFromPortIdProperty() {
		return linkFromPortIdProperty;
	}

	public void setLinkFromPortIdProperty(String linkFromPortIdProperty) {
		this.linkFromPortIdProperty = linkFromPortIdProperty;
	}

	public String getLinkToPortIdProperty() {
		return linkToPortIdProperty;
	}

	public void setLinkToPortIdProperty(String linkToPortIdProperty) {
		this.linkToPortIdProperty = linkToPortIdProperty;
	}

	public List<NodeStep2> getNodeDataArray() {
		return nodeDataArray;
	}

	public void setNodeDataArray(List<NodeStep2> nodeDataArray) {
		this.nodeDataArray = nodeDataArray;
	}

	public List<LinkStep2> getLinkDataArray() {
		return linkDataArray;
	}

	public void setLinkDataArray(List<LinkStep2> linkDataArray) {
		this.linkDataArray = linkDataArray;
	}

}
