package org.logic.tree.manager.context;

import static org.logic.tree.util.Color.GOAL_BEGIN;
import static org.logic.tree.util.Color.GOAL_END;
import static org.logic.tree.util.Color.GOAL_GAP;
import static org.logic.tree.util.Constants.STATUS_STEP_6;
import static org.logic.tree.util.MessageUtil.getMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.logic.tree.manager.context.diagram.rootcause.DiagramStep5;
import org.logic.tree.manager.context.diagram.rootcause.DiagramStep6;
import org.logic.tree.manager.context.diagram.rootcause.ModelData;
import org.logic.tree.manager.context.diagram.rootcause.NodeStep5;
import org.logic.tree.manager.context.diagram.rootcause.NodeStep6;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public class Step6 extends State {
	
	private Logger LOG = LoggerFactory.getLogger(Step6.class);
	
	public Step6() {
		super();
	    setIdStep(STATUS_STEP_6);
	}


	@Override
	public void goNext(ProblemContext context) throws JsonParseException, JsonMappingException, IOException {
		context.setState(new Step7());
	}
	
	@Override
	public void goPrevious(ProblemContext context) throws JsonParseException, JsonMappingException, IOException {
		context.setState(new Step5());	
	}

	@Override
	public String generateRootCauseDiagram(String previousDiagram)  throws JsonParseException, JsonMappingException, IOException{
		
		if(previousDiagram == null){
			return null;
		}
		
		
		DiagramStep5 diagram5 = (DiagramStep5) mapper.readValue(previousDiagram, DiagramStep5.class);
		
			
		// Get all nodes without children
		List<NodeStep5> nodes = diagram5.getNodeDataArray();

		Set<Integer> parents = new HashSet<Integer>();
		Set<Integer> allNodes = new HashSet<Integer>();
		Map<Integer, NodeStep5> mapNodes = new HashMap<Integer, NodeStep5>();

		for (NodeStep5 node : nodes) {
			if (node.getParent() != null) {
				parents.add(node.getParent());
			}

			allNodes.add(node.getKey());
			mapNodes.put(node.getKey(), node);
		}

		allNodes.removeAll(parents);

		if (allNodes.isEmpty()) {
			return null;
		}
		
		List<NodeStep6> nodesStep6 = new ArrayList<NodeStep6>();
		
		int key = 0;
		
		//Create headers and siders
		
		nodesStep6.add(new NodeStep6(key,getMessage("problem.root_cause.step_6.validation") , "Header", null, null, null));
		nodesStep6.add(new NodeStep6(++key,getMessage("problem.root_cause.step_6.selected"), "Sider",null,null,null));
		nodesStep6.add(new NodeStep6(++key,getMessage("problem.root_cause.step_6.description"),"Column Header",2,null,null));
		nodesStep6.add(new NodeStep6(++key,getMessage("problem.root_cause.step_6.implementation"),"Column Header",3,null,null));
		nodesStep6.add(new NodeStep6(++key,getMessage("problem.root_cause.step_6.solutions"),"Column Header",4,null,null));
		
		int keyMethod = 1;
		int row = 2;
		
		for(Integer idNode: allNodes){
			String textMethod = keyMethod+"."+mapNodes.get(idNode).getText();
			
			nodesStep6.add(new NodeStep6(++key, textMethod , "Row Sider", null, row, null));
			
			nodesStep6.add(new NodeStep6(++key,null,null,2,row,true));
			nodesStep6.add(new NodeStep6(++key,null,null,3,row,true));
			nodesStep6.add(new NodeStep6(++key,null,null,4,row,true));
			
			keyMethod++;
			row++;
		}
		
					
		
		ModelData modelData = diagram5.getModelData();
		
		if(modelData == null){
			return null;
		}
		
		modelData.setComplementaryModel(createPrioritiesDiagram(keyMethod));
		
		
		DiagramStep6 diagram6A = new DiagramStep6();
		
		diagram6A.set_class("go.GraphLinksModel");
		diagram6A.setModelData(modelData);
		diagram6A.setNodeDataArray(nodesStep6);
		
		
		LOG.debug("Diagram created");
		
		return mapper.writeValueAsString(diagram6A);
		

	}

	@Override
	public String generateGoalsDiagram(String previousDiagram) throws JsonParseException, JsonMappingException, IOException{
		if(previousDiagram == null){
			return null;
		}
		
		org.logic.tree.manager.context.diagram.goals.DiagramStep5 diagram5 = mapper.readValue(previousDiagram, org.logic.tree.manager.context.diagram.goals.DiagramStep5.class);
	
		
		org.logic.tree.manager.context.diagram.goals.DiagramStep6 diagram6 = new org.logic.tree.manager.context.diagram.goals.DiagramStep6(); 
		List<org.logic.tree.manager.context.diagram.goals.NodeStep6> nodesStep6 = new ArrayList<org.logic.tree.manager.context.diagram.goals.NodeStep6>();
		org.logic.tree.manager.context.diagram.goals.ModelData modelData = diagram5.getModelData();
		
		nodesStep6.add(new org.logic.tree.manager.context.diagram.goals.NodeStep6("base", true, "100 50", "900 450", null, null));
		nodesStep6.add(new org.logic.tree.manager.context.diagram.goals.NodeStep6(modelData.getFromSituation(), false,"100 400", modelData.getSizeFromSituation(), "base", GOAL_BEGIN));

		nodesStep6.add(new org.logic.tree.manager.context.diagram.goals.NodeStep6(modelData.getGoalSituation(), false,"850 50", modelData.getSizeGoalSituation(), "base", GOAL_END));
		
		int xposition = 300;
		
		for(String idea: modelData.getIdeas()){
		
			nodesStep6.add(new org.logic.tree.manager.context.diagram.goals.NodeStep6(idea+": 0"+modelData.getUnits(), false,xposition +" 50", "150 100", "base", GOAL_GAP));
			
			xposition +=150;
		}
		
			
		
		diagram6.set_class("go.GraphLinksModel");
		diagram6.setNodeDataArray(nodesStep6);
		diagram6.setModelData(modelData);
		
		LOG.debug("Diagram created");
		
		return mapper.writeValueAsString(diagram6);
	}

	@Override
	public String generateProsConsDiagram(String previousDiagram) {
		// TODO Auto-generated method stub
		return null;
	}

	private String createPrioritiesDiagram(int numberMethods) throws JsonProcessingException{
		
		int key = 0;
		
		List<NodeStep6> nodesPrioritiesDiagram = new ArrayList<NodeStep6>();
		
		nodesPrioritiesDiagram.add(new NodeStep6(key, getMessage("problem.root_cause.step_6.ease"), "Header", null, null, null));
		nodesPrioritiesDiagram.add(new NodeStep6(++key, getMessage("problem.root_cause.step_6.impact"), "Sider", null, null, null));
		nodesPrioritiesDiagram.add(new NodeStep6(++key,getMessage("problem.root_cause.step_6.easy"), "Column Header", 2, null, null));
		nodesPrioritiesDiagram.add(new NodeStep6(++key,getMessage("problem.root_cause.step_6.difficult"), "Column Header", 3, null, null));
		nodesPrioritiesDiagram.add(new NodeStep6(++key,getMessage("problem.root_cause.step_6.high"), "Row Sider", null, 2, null));
		nodesPrioritiesDiagram.add(new NodeStep6(++key,getMessage("problem.root_cause.step_6.low"), "Row Sider", null, 3, null));
		
		nodesPrioritiesDiagram.add(new NodeStep6(22,null, null, 2, 2, true));
		nodesPrioritiesDiagram.add(new NodeStep6(23,null, null, 2, 3, true));
		nodesPrioritiesDiagram.add(new NodeStep6(32,null, null, 3, 2, true));
		nodesPrioritiesDiagram.add(new NodeStep6(33,null, null, 3, 3, true));
		
		
		for(int i= 1; i<numberMethods; i++){
			NodeStep6 priority = new NodeStep6(++key, String.valueOf(i), "Priority", null, null, null);
			priority.setGroup(String.valueOf(22));
			
			nodesPrioritiesDiagram.add(priority);
		}
		
		
		DiagramStep6 diagramPriorities = new DiagramStep6();
		
				
		diagramPriorities.set_class("go.GraphLinksModel");
		diagramPriorities.setNodeDataArray(nodesPrioritiesDiagram);
		
		return mapper.writeValueAsString(diagramPriorities);
	}
		
}
