package org.logic.tree.manager.context;

import static org.logic.tree.util.Color.GOAL_BEGIN;
import static org.logic.tree.util.Color.GOAL_END;
import static org.logic.tree.util.Constants.STATUS_STEP_3;
import static org.logic.tree.util.MessageUtil.getMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.logic.tree.manager.context.diagram.rootcause.DiagramStep2;
import org.logic.tree.manager.context.diagram.rootcause.DiagramStep3;
import org.logic.tree.manager.context.diagram.rootcause.InfoCase;
import org.logic.tree.manager.context.diagram.rootcause.LinkStep2;
import org.logic.tree.manager.context.diagram.rootcause.ModelData;
import org.logic.tree.manager.context.diagram.rootcause.NodeStep2;
import org.logic.tree.manager.context.diagram.rootcause.NodeStep3;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public class Step3 extends State {
	
	private Logger LOG = LoggerFactory.getLogger(Step3.class);
	
	public Step3() {
		super();
	    setIdStep(STATUS_STEP_3);
	}


	@Override
	public void goNext(ProblemContext context) throws JsonParseException, JsonMappingException, IOException {
		context.setState(new Step4());

	}
	
	@Override
	public void goPrevious(ProblemContext context) throws JsonParseException, JsonMappingException, IOException {
		context.setState(new Step2());	
	}

	@Override
	public String generateRootCauseDiagram(String previousDiagram)  throws JsonParseException, JsonMappingException, IOException{
		
		if(previousDiagram == null){
			return null;
		}
		
		DiagramStep2 diagram2 = mapper.readValue(previousDiagram, DiagramStep2.class);
		
		List<InfoCase> listInfoCase = new ArrayList<InfoCase>();
		List<NodeStep3> listNodes = new ArrayList<NodeStep3>();
		
		int key = 1;
		
		
		for(NodeStep2 node: diagram2.getNodeDataArray()){
			if(node.getCategory()==null && node.getFigure() == null){
				Integer idNumberCase = searchKeyNumberCase(node.getKey(), diagram2.getLinkDataArray());
				
				int nodeParent = key;
				
				listNodes.add(createNode(node.getText(), nodeParent));
				listNodes.add(createSubnode(getMessage("problem.root_cause.step_3.analysis"), ++key, nodeParent));
				listNodes.add(createSubnode(getMessage("problem.root_cause.step_3.hypothesis"),++key,nodeParent));
				listNodes.add(createSubnode(getMessage("problem.root_cause.step_3.reasoning"), ++key, nodeParent));
				listNodes.add(createSubnode(getMessage("problem.root_cause.step_3.information_source"),++key,nodeParent));
				listNodes.add(createSubnode(getMessage("problem.root_cause.step_3.plan"),++key,nodeParent));
				
				if(idNumberCase != null){
					String numberOfCases = searchNumberCase(idNumberCase, diagram2.getNodeDataArray());
					
					listInfoCase.add(new InfoCase(node.getText(), numberOfCases));
				}
				
				key++;
			}
		}
		
		DiagramStep3 diagramStep3 = new DiagramStep3();
		
		ModelData modelData = new ModelData();
		
		modelData.setListInfoCase(listInfoCase);
		
		diagramStep3.setModelData(modelData);
		diagramStep3.set_class("go.GraphLinksModel");
		diagramStep3.setNodeDataArray(listNodes);
		
		LOG.debug("Diagram created");
		
		return mapper.writeValueAsString(diagramStep3);
	}

	@Override
	public String generateGoalsDiagram(String previousDiagram)  throws JsonParseException, JsonMappingException, IOException {
		if(previousDiagram == null){
			return null;
		}
		
		org.logic.tree.manager.context.diagram.goals.DiagramStep2 diagram2 = mapper.readValue(previousDiagram, org.logic.tree.manager.context.diagram.goals.DiagramStep2.class);

		List<org.logic.tree.manager.context.diagram.goals.NodeStep2> nodesStep2 = diagram2.getNodeDataArray();
		
		org.logic.tree.manager.context.diagram.goals.ModelData modelData = diagram2.getModelData();
		
		for(org.logic.tree.manager.context.diagram.goals.NodeStep2 node:nodesStep2){
			if(GOAL_BEGIN.equals(node.getColor())){
				modelData.setFromSituation(node.getKey());
				modelData.setSizeFromSituation(node.getSize());
			}else if (GOAL_END.equals(node.getColor())){
				modelData.setGoalSituation(node.getKey());
				modelData.setSizeGoalSituation(node.getSize());
			}
		}
		
		
		org.logic.tree.manager.context.diagram.goals.NodeStep3 nodeStep3 = new org.logic.tree.manager.context.diagram.goals.NodeStep3();
		
		nodeStep3.setKey(0);
		nodeStep3.setText(getMessage("problem.goals.step_3.question"));
		nodeStep3.setBrush("coral");
		nodeStep3.setLoc("0 0");
		
		
		org.logic.tree.manager.context.diagram.goals.DiagramStep3 diagramStep3 = new org.logic.tree.manager.context.diagram.goals.DiagramStep3();
		
		diagramStep3.set_class("go.TreeModel");
		diagramStep3.setModelData(modelData);
		diagramStep3.setNodeDataArray(Collections.singletonList(nodeStep3));
		
		LOG.debug("Diagram created");
		
		return mapper.writeValueAsString(diagramStep3);
		
	}

	@Override
	public String generateProsConsDiagram(String previousDiagram) throws IOException {
		if(previousDiagram == null){
			return null;
		}
		
		org.logic.tree.manager.context.diagram.proscons.DiagramStep2 diagram2 = mapper.readValue(previousDiagram, org.logic.tree.manager.context.diagram.proscons.DiagramStep2.class);

		
		List<org.logic.tree.manager.context.diagram.proscons.NodeStep2> listNodes = diagram2.getNodeDataArray();
		

		Map<Integer, Integer> mapVotosOpcion = new HashMap<Integer,Integer>();
		

		
		for(org.logic.tree.manager.context.diagram.proscons.NodeStep2 nodeStep2:listNodes){
			
			
			
			if("Positive".equals(nodeStep2.getCategory())){
				int lastParent = searchLastParent(nodeStep2.getGroup(), listNodes);
								
				if(!mapVotosOpcion.containsKey(lastParent)){
					mapVotosOpcion.put(lastParent, 1);
				}else{
					int votos = mapVotosOpcion.get(lastParent);
					
					mapVotosOpcion.put(lastParent, ++votos);
				}
			}else if("Negative".equals(nodeStep2.getCategory())){
				int lastParent = searchLastParent(nodeStep2.getGroup(), listNodes);
				
				if(!mapVotosOpcion.containsKey(lastParent)){
					mapVotosOpcion.put(lastParent, -1);
				}else{
					int votos = mapVotosOpcion.get(lastParent);			
					mapVotosOpcion.put(lastParent, --votos);
				}
			}
			
		}
		
		if(!mapVotosOpcion.isEmpty()){
			
			Integer keyBestOption = null;
			Integer countBestOption = null;
			
			List<Integer> keyList  = new ArrayList<Integer>(mapVotosOpcion.keySet());
			
			for(Integer key: keyList){
							
				if(countBestOption == null || mapVotosOpcion.get(key).compareTo(countBestOption) > 0 ){
					keyBestOption = key;
					countBestOption = mapVotosOpcion.get(key);
				}
			}
			
			for(org.logic.tree.manager.context.diagram.proscons.NodeStep2 nodeStep2:listNodes){
				
				if( keyBestOption.equals(nodeStep2.getKey())){
					nodeStep2.setCategory("OfGroupsBestOption");
				}
				
			}
		}
				
			

		
		diagram2.setNodeDataArray(listNodes);
		
		LOG.debug("Diagram created");
		
		return mapper.writeValueAsString(diagram2);
	}
	
	private Integer searchKeyNumberCase(int key, List<LinkStep2> linkDataArray){
		
		if(linkDataArray == null){
			return null;
		}
		
		for(LinkStep2 link:linkDataArray){
			if(link.getFrom() == key){
				return link.getTo();
			}
		}
		
		return null;
	}
	
	private String searchNumberCase(int key, List<NodeStep2> nodes){
		for(NodeStep2 node: nodes){
			if(node.getKey() == key && "Number".equals(node.getCategory())){
				return node.getText();
			}
		}
		
		return null;
	}
	
	private NodeStep3 createNode(String text, int key){
		NodeStep3 node = new NodeStep3();
		
		node.setKey(key);
		node.setText(text);
		node.setIsGroup(true);
		node.setCategory("OfGroups");
		
		return node;
	}
	
	private NodeStep3 createSubnode(String text, int key, int nodeParent){
		NodeStep3 node = new NodeStep3();
		
		node.setKey(key);
		node.setText(text);
		node.setIsGroup(true);
		node.setCategory("OfNodes");
		node.setGroup(nodeParent);
		
		return node;
	}

	private Integer searchLastParent(int parentKey ,List<org.logic.tree.manager.context.diagram.proscons.NodeStep2> listNodes){
		
		Integer lastParent = null;
		
		for(org.logic.tree.manager.context.diagram.proscons.NodeStep2 nodeStep2:listNodes){
			if(nodeStep2.getKey() == parentKey && nodeStep2.getIsGroup() && nodeStep2.getGroup() == null){
				lastParent = parentKey;
			}else if(nodeStep2.getKey() == parentKey && nodeStep2.getIsGroup() && nodeStep2.getGroup() != null){
				List<org.logic.tree.manager.context.diagram.proscons.NodeStep2> newListNodes = new ArrayList<org.logic.tree.manager.context.diagram.proscons.NodeStep2>(listNodes);
				int newParent = nodeStep2.getGroup();
				newListNodes.remove(nodeStep2);
				lastParent =  searchLastParent(newParent, newListNodes);
			}
		}
		
		return lastParent;
	}
	

	
}
