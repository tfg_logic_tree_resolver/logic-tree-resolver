package org.logic.tree.manager.context;

import static org.logic.tree.util.Color.GOAL_BEGIN;
import static org.logic.tree.util.Color.GOAL_END;
import static org.logic.tree.util.Color.GOAL_GAP;
import static org.logic.tree.util.Constants.STATUS_STEP_2;
import static org.logic.tree.util.MessageUtil.getMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.logic.tree.manager.context.diagram.rootcause.DiagramStep1;
import org.logic.tree.manager.context.diagram.rootcause.DiagramStep2;
import org.logic.tree.manager.context.diagram.rootcause.LinkStep2;
import org.logic.tree.manager.context.diagram.rootcause.NodeStep1;
import org.logic.tree.manager.context.diagram.rootcause.NodeStep2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public class Step2 extends State {

	private Logger LOG = LoggerFactory.getLogger(Step2.class);

	public Step2() {
		super();
		setIdStep(STATUS_STEP_2);
	}

	@Override
	public void goNext(ProblemContext context) throws JsonParseException, JsonMappingException, IOException {
		context.setState(new Step3());
	}

	@Override
	public void goPrevious(ProblemContext context) throws JsonParseException, JsonMappingException, IOException {
		context.setState(new Step1());
	}

	@Override
	public String generateRootCauseDiagram(String previousDiagram) throws JsonParseException, JsonMappingException, IOException {

		if(previousDiagram == null){
			return null;
		}
		
		DiagramStep1 diagram1 = mapper.readValue(previousDiagram, DiagramStep1.class);

		// Get all nodes without children

		List<NodeStep1> nodes = diagram1.getNodeDataArray();

		Set<Integer> parents = new HashSet<Integer>();
		Set<Integer> allNodes = new HashSet<Integer>();
		Map<Integer, NodeStep1> mapNodes = new HashMap<Integer, NodeStep1>();

		for (NodeStep1 node : nodes) {
			if (node.getParent() != null) {
				parents.add(node.getParent());
			}

			allNodes.add(node.getKey());
			mapNodes.put(node.getKey(), node);
		}

		allNodes.removeAll(parents);

		if (allNodes.isEmpty()) {
			return null;
		}

		List<NodeStep2> listNodes = new ArrayList<NodeStep2>();
		List<LinkStep2> listLinks = new ArrayList<LinkStep2>();

		int nodeKey = 1;
		// Principal column of nodes
		int xprimary = 0;
		int yprimary = 0;

		// Secondary column of nodes
		int xsecondary = 400;
		int ysecondary = 100;

		NodeStep2 nodeTotal = new NodeStep2();

		nodeTotal.setKey(nodeKey);
		nodeTotal.setCategory("Total");
		nodeTotal.setLoc(xprimary + " " + yprimary);
		nodeTotal.setText("0");
		listNodes.add(nodeTotal);

		listLinks.add(createRectangleCircleLink(nodeKey, nodeKey + 1));

		Iterator<Integer> iteratorNotParents = allNodes.iterator();

		while (iteratorNotParents.hasNext()) {

			NodeStep1 nodeStep1 = mapNodes.get((Integer) iteratorNotParents.next());

			String text = nodeStep1.getName();

			// This is the last cause
			if (!iteratorNotParents.hasNext()) {

				// crate rectangle node
				int rectangleKey = ++nodeKey;
				yprimary = yprimary + 200;

				listNodes.add(createRectangleNode(rectangleKey, text, xprimary, yprimary));

				// creating circle node
				int circleKey = ++nodeKey;
				yprimary = yprimary + 100;

				listNodes.add(createCircleNode(circleKey, xprimary, yprimary));

				// creating link between rectangle and circle
				listLinks.add(createRectangleCircleLink(rectangleKey, circleKey));

				break;

			}

			// Creating diamond node
			int diamondKey = ++nodeKey;
			yprimary = yprimary + 200;

			listNodes.add(createDiamondNode(diamondKey, text, xprimary, yprimary));

			// Creating rectangle node
			int rectangleKey = ++nodeKey;
			ysecondary = ysecondary + 100;

			listNodes.add(createRectangleNode(rectangleKey, text, xsecondary, ysecondary));

			// creating link between diamond and rectangle
			listLinks.add(createDiamondRectangleLink(diamondKey, rectangleKey));

			// creating node circle
			int circleKey = ++nodeKey;
			ysecondary = ysecondary + 100;

			listNodes.add(createCircleNode(circleKey, xsecondary, ysecondary));

			// creating link between rectangle and circle
			listLinks.add(createRectangleCircleLink(rectangleKey, circleKey));

			// creating link between diamond and next object
			listLinks.add(createDiamondNextObjectLink(diamondKey, nodeKey + 1));

		}

		DiagramStep2 diagramStep2 = new DiagramStep2();

		diagramStep2.set_class("go.GraphLinksModel");
		diagramStep2.setLinkFromPortIdProperty("fromPort");
		diagramStep2.setLinkToPortIdProperty("toPort");
		diagramStep2.setNodeDataArray(listNodes);
		diagramStep2.setLinkDataArray(listLinks);

		LOG.debug("Diagram created");
		
		return mapper.writeValueAsString(diagramStep2);

	}

	@Override
	public String generateGoalsDiagram(String previousDiagram) throws JsonParseException, JsonMappingException, IOException {
		if(previousDiagram == null){
			return null;
		}
		
		org.logic.tree.manager.context.diagram.goals.DiagramStep1 diagram1 = mapper.readValue(previousDiagram, org.logic.tree.manager.context.diagram.goals.DiagramStep1.class);

		List<org.logic.tree.manager.context.diagram.goals.NodeStep1> nodes = diagram1.getNodeDataArray();
		
		//Guetting the units
		String units = "uds.";
		
		for(org.logic.tree.manager.context.diagram.goals.NodeStep1 node: nodes){
			if("OfGroups".equals(node.getCategory())){
				units = node.getText();
				break;
			}
		}
		
		org.logic.tree.manager.context.diagram.goals.DiagramStep2 diagram2 = new org.logic.tree.manager.context.diagram.goals.DiagramStep2(); 
		List<org.logic.tree.manager.context.diagram.goals.NodeStep2> nodesStep2 = new ArrayList<org.logic.tree.manager.context.diagram.goals.NodeStep2>();
		org.logic.tree.manager.context.diagram.goals.ModelData modelData = new org.logic.tree.manager.context.diagram.goals.ModelData();
		
		nodesStep2.add(new org.logic.tree.manager.context.diagram.goals.NodeStep2("base", true, "100 50", "550 450", null, null));
		nodesStep2.add(new org.logic.tree.manager.context.diagram.goals.NodeStep2(getMessage("problem.goals.step_2.from")+": 0"+units, false,"100 400", "150 100", "base", GOAL_BEGIN));
		nodesStep2.add(new org.logic.tree.manager.context.diagram.goals.NodeStep2(getMessage("problem.goals.step_2.gap")+": 0"+units, false,"300 50", "150 350", "base", GOAL_GAP));
		nodesStep2.add(new org.logic.tree.manager.context.diagram.goals.NodeStep2(getMessage("problem.goals.step_2.goal")+": 0"+units, false,"500 50", "150 450", "base", GOAL_END));
		
		modelData.setUnits(units);
		
		diagram2.set_class("go.GraphLinksModel");
		diagram2.setNodeDataArray(nodesStep2);
		diagram2.setModelData(modelData);
		
		LOG.debug("Diagram created");
		
		return mapper.writeValueAsString(diagram2);
	}

	@Override
	public String generateProsConsDiagram(String previousDiagram) throws JsonParseException, JsonMappingException, IOException {
		
		if(previousDiagram == null){
			return null;
		}
		
		org.logic.tree.manager.context.diagram.proscons.DiagramStep1 diagram1 = mapper.readValue(previousDiagram, org.logic.tree.manager.context.diagram.proscons.DiagramStep1.class);

		
		List<org.logic.tree.manager.context.diagram.proscons.NodeStep1> listNodes = diagram1.getNodeDataArray();
		
		List<org.logic.tree.manager.context.diagram.proscons.NodeStep2> listNodesStep2 = new ArrayList<org.logic.tree.manager.context.diagram.proscons.NodeStep2>();
		
		for(org.logic.tree.manager.context.diagram.proscons.NodeStep1 nodeStep1:listNodes){
			
			if(nodeStep1.getIsGroup() && ("OfGroups".equals(nodeStep1.getCategory()) ||"OfGroupsBestOption".equals(nodeStep1.getCategory()) ) ){
				listNodesStep2.add(createNodeProsCons(nodeStep1.getText(), nodeStep1.getKey()));	
			}else if(nodeStep1.getIsGroup() && "OfNodes".equals(nodeStep1.getCategory())){
				listNodesStep2.add(createSubnodeProsCons(nodeStep1.getText(), nodeStep1.getKey(), nodeStep1.getGroup()));
			}else{
				listNodesStep2.add(createSubnodeProsCons(nodeStep1.getText(), nodeStep1.getKey(), nodeStep1.getGroup()));
			}
			
		}
		
			
		
		org.logic.tree.manager.context.diagram.proscons.DiagramStep2 diagramStep2 = new org.logic.tree.manager.context.diagram.proscons.DiagramStep2();
			
		
		diagramStep2.set_class("go.GraphLinksModel");
		diagramStep2.setNodeDataArray(listNodesStep2);
		
		LOG.debug("Diagram created");
		
		return mapper.writeValueAsString(diagramStep2);
	}

	private NodeStep2 createDiamondNode(int key, String text, int positionX, int positionY) {
		NodeStep2 nodeDiamond = new NodeStep2();

		nodeDiamond.setKey(key);
		nodeDiamond.setText(text);
		nodeDiamond.setFigure("Diamond");
		nodeDiamond.setLoc(positionX + " " + positionY);

		return nodeDiamond;
	}

	private NodeStep2 createRectangleNode(int key, String text, int positionX, int positionY) {
		NodeStep2 nodeRectangle = new NodeStep2();
		nodeRectangle.setKey(key);
		nodeRectangle.setText(text);
		nodeRectangle.setLoc(positionX + " " + positionY);

		return nodeRectangle;
	}

	private LinkStep2 createDiamondRectangleLink(int from, int to) {
		LinkStep2 linkDiamondRectangle = new LinkStep2();
		linkDiamondRectangle.setFrom(from);
		linkDiamondRectangle.setTo(to);
		linkDiamondRectangle.setFromPort("R");
		linkDiamondRectangle.setToPort("L");
		linkDiamondRectangle.setVisible(true);

		return linkDiamondRectangle;
	}

	private NodeStep2 createCircleNode(int key, int positionX, int positionY) {
		NodeStep2 nodeCircle = new NodeStep2();

		nodeCircle.setKey(key);
		nodeCircle.setCategory("Number");
		nodeCircle.setText("0");
		nodeCircle.setLoc(positionX + " " + positionY);

		return nodeCircle;
	}

	private LinkStep2 createRectangleCircleLink(int from, int to) {
		LinkStep2 linkRectangleCircle = new LinkStep2();
		linkRectangleCircle.setFrom(from);
		linkRectangleCircle.setTo(to);
		linkRectangleCircle.setFromPort("B");
		linkRectangleCircle.setToPort("T");

		return linkRectangleCircle;
	}

	private LinkStep2 createDiamondNextObjectLink(int from, int to) {
		LinkStep2 linkDiamondNextObject = new LinkStep2();
		linkDiamondNextObject.setFrom(from);
		linkDiamondNextObject.setTo(to);
		linkDiamondNextObject.setFromPort("B");
		linkDiamondNextObject.setToPort("T");
		linkDiamondNextObject.setVisible(true);
		linkDiamondNextObject.setText("No");

		return linkDiamondNextObject;
	}

	
	private org.logic.tree.manager.context.diagram.proscons.NodeStep2 createNodeProsCons(String text, int key){
		org.logic.tree.manager.context.diagram.proscons.NodeStep2 node = new org.logic.tree.manager.context.diagram.proscons.NodeStep2();
		
		node.setKey(key);
		node.setText(text);
		node.setIsGroup(true);
		node.setCategory("OfGroups");
		
		return node;
	}
	
	private org.logic.tree.manager.context.diagram.proscons.NodeStep2 createSubnodeProsCons(String text, int key, int nodeParent){
		org.logic.tree.manager.context.diagram.proscons.NodeStep2 node = new org.logic.tree.manager.context.diagram.proscons.NodeStep2();
		
		node.setKey(key);
		node.setText(text);
		node.setIsGroup(true);
		node.setCategory("OfNodes");
		node.setGroup(nodeParent);
		
		return node;
	}
	
}
