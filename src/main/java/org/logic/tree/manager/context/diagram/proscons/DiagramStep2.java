package org.logic.tree.manager.context.diagram.proscons;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */
public class DiagramStep2 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6856151339560271377L;

	private String _class;

	private List<NodeStep2> nodeDataArray;

	@JsonProperty("class")
	public String get_class() {
		return _class;
	}

	@JsonProperty("class")
	public void set_class(String _class) {
		this._class = _class;
	}

	public List<NodeStep2> getNodeDataArray() {
		return nodeDataArray;
	}

	public void setNodeDataArray(List<NodeStep2> nodeDataArray) {
		this.nodeDataArray = nodeDataArray;
	}

}
