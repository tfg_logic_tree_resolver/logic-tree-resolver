package org.logic.tree.manager.context.diagram.rootcause;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */
public class DiagramStep4 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6807395669332627658L;

	private String _class;

	private boolean copiesArrays;

	private boolean copiesArrayObjects;

	private List<NodeStep4> nodeDataArray;

	@JsonProperty("class")
	public String get_class() {
		return _class;
	}

	@JsonProperty("class")
	public void set_class(String _class) {
		this._class = _class;
	}

	public boolean getCopiesArrays() {
		return copiesArrays;
	}

	public void setCopiesArrays(boolean copiesArrays) {
		this.copiesArrays = copiesArrays;
	}

	public boolean getCopiesArrayObjects() {
		return copiesArrayObjects;
	}

	public void setCopiesArrayObjects(boolean copiesArrayObjects) {
		this.copiesArrayObjects = copiesArrayObjects;
	}

	public List<NodeStep4> getNodeDataArray() {
		return nodeDataArray;
	}

	public void setNodeDataArray(List<NodeStep4> nodeDataArray) {
		this.nodeDataArray = nodeDataArray;
	}

}
