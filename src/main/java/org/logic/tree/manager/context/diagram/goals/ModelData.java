package org.logic.tree.manager.context.diagram.goals;

import java.io.Serializable;
import java.util.List;

public class ModelData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2143894831742448182L;

	private String units;

	private String fromSituation;

	private String sizeFromSituation;

	private String goalSituation;

	private String sizeGoalSituation;
	
	private List<String> ideas;

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public String getFromSituation() {
		return fromSituation;
	}

	public void setFromSituation(String fromSituation) {
		this.fromSituation = fromSituation;
	}

	public String getSizeFromSituation() {
		return sizeFromSituation;
	}

	public void setSizeFromSituation(String sizeFromSituation) {
		this.sizeFromSituation = sizeFromSituation;
	}

	public String getGoalSituation() {
		return goalSituation;
	}

	public void setGoalSituation(String goalSituation) {
		this.goalSituation = goalSituation;
	}

	public String getSizeGoalSituation() {
		return sizeGoalSituation;
	}

	public void setSizeGoalSituation(String sizeGoalSituation) {
		this.sizeGoalSituation = sizeGoalSituation;
	}

	public List<String> getIdeas() {
		return ideas;
	}

	public void setIdeas(List<String> ideas) {
		this.ideas = ideas;
	}

	
}
