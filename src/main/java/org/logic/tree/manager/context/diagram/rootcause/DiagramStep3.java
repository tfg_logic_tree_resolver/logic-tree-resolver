package org.logic.tree.manager.context.diagram.rootcause;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */
public class DiagramStep3 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9187565819952257039L;

	private String _class;
	
	private List<NodeStep3> nodeDataArray;
	
	private ModelData modelData;
	

	@JsonProperty("class")
	public String get_class() {
		return _class;
	}

	@JsonProperty("class")
	public void set_class(String _class) {
		this._class = _class;
	}

	public List<NodeStep3> getNodeDataArray() {
		return nodeDataArray;
	}

	public void setNodeDataArray(List<NodeStep3> nodeDataArray) {
		this.nodeDataArray = nodeDataArray;
	}

	public ModelData getModelData() {
		return modelData;
	}

	public void setModelData(ModelData modelData) {
		this.modelData = modelData;
	}

	
	
}
