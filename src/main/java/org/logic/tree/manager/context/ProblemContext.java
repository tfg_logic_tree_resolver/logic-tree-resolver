package org.logic.tree.manager.context;

import static org.logic.tree.util.Constants.PROBLEM_TYPE_GOALS;
import static org.logic.tree.util.Constants.PROBLEM_TYPE_PROS_CONS;
import static org.logic.tree.util.Constants.PROBLEM_TYPE_ROOT_CAUSES;
import static org.logic.tree.util.Constants.STATUS_STEP_1;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */
public class ProblemContext {

	private Long idProblem;
	private Integer problemType;
	private State currentState;
	private String currentDiagram;

	public ProblemContext(Long idProblem, Integer problemType, State currentState) throws JsonParseException, JsonMappingException, IOException {
		super();
		this.idProblem = idProblem;
		this.problemType = problemType;
		setState(currentState);
	}

	public String getCurrentDiagram() {
		return currentDiagram;
	}

	public void goNext() throws JsonParseException, JsonMappingException, IOException {
		currentState.goNext(this);
	}

	void setState(State actualState) throws JsonParseException, JsonMappingException, IOException {
		this.currentState = actualState;

		String previousDiagram = this.currentDiagram;

		if (previousDiagram != null || actualState.idStep.equals(STATUS_STEP_1)) {
			switch (problemType) {
			case PROBLEM_TYPE_ROOT_CAUSES:
				this.currentDiagram = this.currentState.generateRootCauseDiagram(previousDiagram);
				break;
			case PROBLEM_TYPE_GOALS:
				this.currentDiagram = this.currentState.generateGoalsDiagram(previousDiagram);
				break;
			case PROBLEM_TYPE_PROS_CONS:
				this.currentDiagram = this.currentState.generateProsConsDiagram(previousDiagram);
				break;
			}
		}
	}

	public void goPrevious() throws JsonParseException, JsonMappingException, IOException {
		currentState.goPrevious(this);
	}

	public State getCurrentState() {
		return currentState;
	}

	public Long getIdProblem() {
		return idProblem;
	}

	public void setCurrentDiagram(String currentDiagram) {
		this.currentDiagram = currentDiagram;
	}

}
