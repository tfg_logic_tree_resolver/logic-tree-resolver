package org.logic.tree.manager.context;

import static org.logic.tree.util.Constants.STATUS_STEP_4;
import static org.logic.tree.util.MessageUtil.getMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.logic.tree.manager.context.diagram.rootcause.DiagramStep3;
import org.logic.tree.manager.context.diagram.rootcause.DiagramStep4;
import org.logic.tree.manager.context.diagram.rootcause.InfoCase;
import org.logic.tree.manager.context.diagram.rootcause.NodeStep4;
import org.logic.tree.manager.context.diagram.rootcause.Slice;
import org.logic.tree.util.Color;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;



/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public class Step4 extends State {
	
	private Logger LOG = LoggerFactory.getLogger(Step4.class);
	
	public Step4() {
		super();
	    setIdStep(STATUS_STEP_4);
	}


	@Override
	public void goNext(ProblemContext context) throws JsonParseException, JsonMappingException, IOException {
		context.setState(new Step5());
	}
	
	@Override
	public void goPrevious(ProblemContext context) throws JsonParseException, JsonMappingException, IOException {
		context.setState(new Step3());	
	}

	@Override
	public String generateRootCauseDiagram(String previousDiagram)  throws JsonParseException, JsonMappingException, IOException{
		if(previousDiagram == null){
			return null;
		}
		DiagramStep3 diagram3 = (DiagramStep3) mapper.readValue(previousDiagram, DiagramStep3.class);
		
		
		List<Slice> slices = new ArrayList<Slice>();
		
		if(diagram3.getModelData()!= null && !diagram3.getModelData().getListInfoCase().isEmpty()){
		
			List<InfoCase> listInfoCase = diagram3.getModelData().getListInfoCase();
			List<String> colorSlices = Color.randomColor(listInfoCase.size());
			
			int keyColorSlices = 0;
			
			for(InfoCase infoCase: listInfoCase){
				
				int number = StringUtils.isNumeric(infoCase.getNumber())?Integer.valueOf(infoCase.getNumber()):0;
				Slice slice = new Slice(infoCase.getCaseText(), number, colorSlices.get(keyColorSlices));
				keyColorSlices++;
				
				slices.add(slice);
			
			}
		}
		
		NodeStep4 nodeDataArray = new NodeStep4();
		
		nodeDataArray.setKey(0);
		nodeDataArray.setNamePie(getMessage("problem.root_cause.step_4.real_cases"));
		nodeDataArray.setNameGuessedPie(getMessage("problem.root_cause.step_4.guessed_cases"));
		nodeDataArray.setSlices(slices);
		nodeDataArray.setGuessedslices(slices);
		
		DiagramStep4 diagram4 = new DiagramStep4();
		
		diagram4.set_class("go.Model");
		diagram4.setCopiesArrayObjects(true);
		diagram4.setCopiesArrays(true);
		diagram4.setNodeDataArray(Collections.singletonList(nodeDataArray));
		
		LOG.debug("Diagram created");
		
		return mapper.writeValueAsString(diagram4);
	}

	@Override
	public String generateGoalsDiagram(String previousDiagram) throws JsonParseException, JsonMappingException, IOException {
		
		if(previousDiagram == null){
			return null;
		}
		
		org.logic.tree.manager.context.diagram.goals.DiagramStep3 diagram3 =  mapper.readValue(previousDiagram, org.logic.tree.manager.context.diagram.goals.DiagramStep3.class);
		
		
		// Get all nodes without children
		List<org.logic.tree.manager.context.diagram.goals.NodeStep3> nodes = diagram3.getNodeDataArray();

		Set<Integer> parents = new HashSet<Integer>();
		Set<Integer> allNodes = new HashSet<Integer>();
		Map<Integer, org.logic.tree.manager.context.diagram.goals.NodeStep3> mapNodes = new HashMap<Integer, org.logic.tree.manager.context.diagram.goals.NodeStep3>();

		for (org.logic.tree.manager.context.diagram.goals.NodeStep3 node : nodes) {
			if (node.getParent() != null) {
				parents.add(node.getParent());
			}

			allNodes.add(node.getKey());
			mapNodes.put(node.getKey(), node);
		}

		allNodes.removeAll(parents);

		if (allNodes.isEmpty()) {
			return null;
		}
		
		List<org.logic.tree.manager.context.diagram.goals.NodeStep4> nodesStep4 = new ArrayList<org.logic.tree.manager.context.diagram.goals.NodeStep4>();
		
		int key = 0;
		
		nodesStep4.add(new org.logic.tree.manager.context.diagram.goals.NodeStep4(getMessage("problem.goals.step_4.high_easy"), true, "0 0", "250 250", null, null));
		nodesStep4.add(new org.logic.tree.manager.context.diagram.goals.NodeStep4(getMessage("problem.goals.step_4.high_hard"), true, "250 0", "250 250", null, null));
		nodesStep4.add(new org.logic.tree.manager.context.diagram.goals.NodeStep4(getMessage("problem.goals.step_4.low_easy"), true, "0 250", "250 250", null, null));
		nodesStep4.add(new org.logic.tree.manager.context.diagram.goals.NodeStep4(getMessage("problem.goals.step_4.low_hard"), true, "250 250", "250 250", null, null));
				
		List<String> colors = Color.randomColor(allNodes.size());

		for(Integer idNode:allNodes){
			org.logic.tree.manager.context.diagram.goals.NodeStep3 nodeStep3 = mapNodes.get(idNode);
			nodesStep4.add(new org.logic.tree.manager.context.diagram.goals.NodeStep4(nodeStep3.getText(), false, null, "150 50", "high_hard", colors.get(key++)));
		}
		
		org.logic.tree.manager.context.diagram.goals.DiagramStep4 diagramStep4 = new org.logic.tree.manager.context.diagram.goals.DiagramStep4();
		
		diagramStep4.set_class("go.GraphLinksModel");
		diagramStep4.setModelData(diagram3.getModelData());
		diagramStep4.setNodeDataArray(nodesStep4);

		LOG.debug("Diagram created");
		
		return mapper.writeValueAsString(diagramStep4);
	}

	@Override
	public String generateProsConsDiagram(String previousDiagram) {
		// TODO Auto-generated method stub
		return null;
	}

}
