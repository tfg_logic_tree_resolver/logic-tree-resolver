package org.logic.tree.manager.context.diagram.rootcause;

import java.io.Serializable;

public class InfoCase implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5620051437251911473L;

	private String caseText;
	private String number;
	
	
	
	public InfoCase() {
		super();
	}

	public InfoCase(String caseText, String number) {
		super();
		this.caseText = caseText;
		this.number = number;
	}

	public String getCaseText() {
		return caseText;
	}

	public void setCaseText(String caseText) {
		this.caseText = caseText;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

}
