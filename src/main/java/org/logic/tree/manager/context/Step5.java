package org.logic.tree.manager.context;

import static org.logic.tree.util.Constants.STATUS_STEP_5;
import static org.logic.tree.util.MessageUtil.getMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.logic.tree.manager.context.diagram.rootcause.DiagramStep4;
import org.logic.tree.manager.context.diagram.rootcause.DiagramStep5;
import org.logic.tree.manager.context.diagram.rootcause.InfoCase;
import org.logic.tree.manager.context.diagram.rootcause.ModelData;
import org.logic.tree.manager.context.diagram.rootcause.NodeStep5;
import org.logic.tree.manager.context.diagram.rootcause.Slice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public class Step5 extends State {
	
	private Logger LOG = LoggerFactory.getLogger(Step5.class);

	public Step5() {
		super();
	    setIdStep(STATUS_STEP_5);
	}

	
	@Override
	public void goNext(ProblemContext context) throws JsonParseException, JsonMappingException, IOException {
		context.setState(new Step6());
	}
	
	@Override
	public void goPrevious(ProblemContext context) throws JsonParseException, JsonMappingException, IOException {
		context.setState(new Step4());	
	}

	@Override
	public String generateRootCauseDiagram(String previousDiagram)  throws JsonParseException, JsonMappingException, IOException{
		if(previousDiagram == null){
			return null;
		}
		
		DiagramStep4 diagram4 = (DiagramStep4) mapper.readValue(previousDiagram, DiagramStep4.class);
		
		List<Slice> listSlices = diagram4.getNodeDataArray().get(0).getSlices();
		
		List<InfoCase> realInfoCases = new ArrayList<InfoCase>();
		
		if(listSlices == null){
			return null;
		}
		
		for(Slice slice: listSlices){
			realInfoCases.add(new InfoCase(slice.getText(), String.valueOf(slice.getCount())));
		}
		
		ModelData modelData = new ModelData();
		
		modelData.setListInfoCase(realInfoCases);

		NodeStep5 nodeStep5 = new NodeStep5();
		
		nodeStep5.setKey(0);
		nodeStep5.setText(getMessage("problem.root_cause.step_5.methods"));
		nodeStep5.setBrush("coral");
		nodeStep5.setLoc("0 0");
		
		
		DiagramStep5 diagram5 = new DiagramStep5();
		
		diagram5.set_class("go.TreeModel");
		diagram5.setModelData(modelData);
		diagram5.setNodeDataArray(Collections.singletonList(nodeStep5));
		
		LOG.debug("Diagram created");
		
		return mapper.writeValueAsString(diagram5);
		
	}

	@Override
	public String generateGoalsDiagram(String previousDiagram) throws JsonParseException, JsonMappingException, IOException {
		
		if(previousDiagram == null){
			return null;
		}
		
		org.logic.tree.manager.context.diagram.goals.DiagramStep4 diagram4 = mapper.readValue(previousDiagram, org.logic.tree.manager.context.diagram.goals.DiagramStep4.class);
		
		
		List<org.logic.tree.manager.context.diagram.goals.NodeStep4> nodes = diagram4.getNodeDataArray();


		
		List<org.logic.tree.manager.context.diagram.goals.NodeStep5> nodesStep5 = new ArrayList<org.logic.tree.manager.context.diagram.goals.NodeStep5>();
		
		int key = 0;
		
		//Create headers and siders
		
		nodesStep5.add(new org.logic.tree.manager.context.diagram.goals.NodeStep5(key,getMessage("problem.goals.step_5.analisis") , "Header", null, null, null));
		nodesStep5.add(new org.logic.tree.manager.context.diagram.goals.NodeStep5(++key,getMessage("problem.goals.step_5.ideas"), "Sider",null,null,null));
		nodesStep5.add(new org.logic.tree.manager.context.diagram.goals.NodeStep5(++key,getMessage("problem.goals.step_5.hipothesis"),"Column Header",2,null,null));
		nodesStep5.add(new org.logic.tree.manager.context.diagram.goals.NodeStep5(++key,getMessage("problem.goals.step_5.reasoning"),"Column Header",3,null,null));
		nodesStep5.add(new org.logic.tree.manager.context.diagram.goals.NodeStep5(++key,getMessage("problem.goals.step_5.plan"),"Column Header",4,null,null));
		nodesStep5.add(new org.logic.tree.manager.context.diagram.goals.NodeStep5(++key,getMessage("problem.goals.step_5.info_source"),"Column Header",5,null,null));
		
		int keyMethod = 1;
		int row = 2;
		
		List<String> ideas = new ArrayList<String>();
		for(org.logic.tree.manager.context.diagram.goals.NodeStep4 node: nodes){
					
			
			if(!getMessage("problem.goals.step_4.low_hard").equals(node.getGroup()) && node.getIsGroup()==false){
				String textMethod = keyMethod+"."+node.getKey();
				
				ideas.add(textMethod);
				
				nodesStep5.add(new org.logic.tree.manager.context.diagram.goals.NodeStep5(++key, textMethod , "Row Sider", null, row, null));
				
				nodesStep5.add(new org.logic.tree.manager.context.diagram.goals.NodeStep5(++key,null,null,2,row,true));
				nodesStep5.add(new org.logic.tree.manager.context.diagram.goals.NodeStep5(++key,null,null,3,row,true));
				nodesStep5.add(new org.logic.tree.manager.context.diagram.goals.NodeStep5(++key,null,null,4,row,true));
				nodesStep5.add(new org.logic.tree.manager.context.diagram.goals.NodeStep5(++key,null,null,5,row,true));
				
				keyMethod++;
				row++;
				
			}
			

		}
		
					
		
		org.logic.tree.manager.context.diagram.goals.ModelData modelData = diagram4.getModelData();
		
		modelData.setIdeas(ideas);
		
		org.logic.tree.manager.context.diagram.goals.DiagramStep5 diagram5 = new org.logic.tree.manager.context.diagram.goals.DiagramStep5();
		
		
		diagram5.set_class("go.GraphLinksModel");
		diagram5.setModelData(modelData);
		diagram5.setNodeDataArray(nodesStep5);
		
		
		LOG.debug("Diagram created");
		
		return mapper.writeValueAsString(diagram5);
	}

	@Override
	public String generateProsConsDiagram(String previousDiagram) {
		// TODO Auto-generated method stub
		return null;
	}

}
