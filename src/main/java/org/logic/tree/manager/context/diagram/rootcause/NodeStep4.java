package org.logic.tree.manager.context.diagram.rootcause;

import java.io.Serializable;
import java.util.List;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */
public class NodeStep4 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3404342019152414621L;

	private int key;

	private String namePie;

	private String nameGuessedPie;

	List<Slice> slices;

	List<Slice> guessedslices;

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public String getNamePie() {
		return namePie;
	}

	public void setNamePie(String namePie) {
		this.namePie = namePie;
	}

	public String getNameGuessedPie() {
		return nameGuessedPie;
	}

	public void setNameGuessedPie(String nameGuessedPie) {
		this.nameGuessedPie = nameGuessedPie;
	}

	public List<Slice> getSlices() {
		return slices;
	}

	public void setSlices(List<Slice> slices) {
		this.slices = slices;
	}

	public List<Slice> getGuessedslices() {
		return guessedslices;
	}

	public void setGuessedslices(List<Slice> guessedslices) {
		this.guessedslices = guessedslices;
	}

}
