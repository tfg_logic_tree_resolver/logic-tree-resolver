package org.logic.tree.manager.context.diagram.rootcause;

import java.io.Serializable;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public class NodeStep1 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 793455965243534194L;

	private int key;
	private String name;
	private Integer parent;

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getParent() {
		return parent;
	}

	public void setParent(Integer parent) {
		this.parent = parent;
	}

}
