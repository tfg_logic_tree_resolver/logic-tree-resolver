package org.logic.tree.manager.context.diagram.goals;

import java.io.Serializable;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */
public class NodeStep3 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2821359053750650403L;

	private int key;

	private String text;

	private String brush;

	private String loc;

	private Integer parent;

	public NodeStep3() {
		super();
	}

	public NodeStep3(int key, String text, String brush, String loc, Integer parent) {
		super();
		this.key = key;
		this.text = text;
		this.brush = brush;
		this.loc = loc;
		this.parent = parent;
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getBrush() {
		return brush;
	}

	public void setBrush(String brush) {
		this.brush = brush;
	}

	public String getLoc() {
		return loc;
	}

	public void setLoc(String loc) {
		this.loc = loc;
	}

	public Integer getParent() {
		return parent;
	}

	public void setParent(Integer parent) {
		this.parent = parent;
	}

}
