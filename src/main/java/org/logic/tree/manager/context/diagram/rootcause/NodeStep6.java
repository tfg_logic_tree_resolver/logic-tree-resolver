package org.logic.tree.manager.context.diagram.rootcause;

import java.io.Serializable;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */
public class NodeStep6 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6733077570058155770L;
	private int key;
	private String text;
	private String category;
	private Integer col;
	private Integer row;
	private Boolean isGroup;
	private String group;

	public NodeStep6(int key, String text, String category, Integer col, Integer row, Boolean isGroup) {
		super();
		this.key = key;
		this.text = text;
		this.category = category;
		this.col = col;
		this.row = row;
		this.isGroup = isGroup;
	}

	public NodeStep6() {
		super();
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Integer getCol() {
		return col;
	}

	public void setCol(Integer col) {
		this.col = col;
	}

	public Integer getRow() {
		return row;
	}

	public void setRow(Integer row) {
		this.row = row;
	}

	public Boolean getIsGroup() {
		return isGroup;
	}

	public void setIsGroup(Boolean isGroup) {
		this.isGroup = isGroup;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	
}
