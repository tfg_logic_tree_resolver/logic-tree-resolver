package org.logic.tree.manager.context;

import static org.logic.tree.util.Constants.STATUS_STEP_7;
import static org.logic.tree.util.MessageUtil.getMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.logic.tree.manager.context.diagram.rootcause.DiagramStep4;
import org.logic.tree.manager.context.diagram.rootcause.DiagramStep6;
import org.logic.tree.manager.context.diagram.rootcause.InfoCase;
import org.logic.tree.manager.context.diagram.rootcause.NodeStep4;
import org.logic.tree.manager.context.diagram.rootcause.Slice;
import org.logic.tree.util.Color;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public class Step7 extends State {
	
	private Logger LOG = LoggerFactory.getLogger(Step7.class);
	
	public Step7() {
		super();
	    setIdStep(STATUS_STEP_7);
	}


	@Override
	public void goNext(ProblemContext context) throws JsonParseException, JsonMappingException, IOException {
		// last state
		context.setState(this);
	}

	@Override
	public void goPrevious(ProblemContext context) throws JsonParseException, JsonMappingException, IOException {
		context.setState(new Step6());	
	}
	
	@Override
	public String generateRootCauseDiagram(String previousDiagram)  throws JsonParseException, JsonMappingException, IOException{
		DiagramStep6 diagram6 = (DiagramStep6) mapper.readValue(previousDiagram, DiagramStep6.class);
		
		List<InfoCase> listInfoCase = diagram6.getModelData().getListInfoCase();
		List<Slice> slices = new ArrayList<Slice>();
		
		
		List<String> colorSlices = Color.randomColor(listInfoCase.size());
		
		int keyColorSlices = 0;
		
		for(InfoCase infoCase: listInfoCase){
			
			int number = StringUtils.isNumeric(infoCase.getNumber())?Integer.valueOf(infoCase.getNumber()):0;
			Slice slice = new Slice(infoCase.getCaseText(), number, colorSlices.get(keyColorSlices));
			keyColorSlices++;
			
			slices.add(slice);
		
		}
		
		NodeStep4 nodeDataArray = new NodeStep4();
		
		nodeDataArray.setKey(0);
		nodeDataArray.setNamePie(getMessage("problem.root_cause.step_7.after"));
		nodeDataArray.setNameGuessedPie(getMessage("problem.root_cause.step_7.before"));
		nodeDataArray.setSlices(slices);
		nodeDataArray.setGuessedslices(slices);
		
		DiagramStep4 diagram4 = new DiagramStep4();
		
		diagram4.set_class("go.Model");
		diagram4.setCopiesArrayObjects(true);
		diagram4.setCopiesArrays(true);
		diagram4.setNodeDataArray(Collections.singletonList(nodeDataArray));
		
		LOG.debug("Diagram created");
		
		return mapper.writeValueAsString(diagram4);
	}

	@Override
	public String generateGoalsDiagram(String previousDiagram) {
		
		return null;
	}

	@Override
	public String generateProsConsDiagram(String previousDiagram) {
		// TODO Auto-generated method stub
		return null;
	}

}
