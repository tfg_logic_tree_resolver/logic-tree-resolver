package org.logic.tree.manager.context.diagram.rootcause;

import java.io.Serializable;

public class Slice implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3668792304129379986L;

	private String text;

	private int count;

	private String color;

		
	public Slice() {
		super();
	}

	public Slice(String text, int count, String color) {
		super();
		this.text = text;
		this.count = count;
		this.color = color;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
