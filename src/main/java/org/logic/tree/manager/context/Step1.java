package org.logic.tree.manager.context;

import static org.logic.tree.util.Constants.STATUS_STEP_1;
import static org.logic.tree.util.MessageUtil.getMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.logic.tree.manager.context.diagram.goals.DiagramStep1;
import org.logic.tree.manager.context.diagram.goals.NodeStep1;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public class Step1 extends State {

	private Logger LOG = LoggerFactory.getLogger(Step1.class);
		
	public Step1() {
		super();
	    setIdStep(STATUS_STEP_1);
	}

	@Override
	public void goNext(ProblemContext context) throws JsonParseException, JsonMappingException, IOException {
		context.setState(new Step2());

	}
	
	@Override
	public void goPrevious(ProblemContext context) throws JsonParseException, JsonMappingException, IOException {
		// There is no previous state
		context.setState(this);
	}

	@Override
	public String generateRootCauseDiagram(String previousDiagram)  throws JsonParseException, JsonMappingException, IOException{
		//As first step there is no modifications
		return previousDiagram;
	}

	@Override
	public String generateGoalsDiagram(String previousDiagram) throws JsonProcessingException {
		
		List<NodeStep1> listNodes = new ArrayList<NodeStep1>();
		
		int key = 1;
		
		int nodeParent = key;
		
		listNodes.add(createNode(getMessage("problem.goals.step_1.uds"), key));
		listNodes.add(createSubnode(getMessage("problem.goals.step_1.question1"), ++key, nodeParent));
		listNodes.add(createSubnode(getMessage("problem.goals.step_1.question2"),++key,nodeParent));
		listNodes.add(createSubnode(getMessage("problem.goals.step_1.question3"), ++key, nodeParent));

		
		DiagramStep1 diagramStep1 = new DiagramStep1();
			
		
		diagramStep1.set_class("go.GraphLinksModel");
		diagramStep1.setNodeDataArray(listNodes);
		
		LOG.debug("Diagram created");
		
		return mapper.writeValueAsString(diagramStep1);
		

	}

	@Override
	public String generateProsConsDiagram(String previousDiagram) throws JsonProcessingException {
		List<org.logic.tree.manager.context.diagram.proscons.NodeStep1> listNodes = new ArrayList<org.logic.tree.manager.context.diagram.proscons.NodeStep1>();
		
		int key = 1;
		
		
		
		listNodes.add(createNodeProsCons(getMessage("problem.pros_cons.step_1.option")+ "1", key));
		listNodes.add(createNodeProsCons(getMessage("problem.pros_cons.step_1.option")+ "2", ++key));
		listNodes.add(createSubnodeProsCons(getMessage("problem.pros_cons.step_1.pros"), ++key, 1));
		listNodes.add(createSubnodeProsCons(getMessage("problem.pros_cons.step_1.cons"),++key,1));
		listNodes.add(createSubnodeProsCons(getMessage("problem.pros_cons.step_1.pros"), ++key, 2));
		listNodes.add(createSubnodeProsCons(getMessage("problem.pros_cons.step_1.cons"),++key,2));
		
		
		org.logic.tree.manager.context.diagram.proscons.DiagramStep1 diagramStep1 = new org.logic.tree.manager.context.diagram.proscons.DiagramStep1();
			
		
		diagramStep1.set_class("go.GraphLinksModel");
		diagramStep1.setNodeDataArray(listNodes);
		
		LOG.debug("Diagram created");
		
		return mapper.writeValueAsString(diagramStep1);
	}

	private NodeStep1 createNode(String text, int key){
		NodeStep1 node = new NodeStep1();
		
		node.setKey(key);
		node.setText(text);
		node.setIsGroup(true);
		node.setCategory("OfGroups");
		
		return node;
	}
	
	private NodeStep1 createSubnode(String text, int key, int nodeParent){
		NodeStep1 node = new NodeStep1();
		
		node.setKey(key);
		node.setText(text);
		node.setIsGroup(true);
		node.setCategory("OfNodes");
		node.setGroup(nodeParent);
		
		return node;
	}

	private org.logic.tree.manager.context.diagram.proscons.NodeStep1 createNodeProsCons(String text, int key){
		org.logic.tree.manager.context.diagram.proscons.NodeStep1 node = new org.logic.tree.manager.context.diagram.proscons.NodeStep1();
		
		node.setKey(key);
		node.setText(text);
		node.setIsGroup(true);
		node.setCategory("OfGroups");
		
		return node;
	}
	
	private org.logic.tree.manager.context.diagram.proscons.NodeStep1 createSubnodeProsCons(String text, int key, int nodeParent){
		org.logic.tree.manager.context.diagram.proscons.NodeStep1 node = new org.logic.tree.manager.context.diagram.proscons.NodeStep1();
		
		node.setKey(key);
		node.setText(text);
		node.setIsGroup(true);
		node.setCategory("OfNodes");
		node.setGroup(nodeParent);
		
		return node;
	}

}
