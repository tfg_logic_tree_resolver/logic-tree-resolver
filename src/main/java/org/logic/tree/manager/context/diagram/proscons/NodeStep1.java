package org.logic.tree.manager.context.diagram.proscons;

import java.io.Serializable;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */
public class NodeStep1 implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1052109769930084392L;
	private int key;
	private String text;
	private boolean isGroup;
	private String category;
	private Integer group;

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean getIsGroup() {
		return isGroup;
	}

	public void setIsGroup(boolean isGroup) {
		this.isGroup = isGroup;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Integer getGroup() {
		return group;
	}

	public void setGroup(Integer group) {
		this.group = group;
	}

}
