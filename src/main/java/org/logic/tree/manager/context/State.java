package org.logic.tree.manager.context;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a> 
 *
 */
public abstract class State {

	
	protected Integer idStep;
	
	protected ObjectMapper mapper = new ObjectMapper();
	
	
	public State() {
		super();
		
		this.mapper.setSerializationInclusion(Include.NON_NULL);
		this.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	/**
	 * Sets the next state in the context.
	 * @param context
	 */
	public abstract void goNext(ProblemContext context) throws JsonParseException, JsonMappingException, IOException;

	
	/**
	 * Generates the first diagram for the next step in problem which has the type of RootCause
	 * @param previousDiagram in JSON format
	 * @return the next diagram in JSON format
	 */

	public abstract String generateRootCauseDiagram(String previousDiagram) throws JsonParseException, JsonMappingException, IOException;

	/**
	 * Generates the first diagram for the next step in problem which has the type of Goals
	 * @param previousDiagram in JSON format
	 * @return the next diagram in JSON format
	 */
	public abstract String generateGoalsDiagram(String previousDiagram) throws JsonParseException, JsonMappingException, IOException;

	
	/**
	 * Generates the first diagram for the next step in problem which has the type of Pros&Cons
	 * @param previousDiagram in JSON format
	 * @return the next diagram in JSON format
	 */
	public abstract String generateProsConsDiagram(String previousDiagram) throws JsonParseException, JsonMappingException, IOException;

	
	/**
	 * Sets the previous state in the context.
	 * @param context
	 */
	public abstract void goPrevious(ProblemContext context) throws JsonParseException, JsonMappingException, IOException;


	public Integer getIdStep() {
		return idStep;
	}

	public void setIdStep(Integer idStep) {
		this.idStep = idStep;
	}
	
	
	
}


