package org.logic.tree.manager.context.diagram.rootcause;

import java.io.Serializable;
import java.util.List;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */
public class ModelData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1552987911786538386L;
	
	private List<InfoCase> listInfoCase;
	
	private String complementaryModel;

	public List<InfoCase> getListInfoCase() {
		return listInfoCase;
	}

	public void setListInfoCase(List<InfoCase> listInfoCase) {
		this.listInfoCase = listInfoCase;
	}

	public String getComplementaryModel() {
		return complementaryModel;
	}

	public void setComplementaryModel(String complementaryModel) {
		this.complementaryModel = complementaryModel;
	}

	
}
