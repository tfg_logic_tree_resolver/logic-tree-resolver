package org.logic.tree.manager.context.diagram.rootcause;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public class DiagramStep1 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4819124534563060595L;
	
	private String _class;
	
	private List<NodeStep1> nodeDataArray;
	
	
	@JsonProperty("class")
	public String get_class() {
		return _class;
	}

	@JsonProperty("class")
	public void set_class(String _class) {
		this._class = _class;
	}

	public List<NodeStep1> getNodeDataArray() {
		return nodeDataArray;
	}

	public void setNodeDataArray(List<NodeStep1> nodeDataArray) {
		this.nodeDataArray = nodeDataArray;
	}
	

}
