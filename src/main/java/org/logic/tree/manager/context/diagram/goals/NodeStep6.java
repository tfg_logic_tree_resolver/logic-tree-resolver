package org.logic.tree.manager.context.diagram.goals;

import java.io.Serializable;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */
public class NodeStep6 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8737965684107536514L;

	
	private String key;
	private boolean isGroup;
	private String pos;
	private String size;
	private String group;
	private String color;
	
	

	public NodeStep6(String key, boolean isGroup, String pos, String size, String group, String color) {
		super();
		this.key = key;
		this.isGroup = isGroup;
		this.pos = pos;
		this.size = size;
		this.group = group;
		this.color = color;
	}

	public NodeStep6() {
		super();
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public boolean getIsGroup() {
		return isGroup;
	}

	public void setIsGroup(boolean isGroup) {
		this.isGroup = isGroup;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}


}
