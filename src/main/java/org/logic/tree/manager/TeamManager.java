package org.logic.tree.manager;

import java.util.List;

import org.logic.tree.dao.criteria.TeamCriteria;
import org.logic.tree.dao.vo.Team;
import org.logic.tree.manager.custom.CustomMemberTeam;
import org.logic.tree.manager.custom.CustomTeam;


/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

public interface TeamManager {
	
	/**
	 * Searches all the Teams of an organization
	 * 
	 * @param idOrganization
	 *            the organization id
	 * @return the list of teams
	 */
	public List<Team> getByOrganizationId(Long idOrganization);
	
	/**
	 * Searches the teams by criteria
	 * @param criteria idOrganization, idUser
	 * @return the list of teams
	 */
	public List<Team> findByCriteria(TeamCriteria criteria);
	
	/**
	 * Creates a new Team
	 * @param custom
	 * @param user
	 * @return
	 */
	public CustomTeam saveTeam(CustomTeam custom, String user);

	
	/**
	 * Retrieves all team-members associations in a organization
	 * 
	 * @param idOrganization
	 * @return
	 */
	public List<CustomTeam> getTeamMemberAssociationsByOrgId(Long idOrganization);
	
	/**
	 * Joins members into a team
	 * @param joinedMembers
	 * @param user
	 */
	public void listSaveTeamMembers(List<CustomMemberTeam> joinedMembers, String user);
	
	/**
	 * Deletes logically members of a team
	 * @param leavingMembers
	 * @param user
	 */
	public void listDeleteTeamMembers(List<CustomMemberTeam> leavingMembers, String user);
	
	/**
	 * Searches team-members relationships by criteria.
	 * @param criteria
	 * @return
	 */
	public List<CustomMemberTeam> findMemberTeamByCriteria(TeamCriteria criteria);
	
	/**
	 * Retrieves a team by its identifier
	 * @param idTeam identifier
	 * @return the team
	 */
	public CustomTeam getTeamById(Long idTeam);

}
