package org.logic.tree.manager;


import java.util.List;

import org.logic.tree.dao.criteria.MemberCriteria;
import org.logic.tree.dao.vo.Member;
import org.logic.tree.manager.custom.CustomMember;
import org.logic.tree.manager.exception.ModelException;
import org.logic.tree.manager.exception.UserException;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a> 
 *
 */

public interface MemberManager {
	
	/**
	 * Creates a new user in the database
	 * @param newUser
	 * @return user's identifier
	 * @throws ModelException
	 */

	public Member saveMemberSignUp(CustomMember newUser, String userName) throws ModelException;
	
	/**
	 * Logs in a existent user
	 * @param user
	 * @return user's identifier
	 * @throws ModelException
	 */

	public Member signIn(CustomMember user) throws ModelException;
	

	/**
	 * Search a Member by criteria
	 * @param criteria
	 * @return
	 */
	public Member findOneByCriteria(MemberCriteria criteria);
	
	/**
	 * Search all member's teams by member id
	 * @param idMember
	 * @return The list with team identifiers
	 * @throws UserException if user doesn't exist
	 */
	public List<Long> findIdTeamsByMemberId(Long idMember) throws UserException;
	
	/**
	 * Search a Member by criteria
	 * @param criteria
	 * @return list of custom members
	 */
	public List<CustomMember> findByCriteria(MemberCriteria criteria);
	
	
	/**
	 * Update a list of members
	 * @param members
	 * @param userName
	 */
	public void listUpdateMember(List<CustomMember> members, String userName);
	
	/**
	 * Find a member searching by their id
	 * @param idMember must not be null
	 * @return the member
	 */
	public Member findById(Long idMember);
	
	/***
	 * Updates the language preferences of the member of the organization
	 * @param idOrganization the organization identifier
	 * @param idApplicationLanguage the language identifier
	 * @param userName
	 * @throws UserException
	 */
	public void updateIdApplicationLanguageMembers(Long idOrganization,Integer idApplicationLanguage, String userName) throws UserException;

	
}
