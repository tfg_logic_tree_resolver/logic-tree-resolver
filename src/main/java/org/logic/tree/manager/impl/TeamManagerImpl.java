package org.logic.tree.manager.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.logic.tree.dao.MemberDAO;
import org.logic.tree.dao.TeamDAO;
import org.logic.tree.dao.TeamMemberDAO;
import org.logic.tree.dao.criteria.TeamCriteria;
import org.logic.tree.dao.criteria.specification.TeamMemberSpecification;
import org.logic.tree.dao.criteria.specification.TeamSpecification;
import org.logic.tree.dao.vo.Team;
import org.logic.tree.dao.vo.TeamMember;
import org.logic.tree.manager.TeamManager;
import org.logic.tree.manager.custom.CustomMemberTeam;
import org.logic.tree.manager.custom.CustomTeam;
import org.logic.tree.util.impl.GenericManagerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

@Component
@Transactional(readOnly = true)
public class TeamManagerImpl extends GenericManagerImpl<Team> implements TeamManager{

	@Autowired
	TeamDAO teamDao;
	
	@Autowired
	TeamMemberDAO teamMemberDao;
	
	@Autowired
	MemberDAO memberDAO;
	

	@Override
	public List<Team> getByOrganizationId(Long idOrganization) {
		
		return teamDao.findByIdOrganization(idOrganization);
	
	}

	@Override
	public List<Team> findByCriteria(TeamCriteria criteria) {
		
		TeamSpecification teamSpec = new TeamSpecification(criteria);
		
		return teamDao.findAll(teamSpec);

	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public CustomTeam saveTeam(CustomTeam custom, String user) {
		Team result = teamDao.saveAndFlush(mapper.map(custom, Team.class));
		
		return mapper.map(result, CustomTeam.class);
	}

	@Override
	public List<CustomTeam> getTeamMemberAssociationsByOrgId(Long idOrganization) {
		
		TeamMemberSpecification teamSpec = new TeamMemberSpecification(new TeamCriteria(idOrganization));
		
		List<TeamMember> listTeamMember = teamMemberDao.findAll(teamSpec);
		
		Map<Long,CustomTeam> mapCustomTeams = new HashMap<Long,CustomTeam>();
		
		for(TeamMember teamMember:listTeamMember){
			Long key = teamMember.getId().getIdTeam();
			
			//TODO: At this point getMember shouldn't return null...
			String memberMail = (teamMember.getMember() == null)?memberDAO.getOne(teamMember.getId().getIdMember()).getMail():teamMember.getMember().getMail();

			
			if(mapCustomTeams.containsKey(key)){
				mapCustomTeams.get(key).getMailMembers().add(memberMail);
				
			}else{
				
				CustomTeam custom = mapper.map(teamDao.getOne(teamMember.getId().getIdTeam()), CustomTeam.class);
				custom.setMailMembers(new ArrayList<String>(Arrays.asList(memberMail)));
				mapCustomTeams.put(key, custom);
			}
		}
		
		//Until now, we only have the teams with associations, but we need teams without members as well.
		List<Team> teams = getByOrganizationId(idOrganization);
		
		for(Team team: teams){
			
			if(!mapCustomTeams.containsKey(team.getId())){
				mapCustomTeams.put(team.getId(), mapper.map(team, CustomTeam.class));
			}
		}
		
		return new ArrayList<CustomTeam> (mapCustomTeams.values());
	}



	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	private void updateTeamMembers(List<CustomMemberTeam> customMembersTeam){
		
		List<TeamMember> listTeamMember = new ArrayList<TeamMember>();
		
		for(CustomMemberTeam custom : customMembersTeam){
			
			TeamMember teamMember = mapper.map(custom, TeamMember.class);	
			
			listTeamMember.add(teamMember);
		}
		
		
		teamMemberDao.save(listTeamMember);
		teamMemberDao.flush();
		
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void listSaveTeamMembers(List<CustomMemberTeam> joinedMembers, String user) {
		//At this point audit creation parameters are covered by Audit aspect
		updateTeamMembers(joinedMembers);
		
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void listDeleteTeamMembers(List<CustomMemberTeam> leavingMembers, String user) {
		
		//At this point audit delete parameters are covered by Audit aspect
		updateTeamMembers(leavingMembers);
		
	}

	@Override
	public List<CustomMemberTeam> findMemberTeamByCriteria(TeamCriteria criteria) {
		TeamMemberSpecification teamSpec = new TeamMemberSpecification(criteria);
		
		List<TeamMember> listTeamMember = teamMemberDao.findAll(teamSpec);
		
		return mapper.mapList(listTeamMember,CustomMemberTeam.class);
	}

	@Override
	public CustomTeam getTeamById(Long idTeam) {

		return mapper.map(teamDao.getOne(idTeam), CustomTeam.class);
	}



}
