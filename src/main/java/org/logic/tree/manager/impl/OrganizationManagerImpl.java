package org.logic.tree.manager.impl;


import static org.logic.tree.util.Constants.MEMBER_TYPE_REGULAR;

import java.util.ArrayList;
import java.util.List;

import org.logic.tree.dao.OrganizationDAO;
import org.logic.tree.dao.criteria.MemberCriteria;
import org.logic.tree.dao.criteria.ProblemCriteria;
import org.logic.tree.dao.criteria.TeamCriteria;
import org.logic.tree.dao.vo.Organization;
import org.logic.tree.manager.MemberManager;
import org.logic.tree.manager.OrganizationManager;
import org.logic.tree.manager.ProblemManager;
import org.logic.tree.manager.TeamManager;
import org.logic.tree.manager.custom.CustomMember;
import org.logic.tree.manager.custom.CustomMemberTeam;
import org.logic.tree.manager.custom.CustomOrganization;
import org.logic.tree.manager.custom.CustomProblem;
import org.logic.tree.manager.custom.CustomTeam;
import org.logic.tree.manager.exception.UserException;
import org.logic.tree.util.impl.GenericManagerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

@Component
@Transactional(readOnly = true)
public class OrganizationManagerImpl extends GenericManagerImpl<Organization> implements OrganizationManager {

	@Autowired
	OrganizationDAO organizationDAO;
	
	@Autowired
	TeamManager teamManager;
	
	@Autowired
	MemberManager memberManager; 
	
	@Autowired
	ProblemManager problemManager;

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public CustomOrganization getDetail(Long idOrganization) {
		
		//Find organization
		Organization org = organizationDAO.findOne(idOrganization);
		
		CustomOrganization customOrg = mapper.map(org, CustomOrganization.class);
		
		//Find employees
		MemberCriteria criteria  = new MemberCriteria();
		
		criteria.setIdOrganization(idOrganization);
		criteria.setIdType(MEMBER_TYPE_REGULAR);
			
		List<CustomMember> employees = memberManager.findByCriteria(criteria);
		
		
		customOrg.setEmployees(employees);
		
		//Find teams and its associations
		List<CustomTeam> customTeams = teamManager.getTeamMemberAssociationsByOrgId(idOrganization);
		
		customOrg.setTeams(customTeams);
		
		//Find problems
		ProblemCriteria problemCriteria = new ProblemCriteria();
		problemCriteria.setIdOrganization(idOrganization);
		
		List<CustomProblem> customProblems = problemManager.findByCriteria(problemCriteria);
	
		customOrg.setProblems(customProblems);
		
		return customOrg;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void updateGeneralDetail(CustomOrganization custom, String userName) throws UserException {
		
		Organization orgEntity = mapper.map(custom, Organization.class);
					
		
		organizationDAO.saveAndFlush(orgEntity);
				
		memberManager.updateIdApplicationLanguageMembers(orgEntity.getIdOrganization(), orgEntity.getIdApplicationLanguage(), userName);
						

	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void createTeam(Long idOrganization,String teamName, String userName) {
		CustomTeam customTeam = new CustomTeam();
		
		customTeam.setIdOrganization(idOrganization);
		customTeam.setNameTeam(teamName);
		
		teamManager.saveTeam(customTeam, userName);

		
	}



	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void associateEmployees(List<Long> idMembers, Long idOrganization, String userName) {
		// Employees that already belong to the organization are discarded.
		CustomOrganization customOrganization = mapper.map(organizationDAO.findOne(idOrganization),CustomOrganization.class);
		
		MemberCriteria criteria = new MemberCriteria();
		
		criteria.setListIdMembers(idMembers);
		
		List<CustomMember> employees = memberManager.findByCriteria(criteria);
		
				
		List<CustomMember> newEmployees = new ArrayList<CustomMember>();
		
		for(CustomMember employee: employees){
			if (employee.getOrganization() == null || !employee.getOrganization().getIdOrganization().equals(idOrganization)){
				
				
				employee.setOrganization(customOrganization);			
				newEmployees.add(employee);
			}
		}
		
		if(!CollectionUtils.isEmpty(newEmployees)){
			memberManager.listUpdateMember(newEmployees, userName);
		}
		
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void updateTeamWithEmployees(List<Long> idMembers, Long idTeam, Long idOrganization,String action,
			String userName) {
		
		switch (action) {
		case "join":
			List<CustomMemberTeam> listMemberTeam = new ArrayList<CustomMemberTeam>();
			
			for(Long idMember: idMembers){		
				CustomMemberTeam customMemberTeam = new CustomMemberTeam(idTeam,idMember);
				listMemberTeam.add(customMemberTeam);
			}
			
			teamManager.listSaveTeamMembers(listMemberTeam, userName);
			break;
		case "leave":
			
			TeamCriteria criteria = new TeamCriteria();
			
			criteria.setIdTeam(idTeam);
			criteria.setIdMembers(idMembers);
			
			List<CustomMemberTeam> listCustomTeam = mapper.mapList(teamManager.findMemberTeamByCriteria(criteria), CustomMemberTeam.class);
			if(!listCustomTeam.isEmpty()){
				teamManager.listDeleteTeamMembers(listCustomTeam, userName);
			}
			break;
		default:
			break;
		}
		

	}

	
}
