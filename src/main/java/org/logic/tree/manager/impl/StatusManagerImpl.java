package org.logic.tree.manager.impl;

import static org.logic.tree.util.Constants.STATUS_STEP_1;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.logic.tree.controller.websocket.DiagramMessage;
import org.logic.tree.dao.StatusProblemDAO;
import org.logic.tree.dao.vo.StatusProblem;
import org.logic.tree.dao.vo.pk.StatusProblemPK;
import org.logic.tree.manager.StatusManager;
import org.logic.tree.manager.custom.CustomStatus;
import org.logic.tree.manager.exception.UserException;
import org.logic.tree.util.impl.GenericManagerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */
@Component
@Transactional(readOnly = true)
public class StatusManagerImpl extends GenericManagerImpl<StatusProblem>  implements StatusManager {

	private Logger LOG = LoggerFactory.getLogger(StatusManagerImpl.class);
	
	@Autowired
	StatusProblemDAO statusProblemDao;
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public CustomStatus saveFirstStatus(CustomStatus newStatus, String userName) throws UserException {

		LOG.debug("First status for problem id: "+newStatus.getIdProblem());
		
		newStatus.setInitDate(new Date());
		newStatus.setIdStatus(STATUS_STEP_1);
		
		StatusProblem persistedStatus = statusProblemDao.save(mapper.map(newStatus, StatusProblem.class));
		
				
		return mapper.map(persistedStatus, CustomStatus.class);
	}

	@Override
	public List<CustomStatus> findStatusByProblem(long idProblem) {

		return mapper.mapList(statusProblemDao.findByIdIdProblem(idProblem), CustomStatus.class);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public CustomStatus updateStatus(CustomStatus statusUpdated, String userName) throws UserException {
		LOG.debug("Updating status"+statusUpdated.getIdStatus()+ "for problem id: "+statusUpdated.getIdProblem());
		
		StatusProblem updateStatusProblem = statusProblemDao.save(mapper.map(statusUpdated, StatusProblem.class));
		
		return mapper.map(updateStatusProblem, CustomStatus.class);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void updateStatusDiagram(DiagramMessage message) throws UserException {
		
		StatusProblemPK statusPK = new StatusProblemPK(Long.valueOf(message.getIdStatus()), Long.valueOf(message.getIdProblem()));
		
		CustomStatus statusToUpdate = mapper.map(statusProblemDao.getOne(statusPK),CustomStatus.class);
		
		statusToUpdate.setDiagram(message.getContent());
		
		updateStatus(statusToUpdate,message.getUserName());
		
	}

	@Override
	public Map<Integer, CustomStatus>  findStatusByProblemMap(long idProblem) {
		
		List<CustomStatus> listStatus = findStatusByProblem(idProblem);
		
		Map<Integer, CustomStatus> mapStatus = new HashMap<Integer,CustomStatus>();
		
		for(CustomStatus customStatus:listStatus)
		{
			mapStatus.put(customStatus.getIdStatus(), customStatus);
		}
		
		return mapStatus;
		
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public CustomStatus transitStatus(CustomStatus status, String userName, CustomStatus newStatus) throws UserException {
		
		LOG.debug("Finish status "+status.getIdStatus() +" for problem id: "+status.getIdProblem());
		
		Date now = new Date();
		
		status.setFinishDate(now);
		
		updateStatus(status, userName);
		
		LOG.debug("Next status "+newStatus.getIdStatus()+" for problem id: "+newStatus.getIdProblem());
		
		newStatus.setInitDate(now);
		newStatus.setCreator(userName);
		
		StatusProblem persistedStatus = statusProblemDao.save(mapper.map(newStatus, StatusProblem.class));
		
				
		return mapper.map(persistedStatus, CustomStatus.class);
		
	}

	@Override
	public CustomStatus findOneStatusByProblem(long idProblem, int idStatus) {

		StatusProblem status = statusProblemDao.getOne(new StatusProblemPK((long)idStatus, idProblem));
		
		return mapper.map(status,CustomStatus.class);
	}

}
