package org.logic.tree.manager.impl;

import static org.logic.tree.manager.exception.UserException.ERR_USER_DOES_NOT_BELONG_TO_ANY_ORGANIZATION;
import static org.logic.tree.manager.exception.UserException.ERR_USER_DOES_NOT_EXIST;
import static org.logic.tree.manager.exception.UserException.ERR_USER_DOES_NOT_HAVE_ANY_TEAM;
import static org.logic.tree.manager.exception.UserException.ERR_USER_DOES_NOT_HAVE_PERMISSION_IN_TEAM;

import java.util.List;

import org.logic.tree.dao.ProblemDAO;
import org.logic.tree.dao.TeamDAO;
import org.logic.tree.dao.criteria.MemberCriteria;
import org.logic.tree.dao.criteria.ProblemCriteria;
import org.logic.tree.dao.criteria.specification.ProblemSpecification;
import org.logic.tree.dao.vo.Member;
import org.logic.tree.dao.vo.Problem;
import org.logic.tree.dao.vo.Team;
import org.logic.tree.manager.MemberManager;
import org.logic.tree.manager.ProblemManager;
import org.logic.tree.manager.StatusManager;
import org.logic.tree.manager.custom.CustomProblem;
import org.logic.tree.manager.custom.CustomStatus;
import org.logic.tree.manager.exception.UserException;
import org.logic.tree.util.impl.GenericManagerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a> 
 *
 */
@Component
@Transactional(readOnly = true)
public class ProblemManagerImpl extends GenericManagerImpl<Problem> implements ProblemManager {

	private Logger LOG = LoggerFactory.getLogger(ProblemManagerImpl.class);
	
	
	@Autowired
	ProblemDAO problemDao;
	
	
	@Autowired
	TeamDAO teamDao;
	
	@Autowired
	MemberManager memberManager;
	
	@Autowired
	StatusManager statusManager;
	


	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Long saveIssue(CustomProblem custom, String userName) throws UserException{
		
		Long idTeam = custom.getTeam().getId();
			
		validateUser(userName, idTeam);
				
		
		Problem problem = mapper.map(custom, Problem.class);
		
		problem.setTeam(teamDao.getOne(idTeam));
		
		Long idProblem = problemDao.save(problem).getId();
		
		// Initialize status
		
		CustomStatus newStatus = new CustomStatus();
		newStatus.setIdProblem(idProblem);
		
		statusManager.saveFirstStatus(newStatus, userName);
		
		return idProblem;
		
	}

	@Override
	public List<CustomProblem> findAllIssuesByUserId(Long idUser) throws UserException{
		
		ProblemCriteria criteria = new ProblemCriteria();
			
		criteria.setListTeams(memberManager.findIdTeamsByMemberId(idUser));
			
		return findByCriteria(criteria);
				

	}



	private void validateUser(String userName, Long idTeam) throws UserException{
		
		MemberCriteria criteria = new MemberCriteria();
		criteria.setUserName(userName);
		
		Member creator = memberManager.findOneByCriteria(criteria);
		
		if(creator == null){
			String error = "The user with userName: "+ userName+" doesn't exist";
			LOG.error(error);
						
			throw new UserException(ERR_USER_DOES_NOT_EXIST, error);
		}
		
		if(creator.getOrganization() == null || creator.getOrganization().getIdOrganization() == null){
			String error = "The user with userName: "+userName+" doesn't belong to any organization.";
			LOG.error(error);
			
			throw new UserException(ERR_USER_DOES_NOT_BELONG_TO_ANY_ORGANIZATION,error);
		}
		
				
		if(CollectionUtils.isEmpty(creator.getTeams())){
			String error = "The user doesn't have any team";
			LOG.error(error);
						
			throw new UserException(ERR_USER_DOES_NOT_HAVE_ANY_TEAM, error);
		}
		
		boolean isValid = false;
		for(Team t: creator.getTeams()){
			if(t.getId().equals(idTeam)){
				isValid = true;
				break;
			}
		}
		
		if(!isValid){
			String error = "The user doesn't belong to the team with de id: "
					+ idTeam;
			LOG.error(error);
						
			throw new UserException(ERR_USER_DOES_NOT_HAVE_PERMISSION_IN_TEAM, error);
		}
		
	}



	@Override
	public List<CustomProblem> findByCriteria(ProblemCriteria criteria) {
		ProblemSpecification problemSpec = new ProblemSpecification(criteria);
				
		
		return mapper.mapList(problemDao.findAll(problemSpec,new Sort(Direction.DESC,"creationDate")),CustomProblem.class);
		
	}

	@Override
	public CustomProblem getIssueById(Long idIssue) {
		return mapper.map(problemDao.findOne(idIssue),CustomProblem.class);
	}

	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void updateProblem(CustomProblem custom, String userName) throws UserException {
		
		problemDao.save(mapper.map(custom, Problem.class));
	}
	
	
}
