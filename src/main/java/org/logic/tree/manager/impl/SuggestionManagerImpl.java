package org.logic.tree.manager.impl;

import java.util.List;

import org.logic.tree.dao.SuggestionDAO;
import org.logic.tree.dao.vo.Suggestion;
import org.logic.tree.manager.SuggestionManager;
import org.logic.tree.manager.custom.CustomSuggestion;
import org.logic.tree.manager.exception.UserException;
import org.logic.tree.util.impl.GenericManagerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

@Component
@Transactional(readOnly = true)
public class SuggestionManagerImpl extends GenericManagerImpl<Suggestion> implements SuggestionManager {

	@Autowired
	private SuggestionDAO suggestionDAO;
	
	@Override
	public List<CustomSuggestion> findAllSuggestionsByProblemAndStatus(Long idProblem, Long idStatus)
			throws UserException {
		
		return mapper.mapList(suggestionDAO.findByIdIdProblemAndIdIdStatusOrderByIdIdSuggestionDesc(idProblem, idStatus), CustomSuggestion.class);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public CustomSuggestion saveSuggestion(CustomSuggestion customSuggestion, String userName) throws UserException {
		
		List<Suggestion> persistedSuggestions = suggestionDAO.findByIdIdProblemAndIdIdStatusOrderByIdIdSuggestionDesc(customSuggestion.getIdProblem(), customSuggestion.getIdStatus());

		long lastIdSuggestion = Long.valueOf(0);
		
		if(!persistedSuggestions.isEmpty()){
			Suggestion lastSuggestion = persistedSuggestions.get(0);
			
			if(lastSuggestion!= null){
				lastIdSuggestion = lastSuggestion.getId().getIdSuggestion();
			}
			
		}
			
		customSuggestion.setIdSuggestion(++lastIdSuggestion);
		
		Suggestion newSuggestion = suggestionDAO.save(mapper.map(customSuggestion, Suggestion.class));
		
		return mapper.map(newSuggestion, CustomSuggestion.class);
	}

}
