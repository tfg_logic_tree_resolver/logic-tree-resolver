package org.logic.tree.manager.impl;

import static org.logic.tree.manager.exception.SignUpException.ERR_EMAIL_ALREADY_USED;
import static org.logic.tree.manager.exception.SignUpException.ERR_USER_NAME_ALREADY_USED;
import static org.logic.tree.manager.exception.UserException.ERR_USER_DOES_NOT_EXIST;
import static org.logic.tree.util.Constants.APPLICATION_LANGUAGE_ID_ENG;
import static org.logic.tree.util.Constants.MEMBER_TYPE_ORGANIZATION;
import static org.logic.tree.util.Constants.MEMBER_TYPE_REGULAR;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.logic.tree.dao.MemberDAO;
import org.logic.tree.dao.criteria.MemberCriteria;
import org.logic.tree.dao.criteria.specification.MemberSpecification;
import org.logic.tree.dao.vo.Member;
import org.logic.tree.dao.vo.Organization;
import org.logic.tree.dao.vo.Team;
import org.logic.tree.manager.MemberManager;
import org.logic.tree.manager.custom.CustomMember;
import org.logic.tree.manager.exception.ModelException;
import org.logic.tree.manager.exception.SignUpException;
import org.logic.tree.manager.exception.UserException;
import org.logic.tree.util.Color;
import org.logic.tree.util.EncryptUtil;
import org.logic.tree.util.impl.GenericManagerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */
@Component
@Transactional(readOnly = true)
public class MemberManagerImpl extends GenericManagerImpl<Member> implements MemberManager {

	private Logger LOG = LoggerFactory.getLogger(MemberManagerImpl.class);

	@Autowired
	MemberDAO memberDao;
	

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Member saveMemberSignUp(CustomMember newUser, String userName) throws ModelException {
	
		validateUser(newUser);
	

		LOG.debug("Signing Up: " + newUser.getCompleteName());

		// Transform plain password into a MD5 hash.
		try {
			newUser.setPasswordMember(EncryptUtil.hash(newUser.getPasswordMember()));
		} catch (NoSuchAlgorithmException e) {
			String errMessage = "There was an error transforming the user's password into hash...";
			LOG.error(errMessage);
			throw new ModelException(errMessage);
		}

				
		Member member = mapper.map(newUser, Member.class);
		member.setColor(Color.WHITE);
		member.setIdApplicationLanguage(APPLICATION_LANGUAGE_ID_ENG);
		
		
		int memberType = newUser.getIsOrganization()?MEMBER_TYPE_ORGANIZATION:MEMBER_TYPE_REGULAR;
		
		if(newUser.getIsOrganization()){
			
			Organization org = new Organization();
			
			org.setIdApplicationLanguage(APPLICATION_LANGUAGE_ID_ENG);
			org.setNameOrganization(member.getCompleteName());
			org.setCreator(newUser.getCreator());
			org.setCreationDate(newUser.getCreationDate());
						
			member.setOrganization(org);

		}
				
		member.setIdMemberType(memberType);

		validateObject(member);

		Member result = memberDao.save(member);

		LOG.debug("Id User created: " + result.getIdMember());

		return result;

	}




	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Member signIn(CustomMember user) throws ModelException {

		LOG.debug("Matching user: " + user.getMail());

		MemberCriteria criteria = new MemberCriteria();

		criteria.setMail(user.getMail());
		try {
			criteria.setPassword(EncryptUtil.hash(user.getPasswordMember()));
		} catch (NoSuchAlgorithmException e) {
			String errMessage = "There was an error transforming the user's password into hash...";
			LOG.error(errMessage);
			throw new ModelException(errMessage);
		}

		Member member = findOneByCriteria(criteria);
		
		if (member == null){
			String errorMessage = "There is no member with such criteria";
			LOG.error(errorMessage);
			throw new ModelException(errorMessage);
		}
		

		return member;			
		

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	private void validateUser(CustomMember newUser) throws ModelException, NoResultException {
		MemberCriteria criteria = new MemberCriteria();

		criteria.setMail(newUser.getMail());

		Member memberSameMail = null;
		
		try{
			memberSameMail = findOneByCriteria(criteria);
		}catch(NoResultException e){
			LOG.debug("Email is valid!");	
		}

		if (memberSameMail != null) {
			String errMessage = "This email is already used.";
			LOG.error(errMessage);
			SignUpException exception = new SignUpException(ERR_EMAIL_ALREADY_USED, errMessage);
			throw exception;
		}

		criteria = new MemberCriteria();
		criteria.setUserName(newUser.getUserName());

		Member memberSameUserName = null; 
		try{
			memberSameUserName = findOneByCriteria(criteria);
		}catch(NoResultException e){
			LOG.debug("Username is valid!");
		}

		if (memberSameUserName != null) {
			String errMessage = "This username is already taken.";
			LOG.error(errMessage);
			SignUpException exception = new SignUpException(ERR_USER_NAME_ALREADY_USED, errMessage);
			throw exception;
		}

	}

	@Override
	public Member findOneByCriteria(MemberCriteria criteria) {
		
		MemberSpecification memberSpec = new MemberSpecification(criteria);
		
		return memberDao.findOne(memberSpec);
		
	}

	@Override
	public List<Long> findIdTeamsByMemberId(Long idMember) throws UserException {
		Member user = memberDao.findOne(idMember);

		if (user == null) {
			String error = "The user with id: " + idMember + " doesn't exist";
			LOG.error(error);

			throw new UserException(ERR_USER_DOES_NOT_EXIST, error);
		}

		List<Long> listTeams = new ArrayList<Long>();

		if (!CollectionUtils.isEmpty(user.getTeams())) {
			for (Team t : user.getTeams()) {
				listTeams.add(t.getId());
			}
		}

		return listTeams;
	}


	@Override
	public List<CustomMember> findByCriteria(MemberCriteria criteria) {
		MemberSpecification memberSpec = new MemberSpecification(criteria);
		
		return mapper.mapList(memberDao.findAll(memberSpec),CustomMember.class);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void listUpdateMember(List<CustomMember> members, String userName) {
		
		memberDao.save(mapper.mapList(members, Member.class));
		memberDao.flush();
		
	}

	@Override
	public Member findById(Long idMember){
		
		return memberDao.getOne(idMember);
	}




	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void updateIdApplicationLanguageMembers(Long idOrganization, Integer idApplicationLanguage, String userName)
			throws UserException {
		List<Member> listMembers = memberDao.findByOrganizationIdOrganization(idOrganization);
		
		Date now = new Date();
		
		for(Member member:listMembers){
			member.setIdApplicationLanguage(idApplicationLanguage);
			member.setModificationDate(now);
			member.setModifier(userName);
		}
		
		memberDao.save(listMembers);
		memberDao.flush();
		
	}
	

}
