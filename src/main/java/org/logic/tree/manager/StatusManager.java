package org.logic.tree.manager;

import java.util.List;
import java.util.Map;

import org.logic.tree.controller.websocket.DiagramMessage;
import org.logic.tree.manager.custom.CustomStatus;
import org.logic.tree.manager.exception.UserException;


/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a> 
 *
 */


public interface StatusManager {
	
	/**
	 * Creates the first status for the problem
	 * @param newStatus
	 * @param userName identifier of the creator
	 * @return the status
	 * @throws UserException if the user is not valid
	 */
	public CustomStatus saveFirstStatus(CustomStatus newStatus, String userName) throws UserException;
	
	
	/**
	 * Find all the status of a problem.
	 * @param idProblem id of the problem	
	 * @return a list of all problem's status
	 */
	public List<CustomStatus> findStatusByProblem(long idProblem);
	
	
	/**
	 * Updates the values of the status
	 * @param statusUpdated DTO with updated data
	 * @param userName identifier of the modifier
	 * @return status updated
	 * @throws UserException
	 */
	public CustomStatus updateStatus(CustomStatus statusUpdated, String userName) throws UserException;
	
	/**
	 * Updates the diagram of an status
	 * @param message pojo with the diagram to update in a status.
	 * @throws UserException
	 */
	public void updateStatusDiagram(DiagramMessage message) throws UserException;
	
	/**
	 * Find all the status of a problem.
	 * @param idProblem id of the problem	
	 * @return a map with all problem's status, with status' identifier as key
	 */
	public Map<Integer, CustomStatus> findStatusByProblemMap(long idProblem);
	
	/**
	 * Finish the current state and initializes the new one.
	 * 
	 * @param status the current status
	 * @param userName identifier of the modifier
	 * @param newStatus the next status
	 * @return the new current status
	 * @throws UserException
	 */
	public CustomStatus transitStatus(CustomStatus status, String userName, CustomStatus newStatus) throws UserException;
	
	
	/**
	 * Find one the status of a problem.
	 * @param idProblem id of the problem	
	 * @param idStatus id of the status
	 * @return a list of all problem's status
	 */
	public CustomStatus findOneStatusByProblem(long idProblem, int idStatus);
	

}
