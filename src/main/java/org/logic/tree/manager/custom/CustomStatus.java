package org.logic.tree.manager.custom;

import java.io.Serializable;
import java.util.Date;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */


public class CustomStatus extends CustomEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2693158318845317433L;
	
	private Long idProblem;
	
	private Integer idStatus;
	
	private String diagram;
	
	private Date initDate;

	private Date finishDate;

	public CustomStatus() {
		super();
	}

	public Long getIdProblem() {
		return idProblem;
	}

	public void setIdProblem(Long idProblem) {
		this.idProblem = idProblem;
	}

	public Integer getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(Integer idStatus) {
		this.idStatus = idStatus;
	}

	public String getDiagram() {
		return diagram;
	}

	public void setDiagram(String diagram) {
		this.diagram = diagram;
	}

	public Date getInitDate() {
		return initDate;
	}

	public void setInitDate(Date initDate) {
		this.initDate = initDate;
	}

	public Date getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}
	
	
}
