package org.logic.tree.manager.custom;

import java.io.Serializable;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

public class CustomMemberTeam extends CustomEntity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1909854618331482906L;
	

	private Long idTeam;
	
	private Long idMember;

	
	public CustomMemberTeam() {
		super();
	}

	
	public CustomMemberTeam(Long idTeam, Long idMember) {
		super();
		this.idTeam = idTeam;
		this.idMember = idMember;
	}


	public Long getIdTeam() {
		return idTeam;
	}


	public void setIdTeam(Long idTeam) {
		this.idTeam = idTeam;
	}


	public Long getIdMember() {
		return idMember;
	}


	public void setIdMember(Long idMember) {
		this.idMember = idMember;
	}
	
	

}
