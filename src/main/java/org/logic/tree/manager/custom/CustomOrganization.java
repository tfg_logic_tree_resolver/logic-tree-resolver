package org.logic.tree.manager.custom;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public class CustomOrganization extends CustomEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3705833185119018717L;

	private Long idOrganization;
	
	private List<CustomMember> employees = new ArrayList<CustomMember>();
	private List<CustomTeam> teams;
	private List<CustomProblem> problems;

	private String nameOrganization;
	private Integer idApplicationLanguage;
	
	private int numberEmployees;
	private int numberTeams;
	private int numberProblems;
	private int numberResolvedProblems;
	
	private String domain;

	public CustomOrganization() {
		super();
	}

	public List<CustomMember> getEmployees() {
		return employees;
	}

	public void setEmployees(List<CustomMember> employees) {
		this.employees = employees;
		this.numberEmployees = employees.size();
	}

	public List<CustomTeam> getTeams() {
		return teams;
	}

	public void setTeams(List<CustomTeam> teams) {
		this.teams = teams;
		this.numberTeams = teams.size();
	}

	public List<CustomProblem> getProblems() {
		return problems;
	}

	public void setProblems(List<CustomProblem> problems) {
		this.problems = problems;
		this.numberProblems = problems.size();
		
		int resolvedProblems = 0;
		
		for(CustomProblem problem:problems){
			if(problem.isProblemResolved()){
				resolvedProblems++;
			}
		}
		
		this.numberResolvedProblems = resolvedProblems;
	}

	public String getNameOrganization() {
		return nameOrganization;
	}

	public void setNameOrganization(String nameOrganization) {
		this.nameOrganization = nameOrganization;
	}

	public Integer getIdApplicationLanguage() {
		return idApplicationLanguage;
	}

	public void setIdApplicationLanguage(Integer idApplicationLanguage) {
		this.idApplicationLanguage = idApplicationLanguage;
	}

	public Long getIdOrganization() {
		return idOrganization;
	}
	
	

	public int getNumberEmployees() {
		return numberEmployees;
	}

	public void setNumberEmployees(int numberEmployees) {
		this.numberEmployees = numberEmployees;
	}

	public int getNumberTeams() {
		return numberTeams;
	}

	public void setNumberTeams(int numberTeams) {
		this.numberTeams = numberTeams;
	}

	public int getNumberProblems() {
		return numberProblems;
	}

	public void setNumberProblems(int numberProblems) {
		this.numberProblems = numberProblems;
	}

	public void setIdOrganization(Long idOrganization) {
		this.idOrganization = idOrganization;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public int getNumberResolvedProblems() {
		return numberResolvedProblems;
	}

	public void setNumberResolvedProblems(int numberResolvedProblems) {
		this.numberResolvedProblems = numberResolvedProblems;
	}
	
	
}
