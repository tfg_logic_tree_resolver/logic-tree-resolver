package org.logic.tree.manager.custom;

import java.io.Serializable;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public class CustomProblem extends CustomEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6760315572677296146L;

	private Long id;

	private String title;

	private String description;

	private Long idProblemType;

	private CustomTeam team;
	
	private boolean problemResolved;
	
	
	public CustomProblem(){
		super();
	}

	public CustomProblem(String title, String description, Long idProblemType, Long idTeam) {
		super();
		this.title = title;
		this.description = description;
		this.idProblemType = idProblemType;
		CustomTeam team = new CustomTeam();
		team.setId(idTeam);
		this.team = team;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	public Long getIdProblemType() {
		return idProblemType;
	}

	public void setIdProblemType(Long idProblemType) {
		this.idProblemType = idProblemType;
	}

	public CustomTeam getTeam() {
		return team;
	}

	public void setTeam(CustomTeam team) {
		this.team = team;
	}

	public boolean isProblemResolved() {
		return problemResolved;
	}

	public void setProblemResolved(boolean problemResolved) {
		this.problemResolved = problemResolved;
	}



	
}
