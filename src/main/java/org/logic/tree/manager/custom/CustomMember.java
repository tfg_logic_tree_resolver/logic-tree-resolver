package org.logic.tree.manager.custom;

import java.io.Serializable;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public class CustomMember extends CustomEntity implements Serializable {

	private static final long serialVersionUID = 1683809540706306027L;
	private Long idMember;
	private String completeName;
	private String mail;
	private String passwordMember;
	private String userName;
	private Integer idMemberType;
	private boolean isOrganization;
	private CustomOrganization organization;
	private Integer idApplicationLanguage;
	private String color;
	

	public CustomMember() {
		super();
	}

	public Long getIdMember() {
		return idMember;
	}

	public void setIdMember(Long idMember) {
		this.idMember = idMember;
	}

	public String getCompleteName() {
		return completeName;
	}

	public void setCompleteName(String completeName) {
		this.completeName = completeName;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPasswordMember() {
		return passwordMember;
	}

	public void setPasswordMember(String passwordMember) {
		this.passwordMember = passwordMember;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public boolean getIsOrganization() {
		return isOrganization;
	}

	public void setIsOrganization(boolean isOrganization) {
		this.isOrganization = isOrganization;
	}

	public Integer getIdMemberType() {
		return idMemberType;
	}

	public void setIdMemberType(Integer idMemberType) {
		this.idMemberType = idMemberType;
	}

	public CustomOrganization getOrganization() {
		return organization;
	}

	public void setOrganization(CustomOrganization organization) {
		this.organization = organization;
	}

	public void setOrganization(boolean isOrganization) {
		this.isOrganization = isOrganization;
	}

	public Integer getIdApplicationLanguage() {
		return idApplicationLanguage;
	}

	public void setIdApplicationLanguage(Integer idApplicationLanguage) {
		this.idApplicationLanguage = idApplicationLanguage;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	
}
