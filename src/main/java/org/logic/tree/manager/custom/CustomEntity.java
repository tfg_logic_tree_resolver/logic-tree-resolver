package org.logic.tree.manager.custom;

import java.util.Date;

import org.logic.tree.util.Auditable;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

public abstract class CustomEntity implements Auditable{

	protected String creator;
	protected Date creationDate;
	protected String modifier;
	protected Date modificationDate;
	protected String deleter;
	protected Date deleteDate;
	
	
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	public Date getModificationDate() {
		return modificationDate;
	}
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}
	public String getDeleter() {
		return deleter;
	}
	public void setDeleter(String deleter) {
		this.deleter = deleter;
	}
	public Date getDeleteDate() {
		return deleteDate;
	}
	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}
	
	@Override
	public void saveOperation(String user, Date date) {
		this.setCreator(user);
		this.setCreationDate(date);
		this.setModifier(user);
		this.setModificationDate(date);	
	}
	
	@Override
	public void updateOperation(String user, Date date) {
		this.setModifier(user);
		this.setModificationDate(date);	
	}
		
	
	@Override
	public void deleteOperation(String user, Date date) {
		this.setDeleteDate(date);
		this.setDeleter(user);
		this.setModifier(user);
		this.setModificationDate(date);	
		
	}
	
	
	
	
}
