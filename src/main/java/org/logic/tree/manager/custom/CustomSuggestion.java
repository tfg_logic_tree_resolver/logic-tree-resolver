package org.logic.tree.manager.custom;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */


public class CustomSuggestion extends CustomEntity {
	
	
	private Long idSuggestion;
	
	private Long idStatus;
	
	private Long idProblem;
	
	private String description;
	

	public Long getIdSuggestion() {
		return idSuggestion;
	}

	public void setIdSuggestion(Long idSuggestion) {
		this.idSuggestion = idSuggestion;
	}

	public Long getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(Long idStatus) {
		this.idStatus = idStatus;
	}

	public Long getIdProblem() {
		return idProblem;
	}

	public void setIdProblem(Long idProblem) {
		this.idProblem = idProblem;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	

}
