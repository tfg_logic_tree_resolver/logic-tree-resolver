package org.logic.tree.manager.custom;

import java.io.Serializable;
import java.util.List;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

public class CustomTeam extends CustomEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2370250507415395359L;
	
	private Long id;
	private String nameTeam;
	private Long idOrganization;
	private List<String> mailMembers;
	
	
	public CustomTeam() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNameTeam() {
		return nameTeam;
	}

	public void setNameTeam(String nameTeam) {
		this.nameTeam = nameTeam;
	}

	public Long getIdOrganization() {
		return idOrganization;
	}

	public void setIdOrganization(Long idOrganization) {
		this.idOrganization = idOrganization;
	}

	public List<String> getMailMembers() {
		return mailMembers;
	}

	public void setMailMembers(List<String> mailMembers) {
		this.mailMembers = mailMembers;
	}

	
}
