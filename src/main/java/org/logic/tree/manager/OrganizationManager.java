package org.logic.tree.manager;

import java.util.List;

import org.logic.tree.manager.custom.CustomOrganization;
import org.logic.tree.manager.exception.UserException;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a> 
 *
 */

public interface OrganizationManager {

	
	/**
	 * Recover all the data of an organization
	 * @param idOrganization
	 * @return
	 */
	public CustomOrganization getDetail(Long idOrganization);
	
	
	/**
	 * Updates the basic data of an organization (name, language)
	 * @param custom
	 * @param userName
	 * @throws UserException
	 */
	public void updateGeneralDetail(CustomOrganization custom, String userName) throws UserException;
	
	/**
	 * Associates existing members to an organization
	 * @param idMembers
	 * @param idOrganization
	 * @param userName
	 */
	public void associateEmployees(List<Long> idMembers, Long idOrganization, String userName);
	
	/**
	 * Creates a new team in a organization
	 * @param idOrganization
	 * @param teamName
	 * @param userName
	 */
	public void createTeam(Long idOrganization, String teamName, String userName);
	
	/**
	 * Updates the members of a team
	 * @param idMembers
	 * @param idTeam
	 * @param idOrganization
	 * @param action
	 * @param userName
	 */
	public void updateTeamWithEmployees(List<Long> idMembers,Long idTeam, Long idOrganization,String action, String userName);
	
	
}
