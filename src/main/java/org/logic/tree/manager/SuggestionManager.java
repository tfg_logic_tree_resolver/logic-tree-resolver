package org.logic.tree.manager;

import java.util.List;

import org.logic.tree.manager.custom.CustomSuggestion;
import org.logic.tree.manager.exception.UserException;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a> 
 *
 */

public interface SuggestionManager {
	
	/**
	 * Finds all suggestions of a problem in a given state
	 * @param idProblem problem's identifier
	 * @param idStatus status's identifier
	 * @return a list with suggestions
	 * @throws UserException
	 */
	public List<CustomSuggestion> findAllSuggestionsByProblemAndStatus(Long idProblem, Long idStatus) throws UserException;
	
	/**
	 * Creates a suggestion
	 * @param customSuggestion
	 * @param userName
	 * @return The created Suggestion
	 * @throws UserException
	 */
	public CustomSuggestion saveSuggestion(CustomSuggestion customSuggestion, String userName) throws UserException;

}
