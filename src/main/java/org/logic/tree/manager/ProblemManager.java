package org.logic.tree.manager;

import java.util.List;

import org.logic.tree.dao.criteria.ProblemCriteria;
import org.logic.tree.manager.custom.CustomProblem;
import org.logic.tree.manager.exception.UserException;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a> 
 *
 */

public interface ProblemManager {
	
	/**
	 * Creates an issue in the database
	 * @param custom with title, description and team owner. 
	 * @param idUser identifier of the creator
	 * @return Issue's identifier in database
	 * @throws UserException if the user is not valid
	 */

	public Long saveIssue(CustomProblem custom, String userName) throws UserException;
	
	/**
	 * This method searches for issues the user can visualize or edit, which are owned by 
	 * the organization.
	 * @param idUser identifier of user.
	 * @return a list of issues
	 */
	public List<CustomProblem> findAllIssuesByUserId(Long idUser) throws UserException;
	
	
	/**
	 * Search problems by criteria
	 * @param criteria
	 * @return list of problems
	 */
	public List<CustomProblem> findByCriteria(ProblemCriteria criteria);
	
	/**
	 * Returns an issue founded by its identifier
	 * @param idIssue issue's identifier
	 * @return the issue
	 */
	public CustomProblem getIssueById(Long idIssue);
	
	/**
	 * Updates problems in the database
	 * @param custom
	 * @param userName
	 * @throws UserException
	 */
	public void updateProblem(CustomProblem custom, String userName) throws UserException;
}
