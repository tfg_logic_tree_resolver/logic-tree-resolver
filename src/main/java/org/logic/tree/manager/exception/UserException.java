package org.logic.tree.manager.exception;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public class UserException extends ModelException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3703312445326485914L;

	public final static int ERR_USER_DOES_NOT_EXIST = -1;
	public final static int ERR_USER_DOES_NOT_HAVE_ANY_TEAM = -2;
	public final static int ERR_USER_DOES_NOT_HAVE_PERMISSION_IN_TEAM = -3;
	public final static int ERR_USER_DOES_NOT_BELONG_TO_ANY_ORGANIZATION = -4;

	public UserException() {
		super();

	}

	public UserException(int errorCode, String errorMessage) {
		super(errorCode, errorMessage);

	}

}
