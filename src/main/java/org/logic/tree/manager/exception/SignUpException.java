package org.logic.tree.manager.exception;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public class SignUpException extends ModelException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3700719400852634742L;

	public final static int ERR_USER_NAME_ALREADY_USED = -1;
	public final static int ERR_EMAIL_ALREADY_USED = -2;

	public SignUpException() {
		super();

	}

	public SignUpException(int errorCode, String errorMessage) {
		super(errorCode, errorMessage);

	}

}
