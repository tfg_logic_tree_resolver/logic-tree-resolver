package org.logic.tree.manager.exception;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public class ModelException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7187532363850640049L;

	protected int errorCode;

	protected String errorMessage;

	public ModelException(int errorCode, String errorMessage) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public ModelException() {
		super();

	}

	public ModelException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

	public ModelException(String message, Throwable cause) {
		super(message, cause);

	}

	public ModelException(String message) {
		super(message);

	}

	public ModelException(Throwable cause) {
		super(cause);

	}

}
