package org.logic.tree.controller;

import static org.logic.tree.util.UserRedirecter.redirect;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.logic.tree.dao.vo.Member;
import org.logic.tree.manager.MemberManager;
import org.logic.tree.manager.custom.CustomMember;
import org.logic.tree.manager.exception.ModelException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

@Controller
@RequestMapping({"/sign_in"})
public class SignInController {
	
	
	@Autowired
	private MemberManager userManager;
	

	@RequestMapping(method=RequestMethod.GET)
	public String showForm(Model model){
		if(!model.containsAttribute("user")){
			model.addAttribute(new CustomMember());
		}
		return "sign_in";
	}

	@RequestMapping(method=RequestMethod.POST)
	public String accessUser(@Valid CustomMember user, BindingResult bindingResult, HttpSession httpSession){
		
		
		if(bindingResult.hasErrors()){
			return "sign_in";
		}
		
		try {
			
			List<String> listSessionValues = Collections.list(httpSession.getAttributeNames());
			
			
			for(String name:listSessionValues){
				httpSession.removeAttribute(name);
			}
			
			Member member = getUserManager().signIn(user);
						
			if(member == null){
				bindingResult.addError(new ObjectError("signin-form", "User or password are incorrect or there is no account with this email"));
				return "sign_in";
			}
			
			httpSession.setAttribute("member", member);
			
			
			return redirect(member.getIdMemberType(), true);
			
						
		} catch (ModelException e) {
			bindingResult.addError(new ObjectError("signin-form", "User or password are incorrect."));
			return "sign_in";
		}
		

	}

	public MemberManager getUserManager() {
		return userManager;
	}
	
	
}
