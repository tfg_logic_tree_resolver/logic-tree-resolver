package org.logic.tree.controller;

import static org.logic.tree.util.Constants.MEMBER_TYPE_REGULAR;
import static org.logic.tree.util.Constants.STATUS_STEP_1;
import static org.logic.tree.util.Constants.STATUS_STEP_2;
import static org.logic.tree.util.Constants.STATUS_STEP_3;
import static org.logic.tree.util.Constants.STATUS_STEP_4;
import static org.logic.tree.util.Constants.STATUS_STEP_5;
import static org.logic.tree.util.Constants.STATUS_STEP_6;
import static org.logic.tree.util.Constants.STATUS_STEP_7;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.logic.tree.controller.websocket.DiagramMessage;
import org.logic.tree.dao.vo.Member;
import org.logic.tree.manager.MemberManager;
import org.logic.tree.manager.ProblemManager;
import org.logic.tree.manager.StatusManager;
import org.logic.tree.manager.SuggestionManager;
import org.logic.tree.manager.context.ProblemContext;
import org.logic.tree.manager.context.State;
import org.logic.tree.manager.context.Step1;
import org.logic.tree.manager.context.Step2;
import org.logic.tree.manager.context.Step3;
import org.logic.tree.manager.context.Step4;
import org.logic.tree.manager.context.Step5;
import org.logic.tree.manager.context.Step6;
import org.logic.tree.manager.context.Step7;
import org.logic.tree.manager.custom.CustomProblem;
import org.logic.tree.manager.custom.CustomStatus;
import org.logic.tree.manager.custom.CustomSuggestion;
import org.logic.tree.manager.exception.UserException;
import org.logic.tree.util.ProblemRedirecter;
import org.logic.tree.util.UserRedirecter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

@Controller
@RequestMapping({ "/problem_solving", "problem_solving/root_cause_step_1", "problem_solving/root_cause_step_2",
		"problem_solving/root_cause_step_3", "problem_solving/root_cause_step_4", "problem_solving/root_cause_step_5",
		"problem_solving/root_cause_step_6", "problem_solving/root_cause_step_7" })
public class ProblemSolvingController extends AbstractLTRController {

	private Logger LOG = LoggerFactory.getLogger(ProblemSolvingController.class);

	@Autowired
	private StatusManager statusManager;

	@Autowired
	private SimpMessagingTemplate template;

	@Autowired
	private ProblemManager problemManager;

	@Autowired
	private MemberManager memberManager;

	@Autowired
	private SuggestionManager suggestionManager;

	private static String ON = "on";

	@MessageMapping("/update")
	public void updateDiagram(DiagramMessage message, SimpMessageHeaderAccessor headerAccessor) throws Exception {

		if (checkUserPermissions(message, headerAccessor)) {

			String destination = "/topic/diagram_" + message.getIdProblem() + "_" + message.getIdStatus();

			statusManager.updateStatusDiagram(message);
			
			CustomProblem currentProblem = problemManager.getIssueById(Long.valueOf(message.getIdProblem()));

			problemManager.updateProblem(currentProblem, message.getUserName());

			template.convertAndSend(destination, message);
		}
	}

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView dispatchProblem(HttpSession httpSession, HttpServletRequest request,
			HttpServletResponse response)
			throws UserException, RuntimeException, JsonParseException, JsonMappingException, IOException {

		LOG.debug("Setting the problem");

		setLocale(httpSession, request, response);

		Integer idType = (request.getParameter("type") != null) ? Integer.valueOf(request.getParameter("type"))
				: (Integer) httpSession.getAttribute("idType");
		Long idProblem = (request.getParameter("id") != null) ? Long.valueOf(request.getParameter("id"))
				: (Long) httpSession.getAttribute("idProblem");
		
		Boolean fromStart = Boolean.valueOf(request.getParameter("fromStart"));

		Member member = (Member) httpSession.getAttribute("member");

		CustomProblem problem = problemManager.getIssueById(idProblem);

		// Checking if the user is logged.
		if (member == null) {

			return new ModelAndView("redirect:/home");
		}

		// Checking if the user has permission to access to the problem.

		boolean isMemberOfTheSameOrg = member.getOrganization().getIdOrganization()
				.equals(problem.getTeam().getIdOrganization());

		List<Long> idTeamsOfMember = Collections.emptyList();

		idTeamsOfMember = memberManager.findIdTeamsByMemberId(member.getIdMember());

		boolean isMemberOfTheSameTeam = idTeamsOfMember.contains(problem.getTeam().getId());

		// Redirection if member type isn't correct
		if (member.getIdMemberType() != MEMBER_TYPE_REGULAR || !isMemberOfTheSameOrg) {
			return new ModelAndView(UserRedirecter.redirect(member.getIdMemberType(), false));
		}

		// Obtain statuses
		Map<Integer, CustomStatus> mapStatus = statusManager.findStatusByProblemMap(idProblem);

		// Find actual status
		CustomStatus finalStatus = null;
		for (CustomStatus status : mapStatus.values()) {
			if (status.getFinishDate() == null) {
				finalStatus = status;
				break;
			}
		}

		Integer activeStatus = (httpSession.getAttribute("activeStatus") == null) ? finalStatus.getIdStatus()
				: (Integer) httpSession.getAttribute("activeStatus");
		
		if (fromStart){
			activeStatus = STATUS_STEP_1;
		}
		String savedModelStep = null;

		ProblemContext problemContext = (ProblemContext) httpSession.getAttribute("problemContext");

		if (problemContext == null || problemContext.getIdProblem() != idProblem) {
			problemContext = initProblemContext(activeStatus, idType, idProblem);

			httpSession.setAttribute("problemContext", problemContext);
		}

		if (mapStatus.containsKey(activeStatus)) {

			savedModelStep = mapStatus.get(activeStatus).getDiagram();

			if (activeStatus == STATUS_STEP_1 && savedModelStep == null) {
				savedModelStep = problemContext.getCurrentDiagram();
			}

		}

		// Find suggestions
		List<CustomSuggestion> suggestions = suggestionManager.findAllSuggestionsByProblemAndStatus(idProblem,
				(long) activeStatus);

		httpSession.setAttribute("activeStatus", activeStatus);
		httpSession.setAttribute("idType", idType);
		httpSession.setAttribute("member", member);
		httpSession.setAttribute("mapStatus", mapStatus);
		httpSession.setAttribute("description", problem.getDescription());
		httpSession.setAttribute("title", problem.getTitle());
		httpSession.setAttribute("idProblem", idProblem);
		httpSession.setAttribute("memberCanEdit", isMemberOfTheSameTeam);
		httpSession.setAttribute("savedModelStep", savedModelStep);
		httpSession.setAttribute("suggestions", suggestions);

		ModelAndView modelAndView = new ModelAndView(ProblemRedirecter.redirect(idType, false));

		modelAndView.addObject("member", member);
		modelAndView.addObject("description", problem.getDescription());
		modelAndView.addObject("title", problem.getTitle());
		modelAndView.addObject("mapStatus", mapStatus);
		modelAndView.addObject("idProblem", idProblem);
		modelAndView.addObject("idType", idType);
		modelAndView.addObject("activeStatus", activeStatus);
		modelAndView.addObject("memberCanEdit", isMemberOfTheSameTeam);
		modelAndView.addObject("savedModelStep", savedModelStep);
		modelAndView.addObject("suggestions", suggestions);

		return modelAndView;

	}

	@RequestMapping(value = "working_problem", method = RequestMethod.POST)
	public ModelAndView workingOnProblem(HttpServletRequest request, HttpSession httpSession)
			throws UserException, JsonParseException, JsonMappingException, IOException, RuntimeException {

		Member member = (Member) httpSession.getAttribute("member");
		
		Integer activeStatus =  (Integer) httpSession.getAttribute("activeStatus");
		Integer idType = (Integer) httpSession.getAttribute("idType");

		// Checking if the user is logged.
		if (member == null) {

			return new ModelAndView("redirect:/home");
		}

		String action = request.getParameter("action");
		Boolean redoDiagram = ON.equals(request.getParameter("redo_diagram"));

		Long idProblem = (Long) httpSession.getAttribute("idProblem");

		
		ProblemContext context = (ProblemContext) httpSession.getAttribute("problemContext");
		
		if (context == null || context.getIdProblem() != idProblem) {
			context = initProblemContext(activeStatus, idType, idProblem);

			httpSession.setAttribute("problemContext", context);
		}
		

		switch (action) {
		case "previous":
			setPreviousStep(context, httpSession);
			break;
		case "next":
			nextStep(context, httpSession, redoDiagram);
			break;
		case "resolve":
			resolveProblem(idProblem, member.getUserName());
			return new ModelAndView("redirect:/home");
		}

		return new ModelAndView("redirect:/problem_solving", "customProblem", new CustomProblem());

	}

	@RequestMapping(value = "save_suggestion", method = RequestMethod.POST)
	public ModelAndView saveSuggestion(HttpServletRequest request, HttpSession httpSession) throws UserException {

		Member member = (Member) httpSession.getAttribute("member");

		// Checking if the user is logged.
		if (member == null) {

			return new ModelAndView("redirect:/home");
		}

		Integer activeStatus = (Integer) httpSession.getAttribute("activeStatus");
		Long idProblem = (Long) httpSession.getAttribute("idProblem");

		boolean memberCanSuggest = !(Boolean) httpSession.getAttribute("memberCanEdit");

		String descriptionSuggestion = request.getParameter("suggestion-description");

		if (memberCanSuggest) {
			
			CustomSuggestion customSuggestion = new CustomSuggestion();
			customSuggestion.setIdProblem(idProblem);
			customSuggestion.setIdStatus((long) activeStatus);
			customSuggestion.setDescription(descriptionSuggestion);

			suggestionManager.saveSuggestion(customSuggestion, member.getUserName());
		}

		return new ModelAndView("redirect:/problem_solving", "customProblem", new CustomProblem());
	}

	private ProblemContext initProblemContext(Integer idStatus, Integer idType, Long idProblem)
			throws JsonParseException, JsonMappingException, IOException {

		State currentState = null;

		switch (idStatus) {
		case STATUS_STEP_1:
			currentState = new Step1();
			break;
		case STATUS_STEP_2:
			currentState = new Step2();
			break;
		case STATUS_STEP_3:
			currentState = new Step3();
			break;
		case STATUS_STEP_4:
			currentState = new Step4();
			break;
		case STATUS_STEP_5:
			currentState = new Step5();
			break;
		case STATUS_STEP_6:
			currentState = new Step6();
			break;
		case STATUS_STEP_7:
			currentState = new Step7();
			break;
		}

		return new ProblemContext(idProblem, idType, currentState);
	}

	private void setPreviousStep(ProblemContext context, HttpSession httpSession)
			throws JsonParseException, JsonMappingException, IOException, NullPointerException {

		LOG.debug("Go previous step");
		context.goPrevious();
		httpSession.setAttribute("problemContext", context);
		httpSession.setAttribute("activeStatus", context.getCurrentState().getIdStep());

	}

	private void resolveProblem(Long idProblem, String userName) throws UserException {
		CustomProblem problem = problemManager.getIssueById(idProblem);
		problem.setProblemResolved(true);

		problemManager.updateProblem(problem, userName);

	}

	private void nextStep(ProblemContext context, HttpSession httpSession, boolean redo)
			throws UserException, JsonParseException, JsonMappingException, IOException, NullPointerException {

		LOG.debug("Go next step");

		Integer idPreviousStatus = context.getCurrentState().getIdStep();
		CustomStatus previousStatus = statusManager.findOneStatusByProblem(context.getIdProblem(), idPreviousStatus);

		Map<Integer, CustomStatus> mapStatus = statusManager.findStatusByProblemMap(context.getIdProblem());

		Member member = (Member) httpSession.getAttribute("member");

		context.setCurrentDiagram(previousStatus.getDiagram());
		context.goNext();

		Integer activeStatus = context.getCurrentState().getIdStep();

		httpSession.setAttribute("problemContext", context);
		httpSession.setAttribute("activeStatus", activeStatus);

		// If the users have already gone until this step, it's not necessary to
		// create the status
		if (!mapStatus.containsKey(activeStatus)) {

			CustomProblem currentProblem = problemManager.getIssueById(context.getIdProblem());

			problemManager.updateProblem(currentProblem, member.getUserName());

			CustomStatus newStatus = new CustomStatus();

			newStatus.setIdProblem(context.getIdProblem());

			newStatus.setIdStatus(activeStatus);

			newStatus.setDiagram(context.getCurrentDiagram());

			statusManager.transitStatus(previousStatus, member.getUserName(), newStatus);

		} else if (redo) {
			CustomProblem currentProblem = problemManager.getIssueById(context.getIdProblem());

			problemManager.updateProblem(currentProblem, member.getUserName());

			CustomStatus redoStatus = statusManager.findOneStatusByProblem(context.getIdProblem(), activeStatus);
			redoStatus.setDiagram(context.getCurrentDiagram());
			statusManager.updateStatus(redoStatus, member.getUserName());
		}

	}

	private boolean checkUserPermissions(DiagramMessage message, SimpMessageHeaderAccessor headerAccessor) {

		Map<String, Object> attrs = headerAccessor.getSessionAttributes();

		if (attrs.containsKey("member")) {
			Member member = (Member) attrs.get("member");
			if (!member.getUserName().equals(message.getUserName())) {
				return false;
			}
		}

		return ((boolean) attrs.containsKey("memberCanEdit") && (boolean) attrs.get("memberCanEdit"));

	}

}
