package org.logic.tree.controller;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.logic.tree.dao.vo.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

public abstract class AbstractLTRController {

	@Autowired
	SessionLocaleResolver localeResolver;
	

	public static final Map<String, String> languages;
	static
	{
	    languages = new LinkedHashMap<String, String>();
		languages.put("1", "English");
		languages.put("2", "Galego");
		languages.put("3", "Español");
	}
	
	public static final Map<Integer, Locale> locales;
	static
	{
	    locales = new LinkedHashMap<Integer, Locale>();
		locales.put(1, new Locale("en","EN"));
		locales.put(2, new Locale("gl","ES"));
		locales.put(3, new Locale("es","ES"));
	}
	
	
	
	
	public void setLocale(HttpSession httpSession,HttpServletRequest request, HttpServletResponse response){
		
		Member member = (Member) httpSession.getAttribute("member");
		
		if(member != null){
			Locale locale = locales.get(member.getIdApplicationLanguage());
			
			Locale.setDefault(locale);
		
			localeResolver.setLocale(request, response, locale);
			
		}
		
	} 
	

}
