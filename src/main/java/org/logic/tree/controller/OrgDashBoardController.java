package org.logic.tree.controller;

import static org.logic.tree.util.Constants.MEMBER_TYPE_ORGANIZATION;
import static org.logic.tree.util.Constants.MEMBER_TYPE_REGULAR;
import static org.logic.tree.util.UserRedirecter.redirect;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.logic.tree.dao.criteria.MemberCriteria;
import org.logic.tree.dao.vo.Member;
import org.logic.tree.manager.MemberManager;
import org.logic.tree.manager.OrganizationManager;
import org.logic.tree.manager.custom.CustomMember;
import org.logic.tree.manager.custom.CustomOrganization;
import org.logic.tree.manager.exception.UserException;
import org.logic.tree.util.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

@Controller
@RequestMapping({ "/org_dashboard","org_dashboard/org_dashboard_tab_general","org_dashboard/org_dashboard_tab_employees","org_dashboard/org_dashboard_tab_team" })
@SessionAttributes({"languages"})
public class OrgDashBoardController  extends AbstractLTRController{
	
	public static final String GENERAL_TAB = "tab-profile-general";
	public static final String EMPLOYEES_TAB = "tab-profile-employees";
	public static final String TEAMS_TAB = "tab-profile-teams";
	

	@Autowired
	OrganizationManager orgManager;
	
	@Autowired
	MemberManager memberManager;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showDashBoardPage(HttpSession httpSession, HttpServletRequest request, HttpServletResponse response) {
		
		setLocale(httpSession,request,response);
		
		if (httpSession.getAttribute("member") == null) {

			return new ModelAndView("redirect:/home");
		}

		Member member = (Member) httpSession.getAttribute("member");
		
		//Redirection if member type isn't correct
		if(member.getIdMemberType()!= MEMBER_TYPE_ORGANIZATION){
			return new ModelAndView(redirect(member.getIdMemberType(),false));
		}
		
		ModelAndView modelView = new ModelAndView("org_dashboard");
		
		if(httpSession.getAttribute("org") == null){
		
			CustomOrganization org = orgManager.getDetail(member.getOrganization().getIdOrganization());

			httpSession.setAttribute("org", org);
			modelView.addObject("org", org);
			modelView.addObject("selectedLang", org.getIdApplicationLanguage());

		}
		

		modelView.addObject("customOrg", new CustomOrganization());
		

		modelView.addObject("languages", languages);
		
		
		String tab = (httpSession.getAttribute("selectedTab") == null)?GENERAL_TAB:(String) httpSession.getAttribute("selectedTab");
		
		modelView.addObject("selectedTab",tab);

		return modelView;
	}

	@RequestMapping(value = "save_general" , method = RequestMethod.POST)
	public ModelAndView saveGeneralTab(@Valid CustomOrganization customOrg, BindingResult bindingResult,
			HttpSession httpSession,HttpServletRequest request, HttpServletResponse response) throws UserException {

		Member member = (Member) httpSession.getAttribute("member");
		
		// Checking if the user is logged.
		if (member == null) {

			return new ModelAndView("redirect:/home");
		}
		
		if (customOrg.getNameOrganization() == null) {
			bindingResult
					.addError(new ObjectError("org-form1", "The Organization's name can not be blank."));
			return new ModelAndView("org_dashboard", "customOrg", new CustomOrganization());
		}
		
		
		CustomOrganization org = (CustomOrganization) httpSession.getAttribute("org");

		org.setNameOrganization(customOrg.getNameOrganization());
		org.setIdApplicationLanguage(customOrg.getIdApplicationLanguage());
		

		orgManager.updateGeneralDetail(org, member.getUserName());
		
		CustomOrganization updatedOrg = orgManager.getDetail(org.getIdOrganization());
		
		httpSession.setAttribute("selectedLang", org.getIdApplicationLanguage());
		httpSession.setAttribute("selectedTab",GENERAL_TAB);

		httpSession.setAttribute("org", updatedOrg);
		
	    setLocale(httpSession, request, response);
		
		return new ModelAndView("redirect:/org_dashboard", "customOrg", new CustomOrganization());

	}
	
	@RequestMapping(value = "search_employees" , method = RequestMethod.POST)
	public ModelAndView searchEmployeesByDomain(@Valid CustomOrganization customOrg,BindingResult bindingResult, HttpSession httpSession){
		
		Member member = (Member) httpSession.getAttribute("member");
		
		// Checking if the user is logged.
		if (member == null) {

			return new ModelAndView("redirect:/home");
		}
		
		
		if(StringUtils.isEmpty(customOrg.getDomain())){
			
			bindingResult.addError(new ObjectError("org-form2", "The domain can not be blank."));
			httpSession.setAttribute("selectedTab",EMPLOYEES_TAB);
			httpSession.setAttribute("membersList",Collections.EMPTY_LIST);
			
			return new ModelAndView("redirect:/org_dashboard","customOrg", new CustomOrganization());
			
		}
		
		MemberCriteria criteria = new MemberCriteria();
		criteria.setDomain(customOrg.getDomain());
		criteria.setIdType(MEMBER_TYPE_REGULAR);
		
		List<CustomMember> membersList = memberManager.findByCriteria(criteria);
		
		httpSession.setAttribute("membersList",membersList);
		httpSession.setAttribute("selectedTab",EMPLOYEES_TAB);
		httpSession.setAttribute("idOrg",((CustomOrganization) httpSession.getAttribute("org")).getIdOrganization());
	    
		ModelAndView modelAndView = new ModelAndView("redirect:/org_dashboard","customOrg", new CustomOrganization());
		
		modelAndView.addObject("selectedEmployees", new ArrayList<CustomMember>());

		
		return new ModelAndView("redirect:/org_dashboard","customOrg", new CustomOrganization());
		
	}
	
	@RequestMapping(value = "save_employees" , method = RequestMethod.POST)
	public ModelAndView saveEmployees(HttpServletRequest request, HttpSession httpSession){
		
		Member member = (Member) httpSession.getAttribute("member");
		
		// Checking if the user is logged.
		if (member == null) {

			return new ModelAndView("redirect:/home");
		}
		
		
		String[] selectedEmployees =request.getParameterValues("selectedEmployees");
		if(selectedEmployees == null || selectedEmployees.length == 0){
			
			httpSession.setAttribute("selectedTab",EMPLOYEES_TAB);
					
			return new ModelAndView("redirect:/org_dashboard","customOrg", new CustomOrganization());
		}
		
		Long idOrganization = ((CustomOrganization) httpSession.getAttribute("org")).getIdOrganization();
		
		List<Long> idsSelectedEmployees = NumberUtils.fromStringToLong(Arrays.asList(selectedEmployees));
		
		
		orgManager.associateEmployees(idsSelectedEmployees, idOrganization, member.getUserName());
		
		CustomOrganization updatedOrg = orgManager.getDetail(idOrganization);
		
		httpSession.setAttribute("selectedTab",EMPLOYEES_TAB);
		httpSession.setAttribute("idOrg",idOrganization);

		httpSession.setAttribute("org", updatedOrg);
		
	
		httpSession.setAttribute("membersList",updatedOrg.getEmployees());
		
		return new ModelAndView("redirect:/org_dashboard","customOrg", new CustomOrganization());
	}
	
	@RequestMapping(value = "save_team" , method = RequestMethod.POST)
	public ModelAndView saveTeam(HttpServletRequest request, HttpSession httpSession){
		
		Member member = (Member) httpSession.getAttribute("member");
		
		// Checking if the user is logged.
		if (member == null) {

			return new ModelAndView("redirect:/home");
		}
		
		
		Long idOrganization = ((CustomOrganization) httpSession.getAttribute("org")).getIdOrganization();
		String teamName = request.getParameter("team-name");
		
		if(StringUtils.isBlank(teamName)){
			httpSession.setAttribute("selectedTab",TEAMS_TAB);
			
			return new ModelAndView("redirect:/org_dashboard","customOrg", new CustomOrganization());
		}
		
		
		orgManager.createTeam(idOrganization, teamName, member.getUserName());
		
		CustomOrganization updatedOrg = orgManager.getDetail(idOrganization);
		
		httpSession.setAttribute("org", updatedOrg);
		httpSession.setAttribute("selectedTab",TEAMS_TAB);
		httpSession.setAttribute("idOrg",idOrganization);

				
		return new ModelAndView("redirect:/org_dashboard","customOrg", new CustomOrganization());
	}
	
	@RequestMapping(value = "update_teams" , method = RequestMethod.POST)
	public ModelAndView updateTeams(HttpServletRequest request, HttpSession httpSession){
		
		Member member = (Member) httpSession.getAttribute("member");
		
		// Checking if the user is logged.
		if (member == null) {

			return new ModelAndView("redirect:/home");
		}
		
		
		Long idOrganization = ((CustomOrganization) httpSession.getAttribute("org")).getIdOrganization();
		
		String teamSelected = request.getParameter("organization-teams");
		String[] selectedEmployees =request.getParameterValues("organization-team-employees");
		String action = request.getParameter("action");
		
		
		if(StringUtils.isBlank(teamSelected) || selectedEmployees == null || selectedEmployees.length == 0 || StringUtils.isBlank(action)){
			httpSession.setAttribute("selectedTab",TEAMS_TAB);
			
			return new ModelAndView("redirect:/org_dashboard","customOrg", new CustomOrganization());
		}
		
		List<Long> idsSelectedEmployees = NumberUtils.fromStringToLong(Arrays.asList(selectedEmployees));
		Long idTeam = Long.valueOf(teamSelected);

		
		orgManager.updateTeamWithEmployees(idsSelectedEmployees, idTeam, idOrganization,action, member.getUserName());
		
		CustomOrganization updatedOrg = orgManager.getDetail(idOrganization);
		
		httpSession.setAttribute("org", updatedOrg);
		httpSession.setAttribute("selectedTab",TEAMS_TAB);
		

				
		return new ModelAndView("redirect:/org_dashboard","customOrg", new CustomOrganization());
	}



}
