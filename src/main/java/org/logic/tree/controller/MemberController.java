package org.logic.tree.controller;

import static java.lang.String.valueOf;
import static org.logic.tree.util.Constants.MEMBER_TYPE_REGULAR;
import static org.logic.tree.util.Constants.PROBLEM_TYPE_GOALS;
import static org.logic.tree.util.Constants.PROBLEM_TYPE_PROS_CONS;
import static org.logic.tree.util.Constants.PROBLEM_TYPE_ROOT_CAUSES;
import static org.logic.tree.util.UserRedirecter.redirect;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.logic.tree.dao.criteria.ProblemCriteria;
import org.logic.tree.dao.criteria.TeamCriteria;
import org.logic.tree.dao.vo.Member;
import org.logic.tree.dao.vo.Team;
import org.logic.tree.manager.MemberManager;
import org.logic.tree.manager.ProblemManager;
import org.logic.tree.manager.TeamManager;
import org.logic.tree.manager.custom.CustomProblem;
import org.logic.tree.manager.exception.UserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

@Controller
@RequestMapping({ "/user_dashboard" })
@SessionAttributes({ "problemTypes" })
public class MemberController extends AbstractLTRController {

	@Autowired
	private ProblemManager problemManager;

	@Autowired
	private MemberManager memberManager;

	@Autowired
	private TeamManager teamManager;

	private Logger LOG = LoggerFactory.getLogger(MemberController.class);

	public static final Map<String, String> problemTypes;
	static {
		problemTypes = new LinkedHashMap<String, String>();
		problemTypes.put(valueOf(PROBLEM_TYPE_ROOT_CAUSES), "RootCause");
		problemTypes.put(valueOf(PROBLEM_TYPE_GOALS), "Goals");
		problemTypes.put(valueOf(PROBLEM_TYPE_PROS_CONS), "ProsCons");
	}

	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showDashBoardPage(HttpSession httpSession, HttpServletRequest request,
			HttpServletResponse response) throws UserException {

		setLocale(httpSession, request, response);

		if (httpSession.getAttribute("member") == null) {

			return new ModelAndView("redirect:/home");
		}

		Member member = (Member) httpSession.getAttribute("member");

		
		
		// Redirection if member type isn't correct
		if (member.getIdMemberType() != MEMBER_TYPE_REGULAR) {
			return new ModelAndView(redirect(member.getIdMemberType(), false));
		}

		ModelAndView modelView = new ModelAndView("user_dashboard", "customProb", new CustomProblem());
		
		TeamCriteria criteria = new TeamCriteria();
		if(member.getOrganization()!=null){
			criteria.setIdOrganization(member.getOrganization().getIdOrganization());
			List<Team> listOrgTeams = teamManager.findByCriteria(criteria);
			httpSession.setAttribute("listOrgTeams", listOrgTeams);
			
			List<Long> listIdTeams = memberManager.findIdTeamsByMemberId(member.getIdMember());
			httpSession.setAttribute("listIdTeams", listIdTeams);

			
			List<Team> teams = filterMemberTeams(member, listOrgTeams, listIdTeams);
			httpSession.setAttribute("teams", teams);
			
			
			List<CustomProblem> problems = findProblems(member);
			httpSession.setAttribute("problems", problems);
			
			
			List<CustomProblem> problemstosuggest = findProblemsForSuggestions(member, listOrgTeams, listIdTeams);
			httpSession.setAttribute("problemstosuggest", problemstosuggest);
			
			modelView.addObject("teams", teams);
			modelView.addObject("problems", problems);
			modelView.addObject("problemstosuggest", problemstosuggest);
		}

			

		modelView.addObject("member", member);
		modelView.addObject("problemTypes", problemTypes);
		modelView.addObject("typerootcause", PROBLEM_TYPE_ROOT_CAUSES);
		modelView.addObject("typegoals", PROBLEM_TYPE_GOALS);
		modelView.addObject("typeproscons", PROBLEM_TYPE_PROS_CONS);



		//Reset of the problem context
		httpSession.setAttribute("problemContext", null);
		httpSession.setAttribute("activeStatus", null);
		
		return modelView;
	}

	@RequestMapping(value = "save_problem", method = RequestMethod.POST)
	public ModelAndView saveProblem(@Valid CustomProblem customProb, BindingResult bindingResult,
			HttpSession httpSession) {
		
		Member member = (Member) httpSession.getAttribute("member");
		
		// Checking if the user is logged.
		if (member == null) {

			return new ModelAndView("redirect:/home");
		}
		

		if (StringUtils.isBlank(customProb.getDescription()) || StringUtils.isBlank(customProb.getTitle())) {
			bindingResult.addError(new ObjectError("member-form1", "The problem description can not be blank."));
			return new ModelAndView("user_dashboard", "customProb", new CustomProblem());
		}


		try {
			problemManager.saveIssue(customProb, member.getUserName());
		} catch (UserException e) {
			bindingResult.addError(new ObjectError("member-form1", "Error saving the problem:" + e.getErrorMessage()));
		}

		List<CustomProblem> updatedProblemsList = findProblems(member);
		httpSession.setAttribute("problems", updatedProblemsList);

		ModelAndView modelView = new ModelAndView("redirect:/user_dashboard", "customProb", new CustomProblem());

		modelView.addObject("problems", updatedProblemsList);

		return modelView;

	}

	private List<Team> filterMemberTeams(Member member, List<Team> listOrgTeams, List<Long> listIdTeams) {

		List<Team> listMemberTeams = new ArrayList<Team>();

		for (Team t : listOrgTeams) {
			if (listIdTeams.contains(t.getId())) {
				listMemberTeams.add(t);
			}

		}

		return listMemberTeams;

	}

	private List<CustomProblem> findProblems(Member member) {

		try {
			return problemManager.findAllIssuesByUserId(member.getIdMember());
		} catch (UserException e) {
			LOG.error("Error retrieving problems for user:" + e.getErrorMessage());
			return Collections.emptyList();
		}

	}

	private List<CustomProblem> findProblemsForSuggestions(Member member, List<Team> listOrgTeams,
			List<Long> listIdTeams) {

		List<Long> notMemberTeams = new ArrayList<Long>();

		for (Team t : listOrgTeams) {
			if (!listIdTeams.contains(t.getId())) {
				notMemberTeams.add(t.getId());
			}
		}

		if(!notMemberTeams.isEmpty()){
		
			ProblemCriteria criteria = new ProblemCriteria();

			criteria.setListTeams(notMemberTeams);

			return problemManager.findByCriteria(criteria);
		}
		
		return Collections.emptyList();

	}

}
