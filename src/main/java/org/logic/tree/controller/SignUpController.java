package org.logic.tree.controller;



import static org.logic.tree.manager.exception.SignUpException.ERR_EMAIL_ALREADY_USED;
import static org.logic.tree.manager.exception.SignUpException.ERR_USER_NAME_ALREADY_USED;
import static org.logic.tree.util.Constants.MEMBER_TYPE_ORGANIZATION;
import static org.logic.tree.util.Constants.MEMBER_TYPE_REGULAR;
import static org.logic.tree.util.UserRedirecter.redirect;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.logic.tree.dao.vo.Member;
import org.logic.tree.manager.MemberManager;
import org.logic.tree.manager.custom.CustomMember;
import org.logic.tree.manager.exception.ModelException;
import org.logic.tree.manager.exception.SignUpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

@Controller
@RequestMapping({"/sign_up"})
public class SignUpController {
	
	private static Map<Integer, String> errorObjects;
	
    static
    {
    	errorObjects = new HashMap<Integer, String>();
    	errorObjects.put(ERR_EMAIL_ALREADY_USED,"user_email");
    	errorObjects.put(ERR_USER_NAME_ALREADY_USED, "user_name");
    }
	
	private Logger LOG = LoggerFactory.getLogger(SignUpController.class);
	
	@Autowired
	private MemberManager memberManager;
	
	@RequestMapping(method=RequestMethod.GET)
	public String showForm(Model model){
		model.addAttribute(new CustomMember());
		return "sign_up";
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String createUser(@Valid CustomMember user, BindingResult bindingResult,HttpSession httpSession){
		
		if(bindingResult.hasErrors()){
			return "sign_up";
		}
		
		
		try {
			
			int memberType = user.getIsOrganization()?MEMBER_TYPE_ORGANIZATION:MEMBER_TYPE_REGULAR;
			user.setIdMemberType(memberType);
			
			Member member = getMemberManager().saveMemberSignUp(user, user.getUserName());
			
			httpSession.setAttribute("member", member);
			
			LOG.info("User"+member.getIdMember()+" created");
			
			return redirect(member.getIdMemberType(), true);
			
		}catch (SignUpException se){
			bindingResult.addError(new ObjectError(errorObjects.get(se.getErrorCode()), se.getErrorMessage()));
			return "sign_up";
			
		} catch (ModelException e) {
			bindingResult.addError(new ObjectError("signup-form", "There was an unexpected error."));
			return "sign_up";
		}

	}

	public MemberManager getMemberManager() {
		return memberManager;
	}
	
	
}
