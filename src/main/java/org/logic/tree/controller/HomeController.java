package org.logic.tree.controller;

import static org.logic.tree.util.UserRedirecter.redirect;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.logic.tree.dao.vo.Member;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

@Controller
public class HomeController {
	
	@RequestMapping({"/","/home"})
	public String showHomePage(HttpSession httpSession){
		
		Member member = (Member) httpSession.getAttribute("member");
		
		if(member != null){
			
			return redirect(member.getIdMemberType(), true);
		}
		
		return "home";
		
	}
	
	
	@RequestMapping({"logout"})
	public String logOut(HttpSession httpSession){
		
		List<String> listSessionValues = Collections.list(httpSession.getAttributeNames());
		
		
		for(String name:listSessionValues){
			httpSession.removeAttribute(name);
		}
		

		return "home";
	}

}
