package org.logic.tree.controller.websocket;

import java.io.Serializable;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

public class DiagramMessage implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2562890515712991401L;
	
	private String idProblem;
	private String idStatus; 
	private String content;
	private String userName;


	public DiagramMessage() {
		super();
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getIdProblem() {
		return idProblem;
	}

	public void setIdProblem(String idProblem) {
		this.idProblem = idProblem;
	}

	public String getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(String idStatus) {
		this.idStatus = idStatus;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


	
}
