package org.logic.tree.dao.criteria;

import java.io.Serializable;
import java.util.List;


/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */
public class MemberCriteria implements Serializable {

	private static final long serialVersionUID = -1559627991480120112L;
	
	private String completeName;
	private String mail;
	private String userName;
	private String password;
	private Long idUser;
	private String domain;
	private Long idOrganization;
	private List<Long> listIdMembers;
	private Integer idType;
	
	public MemberCriteria() {
		super();
	}

	public String getCompleteName() {
		return completeName;
	}

	public void setCompleteName(String completeName) {
		this.completeName = completeName;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public Long getIdOrganization() {
		return idOrganization;
	}

	public void setIdOrganization(Long idOrganization) {
		this.idOrganization = idOrganization;
	}

	public List<Long> getListIdMembers() {
		return listIdMembers;
	}

	public void setListIdMembers(List<Long> listIdMembers) {
		this.listIdMembers = listIdMembers;
	}

	public Integer getIdType() {
		return idType;
	}

	public void setIdType(Integer idType) {
		this.idType = idType;
	}

	
}
