package org.logic.tree.dao.criteria.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;

import org.logic.tree.dao.criteria.ProblemCriteria;
import org.logic.tree.dao.vo.Problem;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public class ProblemSpecification implements Specification<Problem> {
	
	private final ProblemCriteria criteria;
	
	public ProblemSpecification(ProblemCriteria criteria) {
		this.criteria = criteria;
	}

	@Override
	public Predicate toPredicate(Root<Problem> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		Assert.notNull(criteria);

						
		List<Predicate> predicates = new ArrayList<>();
		EntityType<Problem> Problem_ = root.getModel();
		
		root.fetch("team");

		
		if(criteria.getIdProblemType()!=null){
			predicates.add(cb.equal(root.get(Problem_.getSingularAttribute("idProblemType")),criteria.getIdProblemType()));			
		}
		
		if(!StringUtils.isEmpty(criteria.getCreator())){
			predicates.add(cb.equal(root.get(Problem_.getSingularAttribute("creator")), criteria.getCreator()));		
		}
		
		if(!StringUtils.isEmpty(criteria.getTitle())){
			predicates.add(cb.equal(root.get(Problem_.getSingularAttribute("title")), criteria.getTitle()));
			
		}
		
		if(criteria.getListTeams() != null){
			
			predicates.add(root.get(Problem_.getSingularAttribute("team")).get("id").in(criteria.getListTeams()));
		}
		
		if(criteria.getIdOrganization()!=null){
			predicates.add(cb.equal(root.get(Problem_.getSingularAttribute("team")).get("idOrganization"),criteria.getIdOrganization()));
		}
			
		
		return cb.and(predicates.toArray(new Predicate[0]));
	}

}
