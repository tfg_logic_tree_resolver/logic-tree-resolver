package org.logic.tree.dao.criteria;

import java.io.Serializable;
import java.util.List;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

public class ProblemCriteria implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1295706061517102305L;

	private String title;
	private String creator;
	private Long idProblemType;
	private List<Long> listTeams;
	private Long idOrganization;

	public ProblemCriteria() {
		super();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Long getIdProblemType() {
		return idProblemType;
	}

	public void setIdProblemType(Long idProblemType) {
		this.idProblemType = idProblemType;
	}

	public List<Long> getListTeams() {
		return listTeams;
	}

	public void setListTeams(List<Long> listTeams) {
		this.listTeams = listTeams;
	}

	public Long getIdOrganization() {
		return idOrganization;
	}

	public void setIdOrganization(Long idOrganization) {
		this.idOrganization = idOrganization;
	}
	
	
}
