package org.logic.tree.dao.criteria.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;

import org.logic.tree.dao.criteria.TeamCriteria;
import org.logic.tree.dao.vo.TeamMember;
import org.logic.tree.dao.vo.pk.TeamMemberPK;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public class TeamMemberSpecification implements Specification<TeamMember>{

	private final TeamCriteria criteria;
	
	public TeamMemberSpecification(TeamCriteria criteria) {
		this.criteria = criteria;
	}
	
	@Override
	public Predicate toPredicate(Root<TeamMember> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		Assert.notNull(criteria);
		
		List<Predicate> predicates = new ArrayList<>();
		EntityType<TeamMember> TeamMember_ = root.getModel();
		
		root.fetch("team", JoinType.INNER);
		root.fetch("member", JoinType.INNER).fetch("organization",JoinType.INNER);
		
		if(criteria.getIdOrganization()!=null){
				           
			predicates.add(cb.equal(root.join("team").get("idOrganization"),criteria.getIdOrganization()));
								
		}
		
		if(!CollectionUtils.isEmpty(criteria.getIdMembers()) && criteria.getIdTeam() != null){
			
			List<TeamMemberPK> idValues = new ArrayList<TeamMemberPK>();
			
			for(Long idMember:criteria.getIdMembers()){
				idValues.add(new TeamMemberPK(criteria.getIdTeam(), idMember));
			}
			
			predicates.add(root.get("id").in(idValues));
		}
		
		predicates.add(cb.isNull(root.get(TeamMember_.getSingularAttribute("deleteDate"))));	
				
		return cb.and(predicates.toArray(new Predicate[0]));
	}


}
