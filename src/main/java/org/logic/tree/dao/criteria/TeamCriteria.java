package org.logic.tree.dao.criteria;

import java.io.Serializable;
import java.util.List;



/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public class TeamCriteria implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6309768450693011862L;
	private Long idOrganization;
	private Long idTeam;
	private List<Long> idMembers;


	public TeamCriteria() {
		super();
	}
	
	
	public TeamCriteria(Long idOrganization) {
		super();
		this.idOrganization = idOrganization;
	}

	public TeamCriteria(Long idTeam, List<Long> idMembers) {
		super();
		this.idTeam = idTeam;
		this.idMembers = idMembers;
	}


	public Long getIdOrganization() {
		return idOrganization;
	}

	public void setIdOrganization(Long idOrganization) {
		this.idOrganization = idOrganization;
	}


	public Long getIdTeam() {
		return idTeam;
	}


	public void setIdTeam(Long idTeam) {
		this.idTeam = idTeam;
	}


	public List<Long> getIdMembers() {
		return idMembers;
	}


	public void setIdMembers(List<Long> idMembers) {
		this.idMembers = idMembers;
	}

	

}
