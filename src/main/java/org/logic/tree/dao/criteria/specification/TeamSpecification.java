package org.logic.tree.dao.criteria.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;

import org.logic.tree.dao.criteria.TeamCriteria;
import org.logic.tree.dao.vo.Team;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.Assert;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public class TeamSpecification implements Specification<Team> {

	private final TeamCriteria criteria;
	
	public TeamSpecification(TeamCriteria criteria) {
		this.criteria = criteria;
	}
	
	@Override
	public Predicate toPredicate(Root<Team> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		Assert.notNull(criteria);
		
		List<Predicate> predicates = new ArrayList<>();
		EntityType<Team> Team_ = root.getModel();
			
	
		if(criteria.getIdOrganization()!=null){
			predicates.add(cb.equal(root.get(Team_.getSingularAttribute("idOrganization")),criteria.getIdOrganization()));
								
		}
		
		predicates.add(cb.isNull(root.get(Team_.getSingularAttribute("deleteDate"))));	
				
		return cb.and(predicates.toArray(new Predicate[0]));
	}

}
