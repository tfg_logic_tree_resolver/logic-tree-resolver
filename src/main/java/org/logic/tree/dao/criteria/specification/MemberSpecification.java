package org.logic.tree.dao.criteria.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;

import org.logic.tree.dao.criteria.MemberCriteria;
import org.logic.tree.dao.vo.Member;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public class MemberSpecification implements Specification<Member> {

	private final MemberCriteria criteria;
	
	public MemberSpecification(MemberCriteria criteria) {
		this.criteria = criteria;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Predicate toPredicate(Root<Member> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		
		Assert.notNull(criteria);
		
		root.fetch("organization",JoinType.LEFT);
		
		List<Predicate> predicates = new ArrayList<>();
		EntityType<Member> Member_ = root.getModel();
			
			
		if (!StringUtils.isEmpty(criteria.getUserName())) {
			predicates.add(cb.equal(root.get(Member_.getSingularAttribute("userName")),
					criteria.getUserName()));
		}

		if (!StringUtils.isEmpty(criteria.getMail())) {
			predicates.add(cb.equal(root.get(Member_.getSingularAttribute("mail")), criteria.getMail()));			
		}

		if (!StringUtils.isEmpty(criteria.getPassword())) {

			predicates.add(cb.equal(root.get(Member_.getSingularAttribute("passwordMember")),
					criteria.getPassword()));

		}

		if (criteria.getIdUser() != null) {
			predicates.add(cb.equal(root.get(Member_.getSingularAttribute("idMember")), criteria.getIdUser()));		
			
		}
		
		if (criteria.getIdOrganization() != null){
			predicates.add(cb.equal(root.get(Member_.getSingularAttribute("organization")).get("idOrganization"),criteria.getIdOrganization()));
		}
		
		if (criteria.getDomain() != null){
			String domainToSearch = "%@"+StringUtils.uncapitalize(criteria.getDomain()).trim()+".%";
			predicates.add(cb.like((Expression<String>) root.get(Member_.getSingularAttribute("mail")), domainToSearch));
		}
		
		
		if (!CollectionUtils.isEmpty(criteria.getListIdMembers())) {
			predicates.add(root.get(Member_.getSingularAttribute("idMember")).in(criteria.getListIdMembers()));		
			
		}
		
		if (criteria.getIdType()!= null){
			predicates.add(cb.equal(root.get(Member_.getSingularAttribute("idMemberType")), criteria.getIdType()));
		}
		
		//Only valid members
		predicates.add(cb.isNull(root.get(Member_.getSingularAttribute("deleteDate"))));
		
		
				
		
		return cb.and(predicates.toArray(new Predicate[0]));
		
	}
	

}
