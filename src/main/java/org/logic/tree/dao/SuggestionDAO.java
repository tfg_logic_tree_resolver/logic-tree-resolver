package org.logic.tree.dao;

import java.util.List;

import org.logic.tree.dao.vo.Suggestion;
import org.logic.tree.dao.vo.pk.SuggestionPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

@Repository
public interface SuggestionDAO extends JpaRepository<Suggestion, SuggestionPK>, JpaSpecificationExecutor<Suggestion>{

	public List<Suggestion> findByIdIdProblemAndIdIdStatusOrderByIdIdSuggestionDesc(Long idProblem,Long idStatus);
}
