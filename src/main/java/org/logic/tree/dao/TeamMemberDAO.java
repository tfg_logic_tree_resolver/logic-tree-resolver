package org.logic.tree.dao;

import org.logic.tree.dao.vo.TeamMember;
import org.logic.tree.dao.vo.pk.TeamMemberPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */
@Repository
public interface TeamMemberDAO extends JpaRepository<TeamMember, TeamMemberPK>, JpaSpecificationExecutor<TeamMember> {

	//Without new methods
}
