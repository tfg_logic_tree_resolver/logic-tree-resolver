package org.logic.tree.dao;

import java.util.List;

import org.logic.tree.dao.vo.StatusProblem;
import org.logic.tree.dao.vo.pk.StatusProblemPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */
@Repository
public interface StatusProblemDAO extends JpaRepository<StatusProblem, StatusProblemPK>, JpaSpecificationExecutor<StatusProblem> {

	public List<StatusProblem> findByIdIdProblem(Long idProblem);
}
