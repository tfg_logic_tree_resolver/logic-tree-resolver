package org.logic.tree.dao.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.WhereJoinTable;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

@Entity
@Table(name = "MEMBER")
public class Member implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6315285591084037342L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "MEMBER_ID")
	private Long idMember;

	@Column(name = "MEMBER_COMPLETE_NAME")
	private String completeName;

	@Column(name = "MEMBER_MAIL")
	@NotBlank
	@Length(max = 255)
	private String mail;
	
	@Column(name = "MEMBER_COLOR")
	@NotBlank
	@Length(max = 6)
	private String color;

	@Column(name = "MEMBER_PASSWORD")
	@NotBlank
	@Length(max = 255)
	private String passwordMember;

	@Column(name = "MEMBER_USER_NAME")
	@NotBlank
	@Length(max = 255)
	private String userName;
	
	@Column(name = "MEMBER_CREATOR")
	@NotBlank
	@Length(max = 255)
	private String creator;

	@Column(name = "MEMBER_CREATION_DATE")
	@NotNull
	private Date creationDate;

	@Column(name = "MEMBER_MODIFIER")
	@Length(max = 255)
	private String modifier;

	@Column(name = "MEMBER_MODIFICATION_DATE")
	private Date modificationDate;

	@Column(name = "MEMBER_DELETER")
	@Length(max = 255)
	private String deleter;

	@Column(name = "MEMBER_DELETE_DATE")
	private Date deleteDate;

	@Column(name = "MEMBER_TYPE_ID")
	@NotNull
	private Integer idMemberType;
	
	@Column(name = "APPLICATION_LANGUAGE_ID")
	@NotNull
	private Integer idApplicationLanguage;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name = "ORGANIZATION_ID")
	private Organization organization;
	
	@ManyToMany
	@JoinTable(
			name= "TEAM_MEMBER",
			joinColumns= @JoinColumn(name="MEMBER_ID", referencedColumnName="MEMBER_ID"),
			inverseJoinColumns= @JoinColumn(name="TEAM_ID", referencedColumnName="TEAM_ID"))
	@WhereJoinTable(clause="TEAM_MEMBER_DELETE_DATE IS NULL")
	private List<Team> teams;

	public Member() {
		super();
	}

	public Long getIdMember() {
		return idMember;
	}

	public void setIdMember(Long idMember) {
		this.idMember = idMember;
	}

	public String getCompleteName() {
		return completeName;
	}

	public void setCompleteName(String completeName) {
		this.completeName = completeName;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getPasswordMember() {
		return passwordMember;
	}

	public void setPasswordMember(String passwordMember) {
		this.passwordMember = passwordMember;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public String getDeleter() {
		return deleter;
	}

	public void setDeleter(String deleter) {
		this.deleter = deleter;
	}

	public Date getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getIdMemberType() {
		return idMemberType;
	}

	public void setIdMemberType(Integer idMemberType) {
		this.idMemberType = idMemberType;
	}

	public Integer getIdApplicationLanguage() {
		return idApplicationLanguage;
	}

	public void setIdApplicationLanguage(Integer idApplicationLanguage) {
		this.idApplicationLanguage = idApplicationLanguage;
	}

	public List<Team> getTeams() {
		return teams;
	}

	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	
	
}
