package org.logic.tree.dao.vo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

@Entity
@Table(name = "PROBLEM")
public class Problem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 839357825956589089L;


	@Id
	@Column(name = "PROBLEM_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "PROBLEM_TITLE")
	@NotBlank
	@Length(max = 255)
	private String title;

	@Column(name = "PROBLEM_DESCRIPTION")
	@Length(max = 500)
	private String description;

	@Column(name = "PROBLEM_CREATOR")
	@NotBlank
	@Length(max = 50)
	private String creator;

	@Column(name = "PROBLEM_CREATION_DATE")
	@NotNull
	private Date creationDate;

	@Column(name = "PROBLEM_MODIFIER")
	@Length(max = 50)
	private String modifier;

	@Column(name = "PROBLEM_MODIFICATION_DATE")
	private Date modificationDate;

	@Column(name = "PROBLEM_DELETER")
	@Length(max = 50)
	private String deleter;

	@Column(name = "PROBLEM_DELETE_DATE")
	private Date deleteDate;

	@Column(name = "PROBLEM_TYPE_ID")
	@NotNull
	private Long idProblemType;
	
	@Column(name = "PROBLEM_RESOLVED")
	@NotNull
	private Boolean problemResolved;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "TEAM_ID")
	private Team team;

	public Problem() {
		super();
	}

		
	public Problem(Long id, String title, String description, String creator, Date creationDate, String modifier,
			Date modificationDate, String deleter, Date deleteDate, Long idProblemType, Team team) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.creator = creator;
		this.creationDate = creationDate;
		this.modifier = modifier;
		this.modificationDate = modificationDate;
		this.deleter = deleter;
		this.deleteDate = deleteDate;
		this.idProblemType = idProblemType;
		this.team = team;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public String getDeleter() {
		return deleter;
	}

	public void setDeleter(String deleter) {
		this.deleter = deleter;
	}

	public Date getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}

	public Long getIdProblemType() {
		return idProblemType;
	}

	public void setIdProblemType(Long idProblemType) {
		this.idProblemType = idProblemType;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}


	public Boolean getProblemResolved() {
		return problemResolved;
	}


	public void setProblemResolved(Boolean problemResolved) {
		this.problemResolved = problemResolved;
	}


}
