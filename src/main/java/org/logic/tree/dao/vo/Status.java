package org.logic.tree.dao.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

@Entity
@Table(name = "STATUS")
public class Status implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5080714593640581671L;

	@Id
	@Column(name = "STATUS_ID")
	private Long id;

	@Column(name = "STATUS_DESCRIPTION")
	@NotBlank
	@Length(max = 255)
	private String description;

;

	public Status() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


}
