package org.logic.tree.dao.vo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.logic.tree.dao.vo.pk.StatusProblemPK;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

@Entity
@Table(name = "STATUS_PROBLEM")
public class StatusProblem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7367345368006462754L;

	@EmbeddedId
	private StatusProblemPK id;

	@Column(name="STATUS_PROBLEM_DIAGRAM")
	private String diagram;
	
	@Column(name = "STATUS_PROBLEM_CREATOR")
	@NotBlank
	@Length(max = 50)
	private String creator;

	@Column(name = "STATUS_PROBLEM_INIT_DATE")
	private Date initDate;

	@Column(name = "STATUS_PROBLEM_MODIFIER")
	@Length(max = 50)
	private String modifier;

	@Column(name = "STATUS_PROBLEM_FINISH_DATE")
	private Date finishDate;

	public StatusProblem() {
		super();
	}

	public StatusProblemPK getId() {
		return id;
	}

	public void setId(StatusProblemPK id) {
		this.id = id;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getInitDate() {
		return initDate;
	}

	public void setInitDate(Date initDate) {
		this.initDate = initDate;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public Date getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}

	public String getDiagram() {
		return diagram;
	}

	public void setDiagram(String diagram) {
		this.diagram = diagram;
	}

	
}
