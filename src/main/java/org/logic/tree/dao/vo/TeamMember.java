package org.logic.tree.dao.vo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.logic.tree.dao.vo.pk.TeamMemberPK;


/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

@Entity
@Table(name = "TEAM_MEMBER")
public class TeamMember implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3145060982008649044L;

	@EmbeddedId
	private TeamMemberPK id;

	@Column(name = "TEAM_MEMBER_CREATOR")
	@NotBlank
	@Length(max = 50)
	private String creator;

	@Column(name = "TEAM_MEMBER_CREATION_DATE")
	@NotNull
	private Date creationDate;

	@Column(name = "TEAM_MEMBER_MODIFIER")
	@Length(max = 50)
	private String modifier;

	@Column(name = "TEAM_MEMBER_MODIFICATION_DATE")
	private Date modificationDate;

	@Column(name = "TEAM_MEMBER_DELETER")
	@Length(max = 255)
	private String deleter;

	@Column(name = "TEAM_MEMBER_DELETE_DATE")
	private Date deleteDate;
		
	@ManyToOne
	@JoinColumn(name = "TEAM_ID",insertable =  false, updatable = false, nullable = false)
	private Team team;
	
	@ManyToOne
	@JoinColumn(name = "MEMBER_ID", insertable =  false, updatable = false, nullable = false)
	private Member member;


	public TeamMember() {
		super();
	}

	public TeamMemberPK getId() {
		return id;
	}

	public void setId(TeamMemberPK id) {
		this.id = id;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public String getDeleter() {
		return deleter;
	}

	public void setDeleter(String deleter) {
		this.deleter = deleter;
	}

	public Date getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	
}
