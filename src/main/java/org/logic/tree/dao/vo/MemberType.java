package org.logic.tree.dao.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a> 
 *
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "MEMBER_TYPE")
public class MemberType implements Serializable {

	@Id
	@Column(name = "MEMBER_TYPE_ID")
	@NotBlank
	private Integer idMemberType;

	@Column(name = "MEMBER_TYPE_DESCRIPTION")
	@Length(max = 50)
	private String descriptionMemberType;

	public MemberType() {
		super();
	}

	public Integer getIdMemberType() {
		return idMemberType;
	}

	public void setIdMemberType(Integer idMemberType) {
		this.idMemberType = idMemberType;
	}

	public String getDescriptionMemberType() {
		return descriptionMemberType;
	}

	public void setDescriptionMemberType(String descriptionMemberType) {
		this.descriptionMemberType = descriptionMemberType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((descriptionMemberType == null) ? 0 : descriptionMemberType
						.hashCode());
		result = prime * result
				+ ((idMemberType == null) ? 0 : idMemberType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MemberType other = (MemberType) obj;
		if (descriptionMemberType == null) {
			if (other.descriptionMemberType != null)
				return false;
		} else if (!descriptionMemberType.equals(other.descriptionMemberType))
			return false;
		if (idMemberType == null) {
			if (other.idMemberType != null)
				return false;
		} else if (!idMemberType.equals(other.idMemberType))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MemberType [idMemberType=" + idMemberType
				+ ", descriptionMemberType=" + descriptionMemberType + "]";
	}

}
