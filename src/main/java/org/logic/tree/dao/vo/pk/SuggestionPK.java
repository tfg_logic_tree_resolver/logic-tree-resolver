package org.logic.tree.dao.vo.pk;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

@Embeddable
public class SuggestionPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6376925271742153943L;

	@Column(name = "SUGGESTION_ID")
	private Long idSuggestion;

	@Column(name = "STATUS_ID")
	private Long idStatus;

	@Column(name = "PROBLEM_ID")
	private Long idProblem;

	public SuggestionPK() {
		super();
	}

	public Long getIdSuggestion() {
		return idSuggestion;
	}

	public void setIdSuggestion(Long idSuggestion) {
		this.idSuggestion = idSuggestion;
	}

	public Long getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(Long idStatus) {
		this.idStatus = idStatus;
	}

	public Long getIdProblem() {
		return idProblem;
	}

	public void setIdProblem(Long idProblem) {
		this.idProblem = idProblem;
	}

	public SuggestionPK(Long idSuggestion, Long idStatus, Long idProblem) {
		super();
		this.idSuggestion = idSuggestion;
		this.idStatus = idStatus;
		this.idProblem = idProblem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idProblem == null) ? 0 : idProblem.hashCode());
		result = prime * result + ((idStatus == null) ? 0 : idStatus.hashCode());
		result = prime * result + ((idSuggestion == null) ? 0 : idSuggestion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SuggestionPK other = (SuggestionPK) obj;
		if (idProblem == null) {
			if (other.idProblem != null)
				return false;
		} else if (!idProblem.equals(other.idProblem))
			return false;
		if (idStatus == null) {
			if (other.idStatus != null)
				return false;
		} else if (!idStatus.equals(other.idStatus))
			return false;
		if (idSuggestion == null) {
			if (other.idSuggestion != null)
				return false;
		} else if (!idSuggestion.equals(other.idSuggestion))
			return false;
		return true;
	}

}
