package org.logic.tree.dao.vo.pk;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

@Embeddable
public class TeamMemberPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6356864815449317246L;
	
	@Column(name = "TEAM_ID")
	private Long idTeam;
	
	@Column(name = "MEMBER_ID")
	private Long idMember;

	public TeamMemberPK() {
		super();
	}

	public TeamMemberPK(Long idTeam, Long idMember) {
		super();
		this.idTeam = idTeam;
		this.idMember = idMember;
	}

	public Long getIdTeam() {
		return idTeam;
	}

	public void setIdTeam(Long idTeam) {
		this.idTeam = idTeam;
	}

	public Long getIdMember() {
		return idMember;
	}

	public void setIdMember(Long idMember) {
		this.idMember = idMember;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idMember == null) ? 0 : idMember.hashCode());
		result = prime * result + ((idTeam == null) ? 0 : idTeam.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TeamMemberPK other = (TeamMemberPK) obj;
		if (idMember == null) {
			if (other.idMember != null)
				return false;
		} else if (!idMember.equals(other.idMember))
			return false;
		if (idTeam == null) {
			if (other.idTeam != null)
				return false;
		} else if (!idTeam.equals(other.idTeam))
			return false;
		return true;
	}

}
