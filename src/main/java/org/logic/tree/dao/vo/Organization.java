package org.logic.tree.dao.vo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;


/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a> 
 *
 */
@Entity
@Table(name = "ORGANIZATION")
public class Organization implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6843292510917179314L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ORGANIZATION_ID")
	private Long idOrganization;

	@Column(name = "ORGANIZATION_NAME")
	@Length(max = 100)
	private String nameOrganization;

	@Column(name = "ORGANIZATION_CREATOR")
	@NotBlank
	@Length(max = 255)
	private String creator;

	@Column(name = "ORGANIZATION_CREATION_DATE")
	@NotNull
	private Date creationDate;

	@Column(name = "ORGANIZATION_MODIFIER")
	@Length(max = 255)
	private String modifier;

	@Column(name = "ORGANIZATION_MODIFICATION_DATE")
	private Date modificationDate;

	@Column(name = "ORGANIZATION_DELETER")
	@Length(max = 255)
	private String deleter;

	@Column(name = "ORGANIZATION_DELETE_DATE")
	private Date deleteDate;

	@Column(name = "APPLICATION_LANGUAGE_ID")
	private Integer idApplicationLanguage;

	public Organization() {
		super();
	}

	public Long getIdOrganization() {
		return idOrganization;
	}

	public void setIdOrganization(Long idOrganization) {
		this.idOrganization = idOrganization;
	}

	public String getNameOrganization() {
		return nameOrganization;
	}

	public void setNameOrganization(String nameOrganization) {
		this.nameOrganization = nameOrganization;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public String getDeleter() {
		return deleter;
	}

	public void setDeleter(String deleter) {
		this.deleter = deleter;
	}

	public Date getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}

	public Integer getIdApplicationLanguage() {
		return idApplicationLanguage;
	}

	public void setIdApplicationLanguage(Integer idApplicationLanguage) {
		this.idApplicationLanguage = idApplicationLanguage;
	}

}
