package org.logic.tree.dao.vo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.logic.tree.dao.vo.pk.SuggestionPK;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

@Entity
@Table(name = "SUGGESTION")
public class Suggestion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5581328553804522353L;

	@EmbeddedId
	private SuggestionPK id;

	@Column(name = "SUGGESTION_DESCRIPTION")
	@NotBlank
	@Length(max = 500)
	private String description;

	@Column(name = "SUGGESTION_CREATOR")
	@NotBlank
	@Length(max = 50)
	private String creator;

	@Column(name = "SUGGESTION_CREATION_DATE")
	@NotNull
	private Date creationDate;

	public Suggestion() {
		super();
	}

	public SuggestionPK getId() {
		return id;
	}

	public void setId(SuggestionPK id) {
		this.id = id;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

}
