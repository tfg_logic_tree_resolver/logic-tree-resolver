package org.logic.tree.dao.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

@Entity
@Table(name = "PROBLEM")
public class ProblemType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9063537491901346515L;

	@Id
	@Column(name = "PROBLEM_TYPE_ID")
	@NotBlank
	private Long id;

	@Column(name = "PROBLEM_TYPE_DESCRIPTION")
	@Length(max = 50)
	@NotBlank
	private String description;

	public ProblemType() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
