package org.logic.tree.dao.vo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TEAM")
public class Team implements Serializable {

	@Column(name = "TEAM_ID")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "TEAM_NAME")
	@NotBlank
	@Length(max = 100)
	private String nameTeam;

	@Column(name = "TEAM_CREATOR")
	@NotBlank
	@Length(max = 50)
	private String creator;

	@Column(name = "TEAM_CREATION_DATE")
	@NotNull
	private Date creationDate;

	@Column(name = "TEAM_MODIFIER")
	@Length(max = 50)
	private String modifier;

	@Column(name = "TEAM_MODIFICATION_DATE")
	private Date modificationDate;

	@Column(name = "TEAM_DELETER")
	@Length(max = 50)
	private String deleter;

	@Column(name = "TEAM_DELETE_DATE")
	private Date deleteDate;

	@Column(name = "ORGANIZATION_ID")
	@NotNull
	private Long idOrganization;
	
	

	public Team() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDeleter() {
		return deleter;
	}

	public void setDeleter(String deleter) {
		this.deleter = deleter;
	}

	public Date getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}

	public String getNameTeam() {
		return nameTeam;
	}

	public void setNameTeam(String nameTeam) {
		this.nameTeam = nameTeam;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Long getIdOrganization() {
		return idOrganization;
	}

	public void setIdOrganization(Long idOrganization) {
		this.idOrganization = idOrganization;
	}

}
