package org.logic.tree.dao.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "APPLICATION_LANGUAGE")
public class ApplicationLanguage implements Serializable {

	@Id
	@Column(name = "APPLICATION_LANGUAGE_ID")
	@NotBlank
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "APPLICATION_LANGUAGE_DESCRIPTION")
	@Length(max = 40)
	private String descriptionApplicationLanguage;

	@Column(name = "APPLICATION_LANGUAGE_LOCALE")
	@Length(max = 10)
	private String localeApplicationLanguage;

	public ApplicationLanguage() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescriptionApplicationLanguage() {
		return descriptionApplicationLanguage;
	}

	public void setDescriptionApplicationLanguage(String descriptionApplicationLanguage) {
		this.descriptionApplicationLanguage = descriptionApplicationLanguage;
	}

	public String getLocaleApplicationLanguage() {
		return localeApplicationLanguage;
	}

	public void setLocaleApplicationLanguage(String localeApplicationLanguage) {
		this.localeApplicationLanguage = localeApplicationLanguage;
	}



}
