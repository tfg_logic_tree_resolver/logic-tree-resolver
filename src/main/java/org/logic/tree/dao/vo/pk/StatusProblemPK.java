package org.logic.tree.dao.vo.pk;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

@Embeddable
public class StatusProblemPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -325648710213639961L;

	@Column(name = "STATUS_ID")
	private Long idStatus;

	@Column(name = "PROBLEM_ID")
	private Long idProblem;

	public StatusProblemPK() {
		super();
	}

	public StatusProblemPK(Long idStatus, Long idProblem) {
		super();
		this.idStatus = idStatus;
		this.idProblem = idProblem;
	}

	public Long getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(Long idStatus) {
		this.idStatus = idStatus;
	}

	public Long getIdProblem() {
		return idProblem;
	}

	public void setIdProblem(Long idProblem) {
		this.idProblem = idProblem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idProblem == null) ? 0 : idProblem.hashCode());
		result = prime * result + ((idStatus == null) ? 0 : idStatus.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StatusProblemPK other = (StatusProblemPK) obj;
		if (idProblem == null) {
			if (other.idProblem != null)
				return false;
		} else if (!idProblem.equals(other.idProblem))
			return false;
		if (idStatus == null) {
			if (other.idStatus != null)
				return false;
		} else if (!idStatus.equals(other.idStatus))
			return false;
		return true;
	}

}
