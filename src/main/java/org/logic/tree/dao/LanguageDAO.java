package org.logic.tree.dao;

import org.logic.tree.dao.vo.ApplicationLanguage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

@Repository
public interface LanguageDAO extends JpaRepository<ApplicationLanguage, Integer>, JpaSpecificationExecutor<ApplicationLanguage> {

	// Without new methods

}
