package org.logic.tree.dao;

import java.util.List;

import org.logic.tree.dao.vo.Member;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

@Repository
public interface MemberDAO extends JpaRepository<Member, Long>, JpaSpecificationExecutor<Member> {

	public List<Member> findByOrganizationIdOrganization(Long idOrganization);

}
