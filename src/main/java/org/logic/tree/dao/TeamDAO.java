package org.logic.tree.dao;

import java.util.List;

import org.logic.tree.dao.vo.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

@Repository
public interface TeamDAO extends JpaRepository<Team, Long>, JpaSpecificationExecutor<Team> {

	List<Team> findByIdOrganization(Long idOrganization);
}
