/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */
package org.logic.tree.util;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public final class Constants {

	private Constants() {
		// restrict instatiation
	}
	
	public static final int MEMBER_TYPE_REGULAR = 1;
	public static final int MEMBER_TYPE_ADMIN = 2;
	public static final int MEMBER_TYPE_ORGANIZATION = 3;
	
	public static final int APPLICATION_LANGUAGE_ID_ENG = 1;
	
	public static final int PROBLEM_TYPE_ROOT_CAUSES = 1;
	public static final int PROBLEM_TYPE_GOALS = 2;
	public static final int PROBLEM_TYPE_PROS_CONS = 3;

	public static final int STATUS_STEP_1 = 1;
	public static final int STATUS_STEP_2 = 2;
	public static final int STATUS_STEP_3 = 3;
	public static final int STATUS_STEP_4 = 4;
	public static final int STATUS_STEP_5 = 5;
	public static final int STATUS_STEP_6 = 6;
	public static final int STATUS_STEP_7 = 7;
	
}
