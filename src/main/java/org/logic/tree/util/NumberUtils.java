package org.logic.tree.util;

import java.util.ArrayList;
import java.util.List;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

public final class NumberUtils {

	private NumberUtils() {
		
	}

	
	public static List<Long> fromStringToLong(List<String> listStrings){
		
		List<Long> listConvertedLong = new ArrayList<Long>();
		
		
		for(String string: listStrings){
			listConvertedLong.add(Long.parseLong(string));
		}
		
		return listConvertedLong;
		
	}
}
