package org.logic.tree.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a> 
 *
 */

public class EncryptUtil {
	
	private static final char[] HEXADECIMAL = { '0', '1', '2', '3',
        '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

    public static String hash(String stringToHash) throws NoSuchAlgorithmException {

            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            
            byte[] bytes = messageDigest.digest(stringToHash.getBytes());
            
            StringBuilder hashedString = new StringBuilder(2 * bytes.length);
            
            for (int i = 0; i < bytes.length; i++) {
            	
                int low = (int)(bytes[i] & 0x0f);
                int high = (int)((bytes[i] & 0xf0) >> 4);
                
                hashedString.append(HEXADECIMAL[high]);
                hashedString.append(HEXADECIMAL[low]);
            }
            return hashedString.toString();

    }
	
}
