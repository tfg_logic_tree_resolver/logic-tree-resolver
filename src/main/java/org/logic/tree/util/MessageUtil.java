package org.logic.tree.util;

import java.util.Locale;
import java.util.ResourceBundle;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a> 
 *
 */

public class MessageUtil {


	
	public static String getMessage(String key){
		ResourceBundle labels = ResourceBundle.getBundle("messages", Locale.getDefault());
		
		return labels.getString(key);
	}
	
}
