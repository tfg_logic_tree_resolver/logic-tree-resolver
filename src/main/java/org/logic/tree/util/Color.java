package org.logic.tree.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public final class Color {
	
	private Color(){}
	
	public static final String WHITE = "ffffff";
	public static final String GREEN = "#7DA753";
	
	
	public static final String GOAL_BEGIN = "#5FB6FF";
	public static final String GOAL_GAP = "#6C6EFF";
	public static final String GOAL_END = "#B55FFF";
	
	
			
	
	public static List<String> randomColor(int numberOfColors){

		List<String> colors = new ArrayList<String>();
		
		Random random = new Random();

		
		for(int i=1; i<=numberOfColors; i++){
			
			colors.add(String.format("#%06x", random.nextInt(256*256*256)));
		}

		
		return colors;
	}

}
