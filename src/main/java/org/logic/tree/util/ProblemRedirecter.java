package org.logic.tree.util;

import static org.logic.tree.util.Constants.PROBLEM_TYPE_GOALS;
import static org.logic.tree.util.Constants.PROBLEM_TYPE_PROS_CONS;
import static org.logic.tree.util.Constants.PROBLEM_TYPE_ROOT_CAUSES;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

public class ProblemRedirecter {

	private ProblemRedirecter() {
		//
	}

	/**
	 * 
	 * @param idProblemType type of problem: rootCause, Goals, pros&cons
	 * @param usePrefix Point out if the redirect prefix is needed or not.
	 * @return a string with the view to redirect.
	 */
	public static String redirect(int idProblemType, boolean usePrefix) {

		String redirection ="";
		
		switch (idProblemType) {
		case PROBLEM_TYPE_ROOT_CAUSES:
			redirection =  "root_cause";
			break;
		case PROBLEM_TYPE_GOALS:
			redirection = "goals";
			break; 
		case PROBLEM_TYPE_PROS_CONS:
			redirection = "pros_cons";
			break;
		default:
			break;
		}
		
		redirection = (usePrefix)?"redirect:/"+redirection:redirection;
		
		return redirection;

	}
	
}
