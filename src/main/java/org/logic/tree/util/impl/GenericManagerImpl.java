package org.logic.tree.util.impl;

import java.io.Serializable;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.logic.tree.manager.exception.ModelException;
import org.logic.tree.util.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 *
 */

public class GenericManagerImpl<T extends Serializable> {

	private Logger LOG = LoggerFactory.getLogger(GenericManagerImpl.class);
	
	@Autowired
	protected Mapper mapper;
	

	/***
	 * This method validates the object values in search of constraints
	 * violations.
	 * 
	 * @param object
	 */
	public void validateObject(T object) throws ModelException{
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		
		Set<ConstraintViolation<T>> violations = validator.validate(object);
		
		if(violations != null && !violations.isEmpty()){
			String errMessage = "There are some constraint violations: ";
						
			for(ConstraintViolation<T> violation: violations){
				
				errMessage += violation.getPropertyPath()+ " "+ violation.getMessage()+";";
			}
			
			LOG.error(errMessage);
			throw new ModelException(errMessage);
		}

		
	}
	

	
}
