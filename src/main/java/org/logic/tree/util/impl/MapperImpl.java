package org.logic.tree.util.impl;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.dozer.MappingException;
import org.logic.tree.util.Mapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MapperImpl extends DozerBeanMapper implements Mapper{

	  
	@Value("dozer_mappings.xml")
	@Override
	public void setMappingFiles(List<String> mappingFileUrls) {
	    super.setMappingFiles(mappingFileUrls);
	}
		
	@SuppressWarnings("rawtypes")
	@Override
	public <T> List<T> mapList(List source, Class<T> destinationClass) throws MappingException {
				
		List<T> list = new ArrayList<T>();
		
		if(!source.isEmpty()){
			for(Object o: source){
				list.add(map(o, destinationClass));
			}
		}
		
		return list;
		
	}

}
