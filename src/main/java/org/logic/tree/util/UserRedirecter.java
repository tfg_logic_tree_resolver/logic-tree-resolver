package org.logic.tree.util;

import static org.logic.tree.util.Constants.MEMBER_TYPE_ADMIN;
import static org.logic.tree.util.Constants.MEMBER_TYPE_ORGANIZATION;
import static org.logic.tree.util.Constants.MEMBER_TYPE_REGULAR;


/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

public class UserRedirecter {

	private UserRedirecter() {
		//
	}

	/**
	 * 
	 * @param idMemberType type of member: regular, organization or admin
	 * @param usePrefix Point out if the redirect prefix is needed or not.
	 * @return a string with the view to redirect.
	 */
	public static String redirect(int idMemberType, boolean usePrefix) {

		String redirection ="";
		
		switch (idMemberType) {
		case MEMBER_TYPE_REGULAR:
			redirection =  "user_dashboard";
			break;
		case MEMBER_TYPE_ORGANIZATION:
			redirection = "org_dashboard";
			break; 
		case MEMBER_TYPE_ADMIN:
			redirection = "admin_dashboard";
			break;
		default:
			break;
		}
		
		redirection = (usePrefix)?"redirect:/"+redirection:redirection;
		
		return redirection;

	}

}
