package org.logic.tree.util;

import java.util.List;

import org.dozer.MappingException;
import org.springframework.stereotype.Component;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a> 
 *
 */

@Component
public interface Mapper extends org.dozer.Mapper {
	
	  /**
	   * Constructs a list of new instances of destinationClass and performs mapping between from source
	   *
	   * @param source
	   * @param destinationClass
	   * @param <T>
	   * @return
	   * @throws MappingException
	   */
	   @SuppressWarnings("rawtypes")
	public <T> List<T> mapList(List source, Class<T> destinationClass) throws MappingException;

}
