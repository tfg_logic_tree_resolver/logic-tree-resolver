package org.logic.tree.util;

import java.util.Date;

/***
 * 
 * @author <a href="mailto:nluaces@gmail.com">Noelia Luaces</a>
 * 
 */

public interface Auditable {
	
	/**
	 * Specifies the creator and modifier in the operation
	 * 
	 * @param user
	 * @param date
	 */
	public void saveOperation(String user, Date date);
	
	/**
	 * Specifies the modifier in the operation
	 * @param user
	 * @param date
	 */
	public void updateOperation(String user, Date date);
	
	/**
	 * Specifies the modifier and deleter in the operation
	 * @param user
	 * @param date
	 */
	public void deleteOperation(String user, Date date);

}
