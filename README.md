Web Application that implements Ken Watanabe's book "Problem Solving 101" strategies 
in order to resolve problems in a collaborative way with diagrams and low latency.
